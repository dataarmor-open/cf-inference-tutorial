from sklearn.datasets import load_boston
import sys
import numpy as np
import pickle
import time
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.utils import check_X_y, check_array
from sklearn.exceptions import NotFittedError
from scipy.sparse.linalg import lsmr
from sklearn.preprocessing import normalize
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV, KFold, train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.datasets import load_boston
from sklearn.pipeline import Pipeline
from tqdm import tqdm

sys.path.append("../")

from gateai_connector.enc_lssvr import ENC_LSSVR
from gateai_connector.enc_preprocess import ENC_PREPROCESS


# cross validation using cv (for example cv = KFold)
def cross_val_score(preprocess, preprocess_way, model, cv):
    mse_list = np.array([])
    x_shape = preprocess.shape_x()
    y_shape = preprocess.shape_y()
    indices = np.arange(y_shape)

    for train_index, test_index in tqdm(cv.split(indices), desc="cross_val_score", leave=False):
        # get train and test data
        preprocess.train_test_split_with_indices(train_index, test_index)
        # transform data
        if preprocess_way == "minmax":
            preprocess.minmax_scaler_fit()
            preprocess.minmax_scaler_transform_x_train()
            preprocess.minmax_scaler_transform_x_test()
        elif preprocess_way == "standard":
            preprocess.standard_scaler_fit()
            preprocess.standard_scaler_transform_x_train()
            preprocess.standard_scaler_transform_x_test()
        else:
            raise ValueError(f"Current preprocess_way = {preprocess_way}, only support minmax or standard")

        preprocess.send_svr()
        # fit model
        model.train_with_score()
        # loss
        mse = model.score_for_test()
        mse_list = np.append(mse_list, mse)
    return mse_list


# gridsearch using cross validation
def gridsearchCV(preprocess, preprocess_way, params, cv):
    if not (preprocess_way == "minmax" or preprocess_way == "standard"):
        raise ValueError(f"Current preprocess_way = {preprocess_way}, only support minmax or standard")
    score = np.array([])
    gridsearch = {
        "best_score_": None,
        "best_C": None,
        "best_epsilon": None,
    }
    for c in tqdm(params["regressor__C"], desc="regressor__C"):
        for epsilon in tqdm(params["regressor__epsilon"], desc="regressor__epsilon"):
            model = ENC_LSSVR()
            model.set_config(
                c,
                epsilon,
            )
            try:
                score_list = cross_val_score(preprocess, preprocess_way, model, cv)
            except Exception as e:
                tqdm.write("c = {}, epsilon = {}, error")
                continue
            score = np.average(score_list)
            if gridsearch["best_score_"] == None:
                gridsearch["best_score_"] = score
                gridsearch["best_C"] = c
                gridsearch["best_epsilon"] = epsilon
            elif gridsearch["best_score_"] > score:
                gridsearch["best_score_"] = score
                gridsearch["best_C"] = c
                gridsearch["best_epsilon"] = epsilon
            tqdm.write("c = {}, epsilon = {}, score = {}".format(c, epsilon, score))
    return gridsearch


if __name__ == "__main__":
    # initialize & keygen
    boston = load_boston()
    xs = boston.data
    ys = boston.target
    preprocess = ENC_PREPROCESS()
    preprocess.reset()
    preprocess.keygen()
    reg = ENC_LSSVR()
    reg.reset()
    reg.keygen()

    # send data
    preprocess.transfer_data(xs, ys)

    # KFoldの設定
    kf = KFold(n_splits=3, shuffle=True)

    # SVRで、パラメータC, epsilonに対して、グリッドサーチを行う。
    params_cnt = 5
    params = {"regressor__C": np.logspace(-2, 2, params_cnt), "regressor__epsilon": np.logspace(-2, 2, params_cnt)}
    print(params)

    gridsearch = gridsearchCV(preprocess, "standard", params, cv=kf)
    print(gridsearch["best_score_"])
    print(gridsearch["best_C"])
    print(gridsearch["best_epsilon"])
