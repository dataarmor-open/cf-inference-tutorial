import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.utils import check_X_y, check_array
from sklearn.exceptions import NotFittedError
from scipy.sparse.linalg import lsmr
from sklearn.preprocessing import normalize
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.datasets import make_regression

sys.path.append("../../")
from gateai_connector.enc_lssvr import ENC_LSSVR

if __name__ == "__main__":
    reg = ENC_LSSVR()
    reg.save_model_on_server("test")
    reg.load_model_on_server("test")
