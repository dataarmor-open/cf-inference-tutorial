#!/usr/bin/python
# -*- coding: utf-8 -*-

from typing import Tuple
from tqdm import tqdm
from re import X
from multiprocessing.spawn import prepare
import sys
from tkinter import Y

import grpc
import numpy as np
import os
import constants

import pytest
from math import isclose
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error

sys.path.append("../")
from gateai_connector.plain_lssvr import LSSVR_GRAD
import constants_client
from grpc_service import input_client_pb2_grpc
from grpc_service import common_model
from grpc_service import noise_service_pb2_grpc
from grpc_service import svr_model
from grpc_service import storage_service_pb2_grpc

from dotenv import load_dotenv

load_dotenv()

"""ENC_LSSVR

* SVRサービスを利用するためのinput_clientとのコネクタ

"""


def make_pb_from_doubleList(doubleList):
    if len(doubleList.shape) == 2:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = np.ravel(doubleList)
        pb.size = doubleList.shape[1]
    else:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = doubleList
        pb.size = 0
    return pb


def make_AzureBlobMessage(container_id: str, filepath: str, blob_name: str = ""):
    """
    Create AzureBlobMessage

    Parameters
    ----------
    container_id : str
    filepath : str
    blob_name : str, ""

    Returns
    -------
    ABMessage : AzureBlobMessage
    """
    ABMessage = common_model.azureBlob_pb2.AzureBlobMessage()
    ABMessage.container_id = container_id
    ABMessage.filepath = filepath
    ABMessage.blob_name = blob_name
    return ABMessage


class ENC_LSSVR:
    def __init__(self, first_impl_rate=1, second_impl_rate=1):
        channel_input_client = grpc.insecure_channel(
            f"{constants_client.INPUT_CLIENT_HOST}:{constants_client.INPUT_CLIENT_PORT}",
            options=[
                ("grpc.max_message_length", constants_client.GRPC_MAX_MESSAGE_LENGTH),
                (
                    "grpc.max_receive_message_length",
                    constants_client.GRPC_MAX_MESSAGE_LENGTH,
                ),
            ],
        )
        self.stub = input_client_pb2_grpc.InputClientStub(channel_input_client)
        options = [
            ("grpc.max_message_length", constants.GRPC_MAX_MESSAGE_LENGTH),
            ("grpc.max_receive_message_length", constants.GRPC_MAX_MESSAGE_LENGTH),
        ]
        channel = grpc.insecure_channel(f"{constants.STORAGE_HOST}:{constants.STORAGE_PORT}", options=options)
        self.stub_storage_service = storage_service_pb2_grpc.StorageServiceStub(channel)
        self.first_impl_rate = first_impl_rate
        self.second_impl_rate = second_impl_rate

    def reset(self):
        """SVRインスタンスの初期化
        送信したパラメータ、X, y, サポートベクターのフラグを初期化する。
        """

        self.stub.svr_reset_data(common_model.message_pb2.PB_Message())

    def keygen(self):
        """鍵生成
        input_clientに鍵を生成する。
        """
        self.stub.svr_gen_key(common_model.message_pb2.PB_Message())

    def save_key_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.svr_save_secret_key(pb_message)

    def load_key_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.svr_load_secret_key(pb_message)

    def set_config(self, C, epsilon, gamma="auto"):
        """パラメータの送信
        Args:
           X_attrs: 学習データの重み
           X_ds: 学習データのデータ数
           C: 正則化パラメータ
           epsilon: イプシロンチューブの長さ
           learning_rate: 最適化問題の学習率
           learning_epoch: 最適化問題の回数
        """
        self.C = C
        self.epsilon = epsilon
        self.gamma = gamma

    def transfer_train_data(self, X_train, y_train):
        """学習データの送信
        Args:
           X_train: 学習データ
           y_train: 学習ラベル
        """
        # データを送信
        pb = make_pb_from_doubleList(X_train)
        self.stub.svr_transfer_x_train(pb)
        pb = make_pb_from_doubleList(y_train)
        self.stub.svr_transfer_y_train(pb)

    def predict(self, X):
        """予測の開始（事前に学習を完了させることが必要）"""

        pb = make_pb_from_doubleList(X)
        self.stub.svr_transfer_x_test(pb)

        pb_message = common_model.message_pb2.PB_Message()
        pb_support_length = self.stub.svr_initialize_kernel_for_predict(pb_message)
        support_length = pb_support_length.number
        for i in tqdm(range(support_length), desc="1/1 kernel"):
            self.stub.svr_predict_one_batch(pb_message)
        self.stub.svr_predict_after_batch(pb_message)
        pb_output = self.stub.svr_get_y_hat(pb_message)
        y_hat_enc = np.array(pb_output.y)
        return y_hat_enc

    def predict_already_send(self):
        """予測の開始（事前に学習を完了させることが必要）"""

        pb_message = common_model.message_pb2.PB_Message()
        pb_support_length = self.stub.svr_initialize_kernel_for_predict(pb_message)
        support_length = pb_support_length.number
        for i in tqdm(range(support_length), desc="1/1 kernel"):
            self.stub.svr_predict_one_batch(pb_message)
        self.stub.svr_predict_after_batch(pb_message)
        pb_output = self.stub.svr_get_y_hat(pb_message)
        y_hat_enc = np.array(pb_output.y)
        return y_hat_enc

    def score(self, X, y):
        """作成したモデルと、引数の検証データでのMSEの計算
        Args:
            X: 検証データ
            y: 検証ラベル
        """
        pb = make_pb_from_doubleList(X)
        self.stub.svr_transfer_x_predict(pb)

        pb_message = common_model.message_pb2.PB_Message()
        pb_support_length = self.stub.svr_initialize_kernel_for_predict(pb_message)
        support_length = pb_support_length.number
        for i in tqdm(range(support_length), desc="1/1 kernel"):
            self.stub.svr_predict_one_batch(pb_message)
        self.stub.svr_predict_after_batch(pb_message)

        pb_input = svr_model.svrTrainInput_pb2.PB_SvrTrainInput()
        X_test_ravel = np.ravel(X)
        pb_input.X[:] = X_test_ravel
        pb_input.index = X.shape[1]
        pb_input.y[:] = y
        self.stub.score_initialize_for_svr(pb_input)
        tmp = self.stub.score_for_svr(common_model.message_pb2.PB_Message()).data
        mse = np.array(tmp)
        return mse

    def score_of_self(self):
        tmp = self.stub.score_self_for_svr(common_model.message_pb2.PB_Message()).data
        mse = np.array(tmp)
        return mse

    def score_for_test(self):
        """作成したモデルと、引数の検証データでのMSEの計算
        Args:
            X: 検証データ
            y: 検証ラベル
        """
        pb_message = common_model.message_pb2.PB_Message()
        self.stub.svr_score_initialize_for_test(pb_message)
        tmp = self.stub.svr_score_for_test(common_model.message_pb2.PB_Message()).data
        mse = np.array(tmp)
        return mse

    def _train_one_while(self, target_rate):
        pb_message = common_model.message_pb2.PB_Message()
        pb_support_length = self.stub.svr_initialize_kernel_function(pb_message)
        support_length = pb_support_length.number
        for i in tqdm(range(support_length), desc="1/3: kernel", leave=False):
            self.stub.svr_distance_one_batch(pb_message)
        self.stub.svr_make_kernel_function(pb_message)

        self.stub.svr_make_atone(pb_message)
        self.stub.svr_initialize_ata(pb_message)
        for i in tqdm(range(support_length + 1), desc="2/3 make problem", leave=False):
            self.stub.svr_make_ata_one_batch(pb_message)
        self.stub.svr_make_ata_after_batch(pb_message)

        for i in range(3):
            self.stub.svr_initialize_beta(pb_message)
            mse_first = self.score_of_self()
            for i in tqdm(range(10), desc="- decide learning_rate", leave=False):
                self.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
            mse_second = self.score_of_self()
            if mse_first < mse_second:
                tqdm.write("too large learning_rate. Multiply learning_rate by 0.9")
                pb_scale = common_model.double_pb2.PB_Double()
                pb_scale.data = 0.8
                self.stub.svr_change_learning_rate(pb_scale)
            else:
                break

        self.stub.svr_initialize_beta(pb_message)
        _mse = np.inf
        self.learning_epoch = 100000
        for i in tqdm(range(self.learning_epoch), desc="3/3 solve problem", leave=False):
            self.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
            if i % 100 == 0:
                mse = self.score_of_self()
                if i is not 0:
                    impl_mse = ((_mse - mse) / mse) * 100
                    tqdm.write(
                        "{}: MSE[{}]:{:.2f} -> MSE[{}]:{:.2f}, IMPROVEMENT:{:.2f}%".format(
                            i, i - 100, _mse, i, mse, impl_mse
                        ),
                    )
                    if impl_mse < target_rate or _mse < mse:
                        break
                _mse = mse

    def train_with_score(self):
        """作成したモデルと、学習データでのMSEの計算、scoreよりも高速（事前にデータの送信が必要）"""
        pb = self.stub.svr_apply_shape_x_train(common_model.message_pb2.PB_Message())
        x_train_shape = np.array(pb.data)

        # prepare config
        pb_config = svr_model.svrTrainConfig_pb2.PB_SvrTrainConfig()
        if self.gamma == "auto":
            self.gamma = 1 / x_train_shape[1]
        pb_config.gamma = self.gamma
        pb_config.C = self.C
        pb_config.epsilon = self.epsilon
        self.stub.svr_upload_config(pb_config)

        pb_message = common_model.message_pb2.PB_Message()
        # 学習開始
        if self.epsilon is 0:
            tqdm.write("1/1 training")
            self._train_one_while(target_rate=self.second_impl_rate)

        if self.epsilon is not 0:
            tqdm.write("1/2 training")
            self._train_one_while(target_rate=self.first_impl_rate)
            # サポートベクターを更新
            self.stub.update_support_for_training(pb_message)
            tqdm.write("2/2 training")
            # 再度学習開始
            self._train_one_while(target_rate=self.second_impl_rate)

    def get_model(self):
        """サーバー上にあるモデルを取得する。"""
        plain_model = LSSVR_GRAD()
        plain_model.C = self.C
        plain_model.epsilon = self.epsilon
        plain_model.gamma = self.gamma
        pb_message = common_model.message_pb2.PB_Message()
        enc_model = self.stub.debug_insecure_get_model_for_training(pb_message)
        bias = np.array(enc_model.bias)
        alpha = np.array(enc_model.alpha)

        plain_model.bias_ = bias
        plain_model.alpha_ = alpha
        sv_np = np.array(enc_model.sv)
        sv_reshape = sv_np.reshape(int(enc_model.n_sv), -1)
        plain_model.support_vectors_ = sv_reshape
        return plain_model

    def transfer_model(self, model):
        # transfer config
        self.C = model.C
        self.epsilon = model.epsilon
        self.gamma = model.gamma
        pb_config = svr_model.svrTrainConfig_pb2.PB_SvrTrainConfig()
        pb_config.gamma = self.gamma
        pb_config.C = self.C
        pb_config.epsilon = self.epsilon
        self.stub.svr_upload_config(pb_config)

        pb = make_pb_from_doubleList(model.support_vectors_)
        self.stub.svr_transfer_sv(pb)

        beta = np.append(model.bias_, model.alpha_)
        pb = make_pb_from_doubleList(beta)
        self.stub.svr_transfer_beta(pb)

    def save_model_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.svr_save_model(pb_message)

    def save_model_on_storage(self, container_id: str, filename: str) -> Tuple[str, str]:
        """
        リモートサーバーにモデルを保存する

        Parameters
        ----------
        container_id : string
            モデルを格納するコンテナID
        filename : string
            モデルのファイル名（フルパスでない）

        Returns
        -------
        container_id : string
            モデルを格納するコンテナID
        blob_name : string
            モデルを保存した際のUUID
        """

        filepath = os.path.join(os.getenv("MODEL_DIR_PATH", "/cf_saas/.store/models/svr"), filename)
        # container_id = os.getenv("MODEL_CONTAINER_ID", "model")
        blobmessage = make_AzureBlobMessage(container_id, filepath)
        print(blobmessage)
        blobmessage = self.stub_storage_service.save_data_on_azure_blob(blobmessage)
        return blobmessage.container_id, blobmessage.blob_name

    def load_model_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.svr_load_model(pb_message)

    def load_model_on_storage(self, container_id: str, filename: str, blob_name: str) -> str:
        """
        リモートサーバーからモデルを取得する

        Parameters
        ----------
        container_id : string
            モデルを格納するコンテナID
        filename : string
            モデルのファイル名（フルパスでない）
        blob_name : string
            モデルを保存した際のUUID

        Returns
        -------
        container_id : string
            モデルが格納されているコンテナID
        filepath : string
            モデルを保存したローカルのフルパス
        """

        filepath = os.path.join(os.getenv("MODEL_DIR_PATH", "/cf_saas/.store/models/svr"), filename)
        # blobmessage.container_id = os.getenv("MODEL_CONTAINER_ID", "model")
        blobmessage = make_AzureBlobMessage(container_id, filepath, blob_name)
        print(blobmessage)
        blobmessage = self.stub_storage_service.load_data_on_azure_blob(blobmessage)
        return filepath

    def gen_permutation_for_predict(self):
        self.stub.svr_gen_permutation_for_predict(common_model.message_pb2.PB_Message())

    def get_y_hat_with_permutation(self):
        pb_message = common_model.message_pb2.PB_Message()
        pb_output = self.stub.svr_get_y_hat_with_permutation(pb_message)
        y_hat_enc = np.array(pb_output.data)
        return y_hat_enc


if __name__ == "__main__":
    pass
