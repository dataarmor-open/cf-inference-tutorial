import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
import numpy as np
import pickle

from grpc_service import input_client_pb2_grpc

from grpc_service import common_model
from grpc_service import noise_model

import data
import constants_client as c
import pickle

def upload_lir_model_raw(stub, model_index):
    with open(f"{c.LIR_RESULT_FOLDER_FOR_GA}/model{model_index}.pkl", "rb") as f:
        model = pickle.load(f)

    pb_w_b = common_model.doubleList_pb2.PB_DoubleList()
    w_b_np = np.zeros(c.ATTRS+1)
    w_b_np[:c.ATTRS] = model.coef_
    w_b_np[c.ATTRS] = model.intercept_
    pb_w_b.data[:] = w_b_np
    pb_w_b.model_index = model_index

    stub.upload_weight_and_bias_for_lir(pb_w_b)

def update_lir_model_raw(stub, model_index):
    with open(f"{c.LIR_RESULT_FOLDER_FOR_GA}/model{model_index}.pkl", "rb") as f:
        model = pickle.load(f)

    pb_w_b = common_model.doubleList_pb2.PB_DoubleList()
    w_b_np = np.zeros(c.ATTRS+1)
    w_b_np[:c.ATTRS] = model.coef_
    w_b_np[c.ATTRS] = model.intercept_
    pb_w_b.data[:] = w_b_np
    pb_w_b.model_index = model_index

    stub.upload_weight_and_bias_for_lir(pb_w_b)

def upload_lir_model(stub, model_index):
    with open(f"{c.LIR_RESULT_FOLDER_FOR_GA}/w_b{model_index}.pkl", "rb") as f:
        model = pickle.load(f)

    pb_w_b = common_model.doubleList_pb2.PB_DoubleList()
    w_b_np = np.zeros(c.ATTRS+1)
    w_b_np[:c.ATTRS] = model[:c.ATTRS]
    w_b_np[c.ATTRS] = model[-1]
    pb_w_b.data[:] = w_b_np
    pb_w_b.model_index = model_index

    stub.upload_weight_and_bias_for_lir(pb_w_b)

def update_lir_model(stub, model_index):
    with open(f"{c.LIR_RESULT_FOLDER_FOR_GA}/w_b{model_index}.pkl", "rb") as f:
        model = pickle.load(f)

    pb_w_b = common_model.doubleList_pb2.PB_DoubleList()
    w_b_np = np.zeros(c.ATTRS+1)
    w_b_np[:c.ATTRS] = model[:c.ATTRS]
    w_b_np[c.ATTRS] = model[-1]
    pb_w_b.data[:] = w_b_np
    pb_w_b.model_index = model_index

    stub.upload_weight_and_bias_for_lir(pb_w_b)


def main():
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    upload_lir_model(stub, 0)
    upload_lir_model(stub, 1)
    pb = noise_model.noise_query_pb2.PB_NoiseQuery(index=0)
    stub.set_noise_index(pb)
    pb = noise_model.noise_query_pb2.PB_NoiseQuery(index=1)
    stub.set_noise_index(pb)

if __name__ == "__main__":
    print("hello, world")
    main()