from grpc.tools import protoc
import glob

protos = glob.glob("./*.proto")
protos.extend(glob.glob("common_model/*.proto"))
protos.extend(glob.glob("lir_model/*.proto"))
protos.extend(glob.glob("lor_model/*.proto"))
protos.extend(glob.glob("svr_model/*.proto"))
protos.extend(glob.glob("tree_model/*.proto"))
protos.extend(glob.glob("ga_model/*.proto"))
protos.extend(glob.glob("kmeans_model/*.proto"))
protos.extend(glob.glob("noise_model/*.proto"))

for path in protos:
    protoc.main(
        (
            '',
            '-I.',
            '--python_out=./grpc_service',
            '--grpc_python_out=./grpc_service',
            f'./{path}',
        )
    )

