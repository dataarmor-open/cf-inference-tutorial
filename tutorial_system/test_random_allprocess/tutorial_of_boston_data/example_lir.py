import sys

import grpc
import numpy as np
import pickle
import time

import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.datasets import make_regression


sys.path.append("../../")
from gateai_connector.enc_lssvr import ENC_LSSVR
from gateai_connector.enc_preprocess import ENC_PREPROCESS
import constants_client as c
from gateai_connector.enc_lir import ENC_LIR


if __name__ == "__main__":
    # initialize and keygen of preprocess and lssvr
    preprocess = ENC_PREPROCESS()
    preprocess.reset()
    preprocess.keygen()
    print("debug1")

    lir0 = ENC_LIR(model_index=0)
    lir0.set_noise_index()
    lir0.set_config()
    lir0.reset_training()
    lir0.keygen_level1()
    lir0.transfer_ces_from_preprocess_to_lir()
    print("debug2")

    # generate test data
    boston = load_boston()
    ds = boston.data.shape[0]

    xs_1 = boston.data[0 : int(ds // 2)]
    ys_1 = boston.target[0 : int(ds // 2)]
    xs_2 = boston.data[int(ds // 2) :]
    ys_2 = boston.target[int(ds // 2) :]
    print("debug3")

    # preprocess
    preprocess.transfer_data(xs_1, ys_1)
    preprocess.transfer_data(xs_2, ys_2)
    preprocess.train_test_split(train_size=0.8)
    preprocess.min_max_scaler()
    preprocess.send_lir()
    print("debug4")

    model_name = "tmp_model1"
    lir0.fit_level1(model_name)
    print("debug5")
