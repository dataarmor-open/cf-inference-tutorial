# 畳み込みニューラルネットモデルのコンパイルチュートリアル


## 概要
このチュートリアルでは、  
`Keras によって生成された畳み込みニューラルネットのモデル`を、  
`GateAIのAPIを使用して秘密計算モデルへとコンパイル`します。  
`コンパイルされた秘密計算モデルは自動的にGateAIサービス群にホスティング`され、  
`ユーザはそのモデルに対して暗号化した入力を送信、結果を得る`。

という流れのチュートリアルとなっています。


## チュートリアルに必要なサービス

GateAI Compile のサービス群が必要となります。

- `input_client`
- `model_client`
- `calculation`
- `calculation_with_key`


## チュートリアルの手順

### 1. GateAIサービス群を立ち上げ、テスト環境に入る

手元に`gateai_keras` とタグがついたDocker イメージを取得した状態で、

```
make folder
make start_all && make start_client && make connect_client
```

を実行します。
実行した段階で、

```
root@8dc99ca218ea:/client/cf-inference-tutorial#
```

コンソール上でテストクライアントのコンテナ環境に入ることができます。


### 2. テストコンテナ内での準備

```
 source setup.sh
 cd tutorial_compiler/tutorial_mnist
```

とし、

```
python train.py
```

を実行します。  
実行すると、`nn_iris.h5` が生成されます。  
このファイルは、Keras 使って平文で生成されたモデルです。  
モデルの詳細を確認したい方は、`train.py`の詳細をご確認ください。


### 3. 鍵生成

```
python keygen.py
```

により、`input_client`にて秘密計算に必要な鍵ペアを生成します。


### 4. モデルのアップロード

```
python upload_model.py
```

により、`model_client`に学習されたモデルをアップロードします。  
`model_client`はモデルを秘密計算可能なモデルへとコンパイルし、  
`calculation`, `calculation_with_key`へと配布します。  

モデルは自動的に入力を待つ状態でホスティングされます。



### 5. 暗号を入力とした推論の実行

```
python predict.py
```

を実行します。

まず、データはクライアントから`input_client`へと送信され、暗号化を施されます。  
暗号化されたデータは`calculation_with_key`, `calculation` のサーバでやりとりされ、  
最終的にモデルの演算が全て完了すると、暗号文は `input_client`へと返却されます。　　

`input_client`は返却された暗号を復号し、クライアントへと戻します。  

チュートリアル実行時の標準出力には、  

```
===============
cipher_result
[1.68797394e-06 6.11690821e-08 9.19264865e-07 1.21633842e-10
 1.48288428e-04 3.94961552e-06 9.99844694e-01 1.15784007e-10
 3.03256087e-07 9.58644708e-08]
6

===============
raw_result
[[1.6879756e-06 6.1169210e-08 9.1926586e-07 1.2163423e-10 1.4828864e-04
  3.9496263e-06 9.9984467e-01 1.1578449e-10 3.0325677e-07 9.5864628e-08]]
6

===============
time for cipher prediction >>> 0.17865514755249023
```

というような出力が表示されます。

- `cipher_result`はGateAIを用いたときの推論結果
- `raw_result`は対照実験として、Scikitlearnを用いて平文で推論したときの結果
- `time for cipher prediction`はGateAIでの予測にかかった時間


を表示しています。

これでチュートリアルは終わりとなります。

お疲れ様でした。


