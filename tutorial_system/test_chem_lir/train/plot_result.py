import numpy as np
import matplotlib.pyplot as plt
import pickle
import constants_client as c

def main():
    for i in range(2):
        with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/loss_label{i}.pkl", "rb") as f:
            loss_label_list = pickle.load(f)

        with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/loss_w_b{i}.pkl", "rb") as f:
            loss_w_b_list = pickle.load(f)
        

        plt.plot(np.arange(len(loss_label_list)), loss_label_list)
        plt.savefig(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/loss_label{i}.png")
        plt.close()
        plt.plot(np.arange(len(loss_w_b_list)), loss_w_b_list)
        plt.savefig(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/loss_w_b{i}.png")
        plt.close()


if __name__ == "__main__":
    print("hello, world")

    main()

