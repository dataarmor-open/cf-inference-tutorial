import { ENC_LIR } from "../../enc_lir.js"
import { ENC_PREPROCESS } from "../../enc_preprocess.js"
import {attrs, bs, ds, ss, eta, update_option} from "./constants.js"

const proto_path = '/Users/kmihara/Documents/cf-inference-tutorial/tutorial_system/proto/input_client.proto'
const input_client_host = "localhost"
const input_client_port = "5101"


const preprocess = new ENC_PREPROCESS(proto_path, input_client_host, input_client_port)
const lir = new ENC_LIR(proto_path, input_client_host, input_client_port, 0)

await preprocess.keygen()
await preprocess.save_key_on_server("tutorial")



await lir.reset_training()
await lir.set_config(attrs, bs, ss, eta, update_option)
await lir.keygen()
await lir.save_key_on_server("tutorial")
await lir.transfer_ces_from_preprocess_to_lir()
