import { dynamic_load_input_client, f64_matrix, f64_vector, get_train_test_indices } from "./utils.js"

class ENC_PREPROCESS {
    constructor(proto_path, input_client_host, input_client_port) {
        this.stub = dynamic_load_input_client(proto_path, input_client_host, input_client_port)
    }

    async keygen() {
        // this.stub.preprocess_gen_key({}, (err, response) => {
        // })
        return new Promise((resolve, reject) => this.stub.preprocess_gen_key({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_gen_key: done")
            resolve(response)
        }))
    }

    async save_key_on_server(name) {
        return new Promise((resolve, reject) => this.stub.preprocess_save_secret_key({ message: name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_save_secret_key: done")
            resolve(response)
        }))
    }

    async load_key_on_server(name) {
        return new Promise((resolve, reject) => this.stub.preprocess_load_secret_key({ message: name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_load_secret_key: done")
            resolve(response)
        }))
    }

    async reset() {
        return new Promise((resolve, reject) => this.stub.preprocess_reset_data({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_reset_data: done")
            resolve(response)
        }))
    }

    async transfer_data(x, y, axis = 0) {
        await this._transfer_data_x(x, axis)
        await this._transfer_data_y(y, axis)
    }

    async _transfer_data_x(x, axis = 0) {
        if (x.constructor !== f64_matrix) {
            throw ("invalid type")
        }

        const pb_x = {
            data: Array.from(x.data),
            size: x.attrs,
        }

        if (axis == 0) {
            return new Promise((resolve, reject) => this.stub.preprocess_transfer_x_vstack(pb_x, (err, response) => {
                if (err) {
                    return reject(err)
                }
                console.log("preprocess_transfer_x_vstack: done")
                resolve(response)
            }))
        }
        else {
            return new Promise((resolve, reject) => this.stub.preprocess_transfer_x_hstack(pb_x, (err, response) => {
                if (err) {
                    return reject(err)
                }
                console.log("preprocess_transfer_x_hstack: done")
                resolve(response)
            }))

        }
    }

    async _transfer_data_y(y, axis = 0) {
        if (y.constructor !== f64_vector) {
            throw ("invalid type")
        }

        const pb_y = {
            data: Array.from(y.data),
            size: 0
        }

        if (axis == 0) {
            return new Promise((resolve, reject) => this.stub.preprocess_transfer_y_vstack(pb_y, (err, response) => {
                if (err) {
                    return reject(err)
                }
                console.log("preprocess_transfer_y_vstack: done")
                resolve(response)
            }))
        }
        else {
            return new Promise((resolve, reject) => this.stub.preprocess_transfer_y_hstack(pb_y, (err, response) => {
                if (err) {
                    return reject(err)
                }
                console.log("preprocess_transfer_y_hstack: done")
                resolve(response)
            }))

        }
    }

    async reset() {
        return new Promise((resolve, reject) => this.stub.preprocess_reset_data({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_reset_data: done")
            resolve(response)
        }))
    }

    async _shape_x() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_shape_x({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_shape_x: done")
            resolve(response.data)
        }))
    }

    async _shape_y() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_shape_y({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_shape_y: done")
            resolve(response.number)
        }))
    }

    async _preprocess_apply_min_of_x_train() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_min_of_x_train({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_min_of_x_train: done")
            resolve(response)
        }))
    }

    async _preprocess_apply_max_of_x_train() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_max_of_x_train({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_max_of_x_train: done")
            resolve(response)
        }))
    }

    async _preprocess_apply_fit_minmax() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_fit_minmax({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_fit_minmax: done")
            resolve(response)
        }))
    }

    async train_test_split() {
        const x_shape = await this._shape_x()
        const y_shape = await this._shape_y()
        if (x_shape[0] !== y_shape) {
            throw (`shape mismatch between train_xs and train_ys: ${x_shape[0]} != ${y_shape}`)
        }

        const train_test_indices = get_train_test_indices(x_shape[0])

        const pb_send = {
            train: train_test_indices[0],
            test: train_test_indices[1],

        }
        return new Promise((resolve, reject) => this.stub.preprocess_train_test_split_with_indices(pb_send, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_train_test_split_with_indices: done")
            resolve(response)
        }))
    }

    async minmax_scaler_fit() {
        await this._preprocess_apply_min_of_x_train()
        await this._preprocess_apply_max_of_x_train()
        await this._preprocess_apply_fit_minmax()
    }

    async save_minmax_scaler_on_server(name) {
        return new Promise((resolve, reject) => this.stub.preprocess_save_minmax_scaler({ message: name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_save_minmax_scaler: done")
            resolve(response)
        }))
    }

    async minmax_scaler_transform_x_train() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_minmax_transform_x_train({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_minmax_transform_x_train: done")
            resolve(response)
        }))

    }

    async minmax_scaler_transform_x_test() {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_minmax_transform_x_test({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_minmax_transform_x_test: done")
            resolve(response)
        }))

    }

    async _transfer_data_to_lir_from_preprocess() {
        return new Promise((resolve, reject) => this.stub.transfer_data_to_lir_from_preprocess({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("transfer_data_to_lir_from_preprocess: done")
            resolve(response)
        }))

    }

    async _lir_convert_preprocess_data() {
        return new Promise((resolve, reject) => this.stub.lir_convert_preprocess_data({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_convert_preprocess_data: done")
            resolve(response)
        }))
    }

    async send_lir() {
        await this._transfer_data_to_lir_from_preprocess()
        await this._lir_convert_preprocess_data()
    }

    async load_minmax_scaler_on_server(name) {
        return new Promise((resolve, reject) => this.stub.preprocess_load_minmax_scaler({ message: name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_load_minmax_scaler: done")
            resolve(response)
        }))
    }

    async transfer_x_for_predict(x) {
        const pb_x = {
            data: Array.from(x.data),
            size: x.attrs
        }

        return new Promise((resolve, reject) => this.stub.transfer_x_test(pb_x, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("transfer_x_test: done")
            resolve(response)
        }))

    }

    async minmax_scaler_transform_x_test(x) {
        return new Promise((resolve, reject) => this.stub.preprocess_apply_minmax_transform_x_test({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("preprocess_apply_minmax_transform_x_test: done")
            resolve(response)
        }))
    }

    async _transfer_x_test_to_lir_from_preprocess() {
        return new Promise((resolve, reject) => this.stub.transfer_x_test_to_lir_from_preprocess({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("transfer_x_test_to_lir_from_preprocess: done")
            resolve(response)
        }))
    }

    async _lir_convert_preprocess_x_test() {
        return new Promise((resolve, reject) => this.stub.lir_convert_preprocess_x_test({}, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_convert_preprocess_x_test: done")
            resolve(response)
        }))

    }

    async send_lir_only_test() {
        await this._transfer_x_test_to_lir_from_preprocess()
        await this._lir_convert_preprocess_x_test()
    }
}

export { ENC_PREPROCESS }





