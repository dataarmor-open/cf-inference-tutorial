# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: common_model/indexDoubleList.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\"common_model/indexDoubleList.proto\"N\n\x12PB_IndexDoubleList\x12\x0c\n\x04\x64\x61ta\x18\x01 \x03(\x01\x12\x0c\n\x04size\x18\x02 \x01(\x05\x12\r\n\x05\x61ttrs\x18\x03 \x01(\x05\x12\r\n\x05index\x18\x04 \x01(\x05\x62\x06proto3')



_PB_INDEXDOUBLELIST = DESCRIPTOR.message_types_by_name['PB_IndexDoubleList']
PB_IndexDoubleList = _reflection.GeneratedProtocolMessageType('PB_IndexDoubleList', (_message.Message,), {
  'DESCRIPTOR' : _PB_INDEXDOUBLELIST,
  '__module__' : 'common_model.indexDoubleList_pb2'
  # @@protoc_insertion_point(class_scope:PB_IndexDoubleList)
  })
_sym_db.RegisterMessage(PB_IndexDoubleList)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PB_INDEXDOUBLELIST._serialized_start=38
  _PB_INDEXDOUBLELIST._serialized_end=116
# @@protoc_insertion_point(module_scope)
