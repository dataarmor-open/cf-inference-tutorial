import sys
import os

import grpc
sys.path.append("../")
from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc
from grpc_service import common_model

from sklearn.model_selection import train_test_split

from tabnanny import check
import constants_client as c
import time
import itertools
import shutil
from tqdm import tqdm
import random
import numpy as np
import pickle

from lir import LIR
from ga import GA
from kmeans import KMEANS

import optimization

import data

if c.IS_FIXING_RANDOM_SEED:
    random.seed(c.RANDOM_SEED)
    np.random.seed(c.RANDOM_SEED)

def check_result_folder_overwrite(target):
    if target == "lir":
        target_folder = c.LIR_TRAIN_RESULT_FOLDER_NAME
    elif target == "ga":
        target_folder = c.GA_RESULT_FOLDER_NAME
    elif target == "kmeans":
        target_folder = c.KMEANS_RESULT_FOLDER_NAME
    elif target == "lir_grid_search":
        target_folder = c.LIR_GRID_SEARCH_RESULT_FOLDER
    else:
        raise Exception("target_folder has something wrong")
    if os.path.exists(target_folder):
        ans = input(f"target_folder '{target_folder}' exists, this process will overwrite the result, are you sure? (y/N) ")
        if ans.lower() not in ["y", "yes"]:
            print(f"ABORT.\n")
            quit()
        else:
            print("PROCEED WITH RESULT OVERWRITE.\n")
            time.sleep(2)
            shutil.rmtree(target_folder)
    os.mkdir(target_folder)



def lir_train_main():
    from train import train_raw, test_train, test_keygen, test_initialize, test_set_config, plot_result

    if c.IS_GRID_SEARCH:
        input("\ngrid_search will be executed, just to confirm, please hit enter.")
        grids = itertools.product(c.ETA_LIST, c.LMB_LIST)
        for condition in grids:
            c.ETA = condition[0]
            c.LMB = condition[1]
            c.LIR_TRAIN_RESULT_FOLDER_NAME=f"{c.LIR_GRID_SEARCH_RESULT_FOLDER}/result_{c.ETA}_{c.LMB}"
    
            check_result_folder_overwrite(target="lir")

            train_raw.main()
            test_keygen.main()
            test_set_config.main()
            executed_without_overflow = test_train.main()
            if not all(executed_without_overflow):
                with open(f"{c.LIR_GRID_SEARCH_RESULT_FOLDER}/overflow_log.txt", "a") as f:
                    text = ", ".join(map(str [c.ETA, c.LMB, "OVERFLOW DETECTED!!"]))
                    text = text + "\n"
                    f.write(text)
            else:
                with open(f"{c.LIR_GRID_SEARCH_RESULT_FOLDER}/overflow_log.txt", "a") as f:
                    text = ", ".join(map(str, [c.ETA, c.LMB, "NO overflow detected"]))
                    text = text + "\n"
                    f.write(text)

            plot_result.main()
    
    else:
        input("\ngrid_search will NOT be executed, just to confirm, please hit enter.")
        check_result_folder_overwrite(target="lir")

        train_raw.main()
        test_keygen.main()
        test_set_config.main()
        executed_without_overflow = test_train.main()
        plot_result.main()


def old_api():
    lir_train_main()
    quit()
    from optimization import test_keygen, test_set_config, test_upload, test_ga, test_ga_result_compare
    from optimization import test_kmeans, test_predict_kmeans_result
    check_result_folder_overwrite(target="ga")
    check_result_folder_overwrite(target="kmeans")
    test_keygen.main()
    test_set_config.main()
    test_upload.main()
    test_ga.main()
    test_ga_result_compare.main()
    test_kmeans.main()
    test_predict_kmeans_result.main()

def test_lir(stub):

    lmb_list = [0.01, 0.02, 0.03]
    lir0 = LIR(stub=stub, model_index=0)
    lir0.reset_training()
    #lir1 = LIR(stub=stub, model_index=1)

    lir0.keygen_level1()
    xs, ys1, ys2 = data.get_data()
    ys = ys1
    xs_train, xs_test, ys_train, ys_test = train_test_split(xs, ys, test_size=0.2, random_state=10)

    for lmb in lmb_list:
        lir0.lmb = lmb
        lir0.set_config()
        lir0.reset_training_data()

        lir0.upload_train_data(xs_train, ys_train)
        lir0.upload_test_data(xs_test, ys_test)

        model_name = f"test_model_lmb_{lmb}"
        lir0.fit_level1(model_name)


    lir1 = LIR(stub=stub, model_index=1)
    lir1.reset_training()

    lir1.keygen_level1()
    xs, ys1, ys2 = data.get_data()
    ys = ys2
    xs_train, xs_test, ys_train, ys_test = train_test_split(xs, ys, test_size=0.2, random_state=10)

    for lmb in lmb_list:
        lir1.lmb = lmb
        lir1.set_config()
        lir1.reset_training_data()

        lir1.upload_train_data(xs_train, ys_train)
        lir1.upload_test_data(xs_test, ys_test)

        model_name = f"test_model_lmb_{lmb}"
        lir1.fit_level1(model_name)



def test_ga(stub, stub_noise):
    lir0 = LIR(stub=stub, model_index=0)
    lir1 = LIR(stub=stub, model_index=1)
    lirs = [lir0, lir1]
    with open("result/model0.pkl", "rb") as f:
        model = pickle.load(f)
    lir0.keygen_for_inference()
    lir0.set_config_for_inference_for_optimization()
    lir0.upload_existing_model(model)

    with open("result/model1.pkl", "rb") as f:
        model = pickle.load(f)
    lir1.keygen_for_inference()
    lir1.set_config_for_inference_for_optimization()
    lir1.upload_existing_model(model)


    #ga = GA(stub=stub, model_index=0, prediction_model=lir, noise_stub=stub_noise)
    ga = GA(stub=stub, model_index=[0, 1], prediction_models=lirs, noise_stub=stub_noise)
    ga.keygen()
    ga.set_config()

    ga.apply()

def test_ga_after_train(stub, stub_noise):
    lir0 = LIR(stub=stub, model_index=0)
    lir1 = LIR(stub=stub, model_index=1)
    lirs = [lir0, lir1]
    lir0.set_config_for_inference_for_optimization()
    lir1.set_config_for_inference_for_optimization()

    ga = GA(stub=stub, model_index=[0, 1], prediction_models=lirs, noise_stub=stub_noise)
    ga.keygen()
    ga.set_config()
    ga.apply()

def test_kmeans(stub, stub_noise, ga):
    chosen_data, chosen_ys = ga.extract_data_from_all_generations()

    kmeans = KMEANS(stub, len(chosen_data))
    kmeans.keygen()
    kmeans.set_config()
    kmeans.upload_datas(chosen_data)
    kmeans.upload_initial_centroids()
    kmeans.apply()

if __name__ == "__main__":
    print("hello, main.py")

    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    ## noise サービスへの接続を確立(チュートリアルのために接続、普通は接続を制限している)
    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)

    test_lir(stub)
    lir0 = LIR(stub=stub, model_index=0)
    lir0.get_models_info_all()
    print("\n\n========================")
    print(f"mae_info: {lir0.mae_info}")
    print(f"mse_info: {lir0.mse_info}")
    print(f"r2_info: {lir0.r2_info}")

    model_name = "test_model_lmb_0.01"
    lir0.transfer_model_to_inference_by_name(model_name)

    lir1 = LIR(stub=stub, model_index=1)
    lir1.get_models_info_all()
    print("\n\n========================")
    print(f"mae_info: {lir1.mae_info}")
    print(f"mse_info: {lir1.mse_info}")
    print(f"r2_info: {lir1.r2_info}")

    model_name = "test_model_lmb_0.01"
    lir1.transfer_model_to_inference_by_name(model_name)

    lir0 = LIR(stub=stub, model_index=0)
    lir1 = LIR(stub=stub, model_index=1)
    lirs = [lir0, lir1]
    lir0.set_config_for_inference_for_optimization()

    lir1.set_config_for_inference_for_optimization()

    test_ga(stub, stub_noise)


    #ga = GA(stub=stub, model_index=0, prediction_model=lir, noise_stub=stub_noise)
    ga = GA(stub=stub, model_index=[0, 1], prediction_models=lirs, noise_stub=stub_noise)

    test_kmeans(stub, stub_noise, ga)

