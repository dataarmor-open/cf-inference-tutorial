# Copyright (c) EAGLYS Inc. All rights reserved.

import pickle
import numpy as np

import keras
from keras.datasets import mnist
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Lambda, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.optimizers import RMSprop, Adam



def dispense_data():
    (pre_x_train, pre_y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_valid, y_train, y_valid = train_test_split(pre_x_train, pre_y_train, test_size=0.175)


    x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
    x_valid = x_valid.reshape(x_valid.shape[0], 28, 28, 1)

    y_train = keras.utils.to_categorical(y_train, 10)
    y_valid = keras.utils.to_categorical(y_valid, 10)


    return x_train, y_train, x_valid, y_valid


def dispense_model():
    model = Sequential()
    model.add(Conv2D(5, kernel_size=(3,3), use_bias=False, input_shape=(28,28,1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(10, use_bias=False, kernel_size=(3,3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10))
    model.add(Activation('softmax'))

    return model



def train_model(model, model_path, train_x, train_y, test_x, test_y):
    model.compile(loss='categorical_crossentropy',
                optimizer=Adam(),
                metrics=['accuracy'])
    history = model.fit(x_train, y_train,
                        batch_size=128,
                        epochs=3,
                        verbose=1,
                        validation_data=(x_valid, y_valid))
    model.save(model_path)



if __name__ == "__main__":
    model_name = 'cnn_mnist'
    model_path = f'{model_name}.h5'
    #model_path = f'./cnn_mnist.h5'
    x_train, y_train, x_valid, y_valid = dispense_data()
    model = dispense_model()
    model = train_model(model, model_path, x_train, y_train, x_valid, y_valid)
