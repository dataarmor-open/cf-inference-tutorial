# ダミーデータを用いた線形回帰モデルの学習、遺伝アルゴリズムの活用、kmeans クラスタリングの活用方法の解説

このチュートリアルを行うための準備編として、
データを準備します。

`tmp.csv`

が必要となりますので、準備の仕方を追記します。

### 検証用コンテナ内での準備


```
git pull
source setup.sh
cd tutorial_system/test_chem_lir
```

データを用意します。頂いたxlsxファイルを  

```
tutorial_system/test_chem_lir/dummy.xlsx
```

のように配置する必要があります。　　
検証用環境のコンテナは、`docker-compose_client.yml ` の

```
volumes:
- ./datas/:/client/cf-inference-tutorial/tutorial_system/datas
```

に記載の通り、  
チュートリアルレポジトリをクローンしたローカル環境とファイルシステムを共有していますので、  
ローカル環境の`cf-inference-tutorial/tutorial_system/datas`に`dummy.xlsx`を保存しておいて、

実行環境コンテナ内の`tutorial_system/test_chem_lir`にて

```
cp ../datas/dummy.xlsx .
```
等で実行環境コンテナ内でもdummy.xlsxにアクセスできるようにすることが可能です。  

実行環境コンテナ内の、`tutorial_system/test_chem_lir`  
に`dummy.xlsx` が用意できたら、


```
sh prepare_data.sh
```

を実行します。 実行が完了すると、これからチュートリアルで使用する　　

`tmp.csv`が生成されています。

確認できましたら、次に進んでください。


# 線形回帰モデルの学習の検証
`readme-lir-train.md` を参照してください。


# 遺伝アルゴリズム、およびkmeans クラスタリングを用いた分析の検証
`readme-optimization.md` を参照してください。




