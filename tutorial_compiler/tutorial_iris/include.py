import time
import grpc
import numpy as np
import pickle
import sys

import keras
from keras.utils import to_categorical, np_utils

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split


from grpc_python.input_client_pb2_grpc import InputClientStub
from grpc_python.model_client_pb2_grpc import ModelClientStub

from grpc_python.model.noParam_pb2 import NoParam
from grpc_python.model.tensor_pb2 import Tensor
from grpc_python.model.modelInfo_pb2 import ModelInfo
from grpc_python.model.modelBinaryKeras_pb2 import ModelBinaryKeras

import network_constants
import network_utils
