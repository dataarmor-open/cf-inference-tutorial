# Copyright (c) EAGLYS Inc. All rights reserved.

import time
import grpc
import numpy as np
import pickle
import sys
sys.path.append("../")

import keras
from keras.utils import to_categorical, np_utils
from keras.models import Model

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

from saas.input_client.service.input_client_pb2_grpc import InputClientStub
from saas.input_client.service.input_client_pb2 import NoParam, Tensor
from saas.model_client.service.model_client_pb2_grpc import ModelClientStub
from saas.model_client.service.model_client_pb2 import ModelInfo, ModelBinaryKeras

import network_constants
import network_utils

def dispense_data():
  iris = load_iris()

  data_x = iris.data
  data_y = iris.target
  data_y = np_utils.to_categorical(data_y)

  x_train, x_test, y_train, y_test = train_test_split(data_x, data_y, test_size=0.2)

  return x_train, x_test, y_train, y_test


if __name__ == "__main__":
  model_name = 'nn_iris'

  # set model index
  model_index = 0

  # raw inference for comparison
  model = keras.models.load_model("modelfiles/h5/nn_iris.h5")

  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  # call inpuot_client to generate keys
  print("call gen_key")
  api_input_client.gen_key(NoParam())

  # call model_client to upload h5 file from modelfiles/h5/{model_name}.h5
  print("call load_and_compile_model_from_local_h5")
  model_info = ModelBinaryKeras()
  model_info.config = pickle.dumps(model.get_config())
  model_info.weights = pickle.dumps(model.get_weights())
  model_info.type_info = 'sequential'
  model_info.intermediate_output = 'dense_1'
  model_info.model_index = model_index
  api_model_client.compile_model_from_binary_keras(model_info)


  # prepare data for inference
  x, y, _, _  = dispense_data()
  test_data = x[0]

  # call input_cleint to do inference
  print("call predict")

  t1 = time.time()

  data_for_cf = test_data.tolist()
  input_data = Tensor()
  input_data.data = pickle.dumps(data_for_cf)
  input_data.model_index = model_index
  input_data.is_receive_intermediate = True

  # call prediction
  result = api_input_client.predict(input_data)
  result = pickle.loads(result.data)

  t2 = time.time()

  # predict with raw input but output is from middle layer
  layer_name = 'dense_1'
  hidden_layer_model = Model(inputs=model.input, outputs=model.get_layer(layer_name).output)
  result_raw = hidden_layer_model.predict(test_data.reshape(1,4))

  print("\n===============")
  print("cipher_result")
  print(result)
  print("\n===============")
  print("raw_result")
  print(result_raw)

  print("\n===============")
  print(f'time for cipher prediction >>> {t2-t1}')



