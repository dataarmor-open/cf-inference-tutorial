# FROM cf_core_py:v1.5a AS builder
# ENV DEBIAN_FRONTEND=noninteractive

# COPY requirements.txt ./

# RUN source ~/.bashrc &&\
#     ~/.pyenv/versions/myenv/bin/pip install -r requirements.txt

# COPY . /cf-inference-tutorial
# WORKDIR /cf-inference-tutorial
# RUN source ~/.bashrc && pip install -r requirements.txt
# WORKDIR /cf-inference-tutorial/tutorial_system
# RUN cd proto && mkdir -p grpc_service && ~/.pyenv/versions/myenv/bin/python gen.py && cp sed.sh grpc_service/ && cd grpc_service && sh sed.sh && cd .. && cp -r grpc_service ../ 

FROM gateai_system:latest
WORKDIR /
COPY . /cf-inference-tutorial
WORKDIR /cf-inference-tutorial
WORKDIR /cf-inference-tutorial/tutorial_system
RUN cd proto && mkdir -p grpc_service && ~/.pyenv/versions/myenv/bin/python gen.py && cp sed.sh grpc_service/ && cd grpc_service && sh sed.sh && cd .. && cp -r grpc_service ../