import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston

sys.path.append("../../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lir import ENC_LIR

C = 10
epsilon = 0.1

C_plain = 10
epsilon_plain = 0.1
boston = load_boston()
xs = boston.data
ys = boston.target
x_train, x_test, y_train, y_test = train_test_split(xs, ys)

if __name__ == "__main__":
    preprocess = ENC_PREPROCESS()
    preprocess.load_key_on_server("tutorial_of_boston_data")
    preprocess.load_minmax_scaler_on_server("bostom_min_max")

    lir = ENC_LIR(model_index=0)
    lir.load_key_on_server("tutorial_of_boston_data")
    lir.load_model_on_server("boston_lir_model")

    preprocess.transfer_x_for_predict(x_test)
    preprocess.minmax_scaler_transform_x_test()
    preprocess.send_lir_only_test()
    y_hat = lir.predict_already_send()
    print(y_hat)
    print(y_test)
    print(len(y_hat))
    print(len(y_test))

    # print(
    #     "ENC_LIR: MSE = ",
    #     mean_squared_error(y_test, y_hat),
    #     ", MAE = ",
    #     mean_absolute_error(y_test, y_hat),
    #     ", R2 = ",
    #     r2_score(y_test, y_hat),
    # )
