import grpc
import pickle
import sys

sys.path.append("../")

from sklearn import datasets

from grpc_python import input_client_pb2_grpc
from grpc_python import model_client_pb2_grpc

from grpc_python.model import message_pb2
from grpc_python.model import randomForestModel_pb2
from grpc_python.model import treeParameter_pb2
from grpc_python.model import data_pb2

import network_utils

if __name__ == "__main__":
    print("hello, world")

    api_input_client = network_utils.get_input_client_stub()

    api_model_client = network_utils.get_model_client_stub()


    with open("rf_iris.pkl", "rb") as f:
      model = pickle.load(f)

    pb_for_upload_config = treeParameter_pb2.PB_TreeParameter()
    pb_for_upload_config.bl = 16
    pb_for_upload_config.multiplier = 1000
    api_model_client.upload_config(pb_for_upload_config)

    pb_for_compile_random_forest = randomForestModel_pb2.PB_RandomForestModel()
    pb_for_compile_random_forest.data = pickle.dumps(model)
    api_model_client.compile_random_forest(pb_for_compile_random_forest)

