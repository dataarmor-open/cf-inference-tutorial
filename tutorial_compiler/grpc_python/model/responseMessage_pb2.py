# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: model/responseMessage.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='model/responseMessage.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x1bmodel/responseMessage.proto\"%\n\x0fResponseMessage\x12\x12\n\nCipherText\x18\x01 \x03(\x0c\x62\x06proto3'
)




_RESPONSEMESSAGE = _descriptor.Descriptor(
  name='ResponseMessage',
  full_name='ResponseMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CipherText', full_name='ResponseMessage.CipherText', index=0,
      number=1, type=12, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=31,
  serialized_end=68,
)

DESCRIPTOR.message_types_by_name['ResponseMessage'] = _RESPONSEMESSAGE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ResponseMessage = _reflection.GeneratedProtocolMessageType('ResponseMessage', (_message.Message,), {
  'DESCRIPTOR' : _RESPONSEMESSAGE,
  '__module__' : 'model.responseMessage_pb2'
  # @@protoc_insertion_point(class_scope:ResponseMessage)
  })
_sym_db.RegisterMessage(ResponseMessage)


# @@protoc_insertion_point(module_scope)
