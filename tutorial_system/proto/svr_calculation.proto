syntax = "proto3";
import "common_model/encServiceEncoded.proto";
import "common_model/message.proto";
import "common_model/double.proto";
import "common_model/number.proto";
import "common_model/CKKSCmatrix.proto";
import "common_model/CKKSCvector.proto";
import "svr_model/svrTrainConfig.proto";
import "svr_model/svrTrainSupport.proto";
import "common_model/intList.proto";

import "common_model/CKKSCveclist.proto";

service SvrCalculation{
  // set up
  rpc transfer_ces_for_training(PB_EncServiceEncoded) returns (PB_Message){};
  rpc transfer_config_for_training(PB_SvrTrainConfig) returns (PB_Message){};
  rpc svr_reset_data(PB_Message) returns (PB_Message){};
  // training  
  rpc transfer_x_train(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_x_train_tile(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_y_train(PB_CKKSCvector) returns (PB_Message){};
  rpc transfer_sv(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_beta(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_shape_x_train(PB_Message) returns (PB_IntList){};
  rpc apply_initialize_distance_index_for_training(PB_Message) returns (PB_Message){};
  rpc apply_distance_index_for_training(PB_Message) returns (PB_CKKSCmatrix){};  
  rpc transfer_powered_K_for_training(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_leveled_K_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc transfer_K_for_training(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_D_for_training(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_D_trans_for_training(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_initialize_ata_index_for_training(PB_Message) returns (PB_Message){};
  rpc apply_make_ata_index_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc change_learning_rate(PB_Double) returns (PB_Message){};
  rpc apply_make_atone_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc transfer_ata_for_training(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_atone_for_training(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_initialize_beta(PB_Message) returns (PB_Message){};
  rpc apply_solve_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc transfer_beta_for_training(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_trace_for_training(PB_Double) returns (PB_Double){};
  rpc apply_y_hat_with_noise(PB_Message) returns (PB_CKKSCvector){};
  rpc transfer_y_hat_with_veclist(PB_CKKSCveclist) returns (PB_Message){};
  rpc gen_permutation_for_predict(PB_Message) returns (PB_Message){};
  rpc apply_y_hat_with_permutation(PB_Message) returns (PB_CKKSCveclist){};
  
  //predict
  rpc transfer_x_predict(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_initialize_distance_index_for_predict(PB_Message) returns (PB_Message){};
  rpc apply_distance_index_for_predict(PB_Message) returns (PB_CKKSCmatrix){};  
  rpc transfer_powered_K_for_predict(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_leveled_K_for_predict(PB_Message) returns (PB_CKKSCmatrix){};

  rpc transfer_D_for_predict(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_predict_for_predict(PB_Message) returns (PB_CKKSCmatrix){};
  rpc transfer_y_hat_for_predict(PB_CKKSCvector) returns (PB_Message){};

  //test
  rpc transfer_x_test(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_initialize_distance_index_for_test(PB_Message) returns (PB_Message){};
  rpc apply_distance_index_for_test(PB_Message) returns (PB_CKKSCmatrix){};  
  rpc transfer_powered_K_for_test(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_leveled_K_for_test(PB_Message) returns (PB_CKKSCmatrix){};
  rpc transfer_D_for_test(PB_CKKSCmatrix) returns (PB_Message){};
  rpc apply_predict_for_test(PB_Message) returns (PB_CKKSCmatrix){};
  
  rpc transfer_y_hat_for_test(PB_CKKSCvector) returns (PB_Message){};

  rpc apply_y_hat_from_training_kernel(PB_Message) returns (PB_CKKSCmatrix){};
  
  //update support
  rpc transfer_y_hat_from_training_kernel(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_diff_y_hat_and_y_train(PB_Message) returns (PB_CKKSCvector){};
  rpc transfer_support(PB_SvrTrainSupport) returns (PB_Message){};
  rpc apply_update_support_vector(PB_Message) returns (PB_Message){};

  //get length
  rpc listen_store_all_length_for_training(PB_Message) returns (PB_Number){};
  rpc listen_store_support_length_for_training(PB_Message) returns (PB_Number){};
  rpc listen_multiparty_length_sum(PB_Message) returns (PB_Number){};
  
  // debug function
  rpc debug_insecure_get_model_for_training(PB_Message) returns (PB_CKKSCvector){};
  rpc debug_insecure_get_y_hat(PB_Message) returns (PB_CKKSCvector){};
  rpc debug_insecure_get_support_for_training(PB_Message) returns (PB_SvrTrainSupport){};
  rpc debug_insecure_get_sv_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc debug_insecure_get_ata_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc debug_insecure_get_atone_for_training(PB_Message) returns (PB_CKKSCmatrix){};
  rpc debug_insecure_get_permutation_for_predict(PB_Message) returns (PB_IntList){};

  // about test predict mse
  rpc transfer_y_test(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_diff_y_hat_and_y_test_for_testing(PB_Message) returns (PB_CKKSCvector){};
  rpc transfer_diff_not_trans_y_hat_and_y_test_for_testing(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_mean_squared_error_for_testing(PB_Message) returns (PB_CKKSCvector){};

  // score self
  rpc apply_diff_y_hat_and_y_train_for_training(PB_Message) returns (PB_CKKSCvector){};
  rpc transfer_diff_y_hat_and_y_train_for_training(PB_CKKSCvector) returns (PB_Message){};
  rpc transfer_diff_not_trans_y_hat_and_y_train_for_training(PB_CKKSCvector) returns (PB_Message){};
  rpc apply_mean_squared_error_for_training(PB_Message) returns (PB_CKKSCvector){};

  rpc transfer_x_train_from_preprocess(PB_CKKSCveclist) returns (PB_Message){};
  rpc transfer_x_test_from_preprocess(PB_CKKSCveclist) returns (PB_Message){};
  rpc transfer_y_train_from_preprocess(PB_CKKSCvector) returns (PB_Message){};
  rpc transfer_y_test_from_preprocess(PB_CKKSCvector) returns (PB_Message){};

  rpc apply_x_train_from_preprocess(PB_Message) returns (PB_CKKSCveclist){};
  rpc apply_x_test_from_preprocess(PB_Message) returns (PB_CKKSCveclist){};
  rpc apply_y_train_from_preprocess(PB_Message) returns (PB_CKKSCvector){};
  rpc apply_y_test_from_preprocess(PB_Message) returns (PB_CKKSCvector){};

  rpc transfer_x_train_from_input(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_x_test_from_input(PB_CKKSCmatrix) returns (PB_Message){};
  rpc transfer_y_train_from_input(PB_CKKSCvector) returns (PB_Message){};
  rpc transfer_y_test_from_input(PB_CKKSCvector) returns (PB_Message){};

  rpc save_model(PB_Message) returns (PB_Message){};
  rpc load_model(PB_Message) returns (PB_Message){};
  rpc get_config(PB_Message) returns (PB_SvrTrainConfig){};
}