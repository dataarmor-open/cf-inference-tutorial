# GateAI Compiler Treeタイプのモデルのコンパイル

## 概要

GateAIは、Scikitlearn、LightGBMから生成される

- ランダムフォレスト (Scikitlearn)
- LightGBM Regression (LightGBM)
- LightGBM Multiclass (LightGBM)

のコンパイルをサポートしています。



## 必要になるDocker イメージ

上のタイプのモデルをコンパイルし、秘密計算を使用するためには  

`gateai_tree` のDocker イメージが必要となります。

上が準備できましたら以下のセッティングにお進みください。

## GateAIサービス群の立ち上げ

必要となるGateAIのサービスを立ち上げ、テスト環境を作ります。

### 1. GateAIサービス群を立ち上げ、テスト環境に入る

手元に`gateai_keras` とタグがついたDocker イメージを取得した状態で、

```
make folder
make start_all && make start_client && make connect_client
```

を実行します。
実行した段階で、

```
root@8dc99ca218ea:/client/cf-inference-tutorial#
```

コンソール上でテストクライアントのコンテナ環境に入ることができます。


### 2. テストコンテナ内での準備

```
 source setup.sh
 cd tutorial_compiler/tutorial_tree
```

以上でGateAIサービス群、およびクライアント環境の準備は整いました。


## チュートリアルフォルダ

チュートリアルとして、

- `test_lgbm_multiclass`
- `test_lgbm_regression`
- `test_random_forest`

の３つのフォルダを用意しています。

これらのチュートリアルのシナリオや、プログラムの詳細は  
フォルダ内のREADME.md
およびPythonのソースコードをご覧ください。

