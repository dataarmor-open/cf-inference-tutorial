# LightGBM（Regression)モデルのコンパイルチュートリアル


## 概要
このチュートリアルでは、  
`LightGBM によって生成されたLightGBM(Regression)のモデル`を、  
`GateAIのAPIを使用して秘密計算モデルへとコンパイル`します。  
`秘密計算モデルはGateAIサービス群へと自動的にホスティング`され、  
`ユーザはそのモデルに対して暗号化した入力を送信、結果を得る`。

という流れのチュートリアルとなっています。


## チュートリアルに必要なサービス

GateAI Compile のサービス群が必要となります。

- `input_client`
- `model_client`
- `calculation`
- `calculation_with_key`


## チュートリアルの手順


```
 cd test_lgbm_regression
```

とし、

```
python train.py
```

を実行します。  
実行すると、`lgbm_regression{i}.pkl` が生成されます。  
i は、モデルに含まれる木の数です。  
このファイルは、LightGBM 使って平文で生成されたモデルです。  
モデルの詳細を確認したい方は、`train.py`の詳細をご確認ください。


### 鍵生成

```
python keygen.py
```

により、`input_client`にて秘密計算に必要な鍵ペアを生成します。


### モデルのアップロード

```
python upload_model.py
```

により、`model_client`に学習されたモデルをアップロードします。  
`model_client`はモデルを秘密計算可能なモデルへとコンパイルし、  
`calculation`, `calculation_with_key`へと配布します。  

モデルは自動的に入力を待つ状態でホスティングされます。



### 暗号を入力とした推論の実行

```
python predict.py
```

を実行します。

まず、データはクライアントから`input_client`へと送信され、暗号化を施されます。  
暗号化されたデータは`calculation_with_key`, `calculation` のサーバでやりとりされ、  
最終的にモデルの演算が全て完了すると、暗号文は `input_client`へと返却されます。　　

`input_client`は返却された暗号を復号し、クライアントへと戻します。  

チュートリアル実行時の標準出力には、  

```
cipher_prediction_time: 2.331045150756836
raw_prediction_time   : 0.004664182662963867
cipher_res: 1.6478855611742351
raw_res   : 1.6479000903156724
y_res     : 1.537
```

というような出力が表示されます。

- `cipher_res`はGateAIを用いたときの推論結果
- `raw_res`は対照実験として、Scikitlearnを用いて平文で推論したときの結果
- `y_res`は正解ラベル
- `cipher_prediction_time`はGateAIでの予測にかかった時間
- `raw_prediction_time`はLightGBMでの平文での予測にかかった時間


を表示しています。

これでチュートリアルは終わりとなります。

お疲れ様でした。


