import { ENC_LIR } from "../../enc_lir.js"
import { ENC_PREPROCESS } from "../../enc_preprocess.js"
import { get_random_dataset } from "../../utils.js"

import { attrs, bs, ds, ss, eta, update_option } from "./constants.js"

const proto_path = '/Users/kmihara/Documents/cf-inference-tutorial/tutorial_system/proto/input_client.proto'
const input_client_host = "localhost"
const input_client_port = "5101"


const preprocess = new ENC_PREPROCESS(proto_path, input_client_host, input_client_port)
const lir = new ENC_LIR(proto_path, input_client_host, input_client_port, 0)

await preprocess.reset()
await preprocess.load_key_on_server("tutorial")
await lir.load_key_on_server("tutorial")


const dataset = get_random_dataset(attrs, ds)
const xs = dataset[0]
const ys = dataset[1]
const w = dataset[2]
const b = dataset[3]

xs.shape()
ys.shape()
w.shape()

await preprocess.transfer_data(xs, ys)

await preprocess.train_test_split()
await preprocess.minmax_scaler_fit()
await preprocess.save_minmax_scaler_on_server("tutorial")
await preprocess.minmax_scaler_transform_x_train()
await preprocess.minmax_scaler_transform_x_test()
await preprocess.send_lir()

await lir.fit_level1(10)
await lir.save_model_on_server("lir_model")