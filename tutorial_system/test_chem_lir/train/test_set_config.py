import sys
# import argparse 
sys.path.append("../../")

import grpc
from grpc_service import input_client_pb2_grpc
from grpc_service import lir_model

import constants_client as c


def main():
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    # parser = argparse.ArgumentParser(description='parameter for grid search') 
    # parser.add_argument("--ETA", help="learning rate")
    # parser.add_argument("--LMB", help="l2 regularization alpha")

    # args = parser.parse_args()

    for i in range(c.NUMBER_OF_GOALS):
        pb_config = lir_model.linearRegressionTrainConfig_pb2.PB_LinearRegressionTrainConfig()

        pb_config.attrs = c.ATTRS
        pb_config.bs = c.BS
        pb_config.ss = c.SS
        pb_config.eta = c.ETA
        pb_config.lmb = c.LMB
        pb_config.update_option = c.UPDATE_OPTION
        pb_config.is_accumulate_update = c.IS_ACCUMURATE_UPDATE
        pb_config.model_index = i

        if c.IS_USE_WARM_UP:
            pb_config.eta = c.INITIAL_ETA


        stub.upload_config_for_training_for_lir(pb_config)


if __name__ == "__main__":
    print("hello, world")
    main()
