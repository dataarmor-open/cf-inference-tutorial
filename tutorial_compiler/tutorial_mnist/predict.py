# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

def dispense_data():
  (pre_x_train, pre_y_train), (x_test, y_test) = mnist.load_data()
  x_train, x_valid, y_train, y_valid = train_test_split(pre_x_train, pre_y_train, test_size=0.2)

  x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
  x_valid = x_valid.reshape(x_valid.shape[0], 28, 28, 1)

  y_train = keras.utils.to_categorical(y_train, 10)
  y_valid = keras.utils.to_categorical(y_valid, 10)

  return x_train, y_train, x_valid, y_valid


if __name__ == "__main__":
  # set model index
  model_index = 2

  model = keras.models.load_model('cnn_mnist.h5')

  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()


  print("call prediction")
  # prepare data for inference
  x, y, _, _ = dispense_data()
  test_data = x[0]

  t1 = time.time()
  # measure time for cipher inference
  # expect data.shape as (c, h, w)
  data_for_cf = test_data.transpose(2,0,1)
  # expect data as list, not np.ndarray
  data_for_cf = data_for_cf.tolist()
  # to call predict, pack pickle.dump(data_for_cf) into Tensor.data

  # call prediction
  input_data = Tensor()
  input_data.data = pickle.dumps(data_for_cf)
  input_data.model_index = model_index

  result = api_input_client.predict(input_data)

  # load result
  result = pickle.loads(result.data)

  t2 = time.time()

  result_raw = model.predict(test_data.reshape(1, 28, 28, 1))


  print("\n===============")
  print("cipher_result")
  print(result)
  print(np.argmax(result))
  print("\n===============")
  print("raw_result")
  print(result_raw)
  print(np.argmax(result_raw))

  print("\n===============")
  print(f'time for cipher prediction >>> {t2-t1}')


