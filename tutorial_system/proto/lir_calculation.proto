syntax = "proto3";

import "common_model/encServiceEncoded.proto";
import "common_model/message.proto";
import "common_model/encList.proto";
import "common_model/enc.proto";
import "common_model/doubleList.proto";
import "common_model/CKKSCvector.proto";
import "common_model/CKKSCveclist.proto";

import "lir_model/linearRegressionConfig.proto";
import "lir_model/linearRegressionTrainConfig.proto";

import "noise_model/noise_query.proto";

service LirCalculation{
  // rpc transfer_ces(PB_EncServiceEncoded) returns (PB_Message){};
  // rpc transfer_config(PB_LinearRegressionTrainConfig) returns (PB_Message){};
  // rpc transfer_encrypted_parameters(PB_EncryptedModelWeights) returns (PB_Message){};
  // rpc apply_one_batch(PB_EncryptedInput) returns (PB_EncryptedOutput){};
  // rpc apply_forward(PB_EncryptedInput) returns (PB_EncryptedInferenceOutput){};

  // Inference
  rpc transfer_config_for_inference(PB_LinearRegressionConfig) returns (PB_Message){};
  rpc upload_weight_and_bias(PB_EncList) returns (PB_Message){};
  rpc set_noise_index(PB_NoiseQuery) returns (PB_Message){};
  rpc apply_forward_for_inference(PB_Enc) returns (PB_Enc){};
  rpc apply_forward_for_inference_raw(PB_DoubleList) returns (PB_DoubleList){};

  rpc apply_forward_for_inference_repeat(PB_EncList) returns (PB_EncList){};
  rpc apply_forward_for_inference_repeat_send_stream(stream PB_EncList) returns (PB_Message){};
  rpc apply_forward_for_inference_repeat_execute_stream(PB_Message) returns (stream PB_EncList){};

  // Training
  rpc transfer_ces(PB_EncServiceEncoded) returns (PB_Message){};
  rpc transfer_ces_for_preprocess(PB_EncServiceEncoded) returns (PB_Message){};
  rpc transfer_config_for_training(PB_LinearRegressionTrainConfig) returns (PB_Message){};
  rpc initialize_weight_with_normal_destribution(PB_Message) returns (PB_Message){};
  rpc initialize_weight_with_zero(PB_Message) returns (PB_Message){};

  rpc initialize_weight_with_normal_destribution_level1(PB_Message) returns (PB_Message){};
  rpc initialize_weight_with_zero_level1(PB_Message) returns (PB_Message){};


  rpc apply_one_batch(PB_EncList) returns (PB_EncList){};
  rpc apply_one_batch_noise(PB_EncList) returns (PB_EncList){};

  rpc apply_test_xs(PB_Enc) returns (PB_Enc){};
  rpc apply_one_batch_noise_level1_first(PB_EncList) returns (PB_EncList){};
  rpc apply_one_batch_noise_level1_first_with_indices(PB_EncList) returns (PB_EncList){};
  rpc apply_one_batch_noise_level1_second(PB_EncList) returns (PB_EncList){};
  rpc apply_one_batch_noise_level1_third(PB_EncList) returns (PB_EncList){};
  rpc apply_one_batch_noise_level1_fourth(PB_EncList) returns (PB_EncList){};
  rpc apply_one_batch_noise_level1_fifth(PB_EncList) returns (PB_Message){};

  rpc upload_noised_w_and_b(PB_EncList) returns (PB_Message){};
  rpc update_w_and_b(PB_EncList) returns (PB_EncList){};
  rpc calculate_loss_diff(PB_EncList) returns (PB_Enc){};
  rpc calculate_loss_diff_level1(PB_EncList) returns (PB_Enc){};
  rpc calculate_loss_diff_level1_with_indices(PB_EncList) returns (PB_Enc){};
  rpc remove_noise_from_loss_diff_and_sum(PB_Enc) returns (PB_Enc){};
  rpc remove_noise_from_loss_diff_and_sum_level1(PB_EncList) returns (PB_Enc){};
  rpc get_w_and_b(PB_Message) returns (PB_EncList){};
  rpc save_best_model(PB_Message) returns (PB_Message){};
  rpc save_best_model_to_model_info_with_name(PB_Message) returns (PB_Message){};
  rpc save_model_to_file_by_name(PB_Message) returns (PB_Message){};
  rpc remove_noise_from_sum_diff_for_mae(PB_Enc) returns (PB_Enc){};
  rpc get_best_model_info(PB_Message) returns (PB_Message){};

  rpc save_model(PB_Message) returns (PB_Message){};
  rpc load_model(PB_Message) returns (PB_Message){};
  rpc get_config(PB_Message) returns (PB_LinearRegressionTrainConfig){};

  rpc set_mae_info(PB_Message) returns (PB_Message){};
  rpc set_r2_info(PB_Message) returns (PB_Message){};
  rpc set_mse_info(PB_Message) returns (PB_Message){};

  rpc get_mae_info(PB_Message) returns (PB_Message){};
  rpc get_r2_info(PB_Message) returns (PB_Message){};
  rpc get_mse_info(PB_Message) returns (PB_Message){};

  rpc get_mae_info_all(PB_Message) returns (PB_Message){};
  rpc get_r2_info_all(PB_Message) returns (PB_Message){};
  rpc get_mse_info_all(PB_Message) returns (PB_Message){};

  rpc reset_training(PB_Message) returns (PB_Message){};
  rpc reset_training_data(PB_Message) returns (PB_Message){};

  rpc save_current_best_model_to_file(PB_Message) returns (PB_Message){};
  rpc get_current_best_model_with_noise(PB_Message) returns (PB_EncList){};
  rpc get_model_by_name_with_noise(PB_Message) returns (PB_EncList){};
  rpc transfer_current_best_model_from_file_to_inference(PB_EncList) returns (PB_Message){};
  rpc transfer_model_to_inference_by_name(PB_EncList) returns (PB_Message){};
  rpc upload_weight_and_bias_for_inference_with_noise(PB_EncList) returns (PB_Message){};
  rpc upload_weight_and_bias_for_inference_with_noise_from_coeff_to_coeff(PB_EncList) returns (PB_Message){};

  rpc transfer_x_train_from_preprocess(PB_CKKSCveclist) returns (PB_Message){};
  rpc transfer_x_test_from_preprocess(PB_CKKSCveclist) returns (PB_Message){};
  rpc transfer_y_train_from_preprocess(PB_CKKSCvector) returns (PB_Message){};
  rpc transfer_y_test_from_preprocess(PB_CKKSCvector) returns (PB_Message){};

  rpc apply_x_train_from_preprocess(PB_Message) returns (PB_CKKSCveclist){};
  rpc apply_x_test_from_preprocess(PB_Message) returns (PB_CKKSCveclist){};
  rpc apply_x_test_from_preprocess_without_noise(PB_Message) returns (PB_CKKSCveclist){};
  rpc apply_y_train_from_preprocess(PB_Message) returns (PB_CKKSCvector){};
  rpc apply_y_test_from_preprocess(PB_Message) returns (PB_CKKSCvector){};
}

