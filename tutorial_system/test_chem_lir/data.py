from copyreg import pickle
import sys
sys.path.append("../")

import numpy as np
from sklearn import preprocessing
from sklearn.linear_model import Ridge
import pickle
import pandas as pd

from typing import List
import matplotlib.pyplot as plt


def get_data():
    df = pd.read_csv("tmp.csv", index_col=0)
    # print(df.head)

    tmp_dict = {}
    for col in df.columns:
        mm = preprocessing.MinMaxScaler()
        if "x" in col:
            tmp_dict[col] = mm.fit_transform(df[col].values.reshape(-1, 1)).reshape(-1)
            # tmp_dict[col] = df[col].values.reshape(-1)
        elif "y" in col:
            tmp_dict[col] = df[col].values.reshape(-1)

    print(tmp_dict.keys())

    df = pd.DataFrame(tmp_dict.values(), index=tmp_dict.keys()).T
    print(df.head)

    xs = []
    ys1 = []
    ys2 = []
    for col in df.columns:
        if "x" in col:
            xs.append(df[col].values)
        elif "y1" in col:
            ys1.append(df[col].values)
        elif "y2" in col:
            ys2.append(df[col].values)

    xs = np.array(xs).T
    ys1 = np.array(ys1).T
    ys2 = np.array(ys2).T

    return xs, ys1, ys2


if __name__ == "__main__":
    xs, ys1, ys2 = get_data()
    print(xs.shape)
    print(ys1.shape)
