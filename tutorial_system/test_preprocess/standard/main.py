import sys
from turtle import color

sys.path.append("../")

from cf_core import CKKSSerializeService
import grpc
import random
import numpy as np
import pickle
import time
import json
from utils import CKKSEncryptionServiceWrapper

import matplotlib.pyplot as plt

# from grpc_python import input_client_pb2
from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc

# from ga_grpc_service.model import message_pb2

from grpc_service import common_model
from grpc_service import ga_model
from grpc_service import noise_model
from grpc_service import kmeans_model

from sklearn.preprocessing import MinMaxScaler


import constants_client as c


if __name__ == "__main__":
    print("hello, world")
    options = [
        ("grpc.max_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
        ("grpc.max_receive_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
    ]
    channel_input_client = grpc.insecure_channel(f"{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}", options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    stub.preprocess_gen_key(common_model.message_pb2.PB_Message())
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())

    party_num = 10

    data_whole = []

    for i in range(party_num):

        data1 = np.random.rand(4, 10)
        data_whole.append(data1)

        pb1 = common_model.indexDoubleList_pb2.PB_IndexDoubleList()
        pb1.data[:] = data1.reshape(-1)
        pb1.index = i
        pb1.size = 4
        pb1.attrs = 10

        stub.send_data_for_minmax(pb1)

    stub.apply_minmax(common_model.message_pb2.PB_Message())

    res_total = []
    for i in range(party_num):
        pb = common_model.message_pb2.PB_Message()
        pb.data_size = 4
        pb.party_index = i
        res = stub.get_minmax_data(pb)
        res = np.array(res.data).reshape(4, 10)
        res_total.append(res)

    data_whole = np.concatenate(data_whole, axis=0)
    res_total = np.concatenate(res_total, axis=0)

    scaler = MinMaxScaler()
    res_skl = scaler.fit_transform(data_whole)

    print("==============================CIPHER")
    print(res_total)

    print("==============================SKL")
    print(res_skl)

    valid = np.isclose(res_total, res_skl, atol=0.05)
    print(valid)
    for i in range(len(valid)):
        assert all(valid[i])
