import grpc
import sys
sys.path.append("../")

from sklearn import datasets

from grpc_python import input_client_pb2_grpc
from grpc_python import model_client_pb2_grpc

import network_constants


def get_input_client_stub():
    channel_input_client = grpc.insecure_channel(f'{network_constants.INPUT_CLIENT_HOST}:{network_constants.INPUT_CLIENT_PORT}', options=[])
    api_input_client = input_client_pb2_grpc.InputClientStub(channel_input_client)
    return api_input_client


def get_model_client_stub():
    channel_model_client = grpc.insecure_channel(f'{network_constants.MODEL_CLIENT_HOST}:{network_constants.MODEL_CLIENT_PORT}', options=[])
    api_model_client = model_client_pb2_grpc.ModelClientStub(channel_model_client)
    return api_model_client
