import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.utils import check_X_y, check_array
from sklearn.exceptions import NotFittedError
from scipy.sparse.linalg import lsmr
from sklearn.preprocessing import normalize
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.datasets import make_regression

sys.path.append("../../")
from gateai_connector.enc_lssvr import ENC_LSSVR
from gateai_connector.enc_preprocess import ENC_PREPROCESS

# initialize and keygen of preprocess and lssvr
preprocess = ENC_PREPROCESS()
preprocess.reset()
preprocess.keygen()
reg = ENC_LSSVR()
reg.reset()
reg.keygen()

# generate test data
xs, ys = make_regression(n_samples=100, n_features=10, random_state=1)
ds = xs.shape[0]

xs_1 = xs[0 : int(ds // 2)]
ys_1 = ys[0 : int(ds // 2)]
xs_2 = xs[int(ds // 2) :]
ys_2 = ys[int(ds // 2) :]

# preprocess
preprocess.transfer_data(xs_1, ys_1)
preprocess.transfer_data(xs_2, ys_2)
preprocess.train_test_split(train_size=0.8)
preprocess.min_max_scaler()
preprocess.send_svr()

# svr

C = 10
epsilon = 0.1
reg.set_config(
    C,
    epsilon,
)
reg.train_with_score()
mse = reg.score_for_test()
print(mse)
