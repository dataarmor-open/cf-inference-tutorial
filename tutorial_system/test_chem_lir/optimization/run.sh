rm -rf ga_res ga_plot kmeans_plot kmeans_res
mkdir ga_res ga_plot kmeans_plot kmeans_res
python test_keygen.py
python test_set_config.py
python test_upload.py
python test_ga.py
python test_ga_result_compare.py
python test_kmeans.py
python test_predict_kmeans_result.py 
