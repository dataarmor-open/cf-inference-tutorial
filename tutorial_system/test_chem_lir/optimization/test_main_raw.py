import sys
from turtle import color
sys.path.append("../")
sys.path.append("../../")

from cf_core import CKKSSerializeService
import grpc
import random
import numpy as np
import time
import json

from utils import CKKSEncryptionServiceWrapper

from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc

from grpc_service import common_model
from grpc_service import noise_model
from grpc_service import kmeans_model

import data

import constants_client as c

from test_set_config import set_config_lir
from test_upload import update_lir_model

def return_cluster_index(x, cluster):
    for k,v in cluster.items():
        if x in v:
            return int(k)

def plot_eval(res, i):
    ax = plt.subplot()
    ax.scatter(c.GOAL[0], c.GOAL[1], color=(0,0,1))
    ax.scatter(res[0], res[1], color=(1,0,0))
    ax.set_ylim(-5,5)
    ax.set_xlim(-5,5)
    plt.savefig(f"goals/plot_{i}.png")
    plt.close()


def lir_and_ga_first(stub):
    xs, ys, w, b = data.load_data()
    xs_np = np.zeros(c.ATTRS * len(xs))
    for i in range(len(xs)):
        xs_np[c.ATTRS*i:(i+1)*c.ATTRS] = xs[i]
    
    
    res_total = []
    for i in range(c.NUMBER_OF_GOALS):
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = xs_np
        pb.model_index = i
        tmp = stub.predict_lir_one_repeat_wo_denoise(pb).data
        tmp2 = np.array(tmp).reshape(c.LIR_DATA_SIZE)
        res_total.append(tmp2)

    res_total = np.array(res_total)
    print(res_total.shape)

    assert res_total.shape[0] == c.NUMBER_OF_GOALS
    assert res_total.shape[1] == c.LIR_DATA_SIZE

    plot_eval(res_total, 0)

    diff_goals = []
    for i in range(c.NUMBER_OF_GOALS):
        diff_goal = res_total[i] - c.GOAL[i]
        diff_goals.append(diff_goal)

    diff_goals_flatten = np.array(diff_goals).reshape(-1)
    assert diff_goals_flatten.shape[0] == c.NUMBER_OF_GOALS * c.LIR_DATA_SIZE
    pb_evaluation = common_model.doubleList_pb2.PB_DoubleList()
    pb_evaluation.data[:] = diff_goals_flatten
    pb_evaluation.size = c.LIR_DATA_SIZE
    stub.upload_evaluation_for_ga(pb_evaluation)

    assert len(xs.shape) == 2
    assert xs.shape[0] == c.LIR_DATA_SIZE
    assert xs.shape[1] == c.ATTRS
    # parents_w_noise = parents + noise
    pb_parents = common_model.doubleList_pb2.PB_DoubleList() 
    pb_parents.data[:] = xs.reshape(-1)
    pb_parents.size = c.LIR_DATA_SIZE
    stub.upload_encrypted_parent_for_ga_wo_denoise(pb_parents)

    next_gens = stub.query_raw_for_ga(common_model.message_pb2.PB_Message())
    next_gens_np = np.array(next_gens.data)
    assert next_gens_np.shape[0] <= c.ATTRS * c.LIR_DATA_SIZE
    assert next_gens_np.shape[0] % c.ATTRS == 0
    next_gens_np = next_gens_np.reshape(-1, c.ATTRS)
    return next_gens_np


def lir_and_ga(next_gens_np, loop_index, stub):

    next_repeat = next_gens_np.shape[0]

    pb = common_model.doubleList_pb2.PB_DoubleList()
    xs_np = np.zeros(c.ATTRS * next_repeat)
    for i in range(next_repeat):
        xs_np[c.ATTRS*i:(i+1)*c.ATTRS] = next_gens_np[i]
    
    pb.data[:] = xs_np
    
    res_total = []
    for i in range(c.NUMBER_OF_GOALS):
        pb.model_index = i
        tmp = stub.predict_lir_one_repeat(pb).data
        assert len(tmp) == next_repeat
        res_total.append(np.array(tmp))

    res_total = np.array(res_total)
    print(res_total.shape)

    assert res_total.shape[0] == c.NUMBER_OF_GOALS
    assert res_total.shape[1] == next_repeat

    plot_eval(res_total, loop_index+1)

    diff_goals = []
    for i in range(c.NUMBER_OF_GOALS):
        diff_goal = res_total[i] - c.GOAL[i]
        diff_goals.append(diff_goal)
    
    current_diff = np.average(np.abs(np.array(diff_goals)), axis=1)
    print(f"current_diff: {current_diff}")
    

    diff_goals_flatten = np.array(diff_goals).reshape(-1)
    assert diff_goals_flatten.shape[0] == c.NUMBER_OF_GOALS * next_repeat
    pb_evaluation = common_model.doubleList_pb2.PB_DoubleList()
    pb_evaluation.data[:] = diff_goals_flatten
    pb_evaluation.size = next_repeat
    stub.upload_evaluation_for_ga(pb_evaluation)

    pb_parents = common_model.doubleList_pb2.PB_DoubleList() 
    pb_parents.data[:] = next_gens_np.reshape(-1)
    pb_parents.size = next_repeat
    stub.upload_encrypted_parent_for_ga(pb_parents)

    next_gens = stub.query_raw_for_ga(common_model.message_pb2.PB_Message())
    # next_gens = stub.query_for_ga(common_model.message_pb2.PB_Message())
    next_gens_np = np.array(next_gens.data)
    next_gens_np = next_gens_np.reshape(-1, c.ATTRS)
    return next_gens_np, current_diff


if __name__ == "__main__":
    print("hello, world")
    colors = []
    for i in range(c.K):
        tmp = []
        for j in range(3):
            tmp.append(random.random())
        colors.append(tmp)
    options = [('grpc.max_message_length', c.GRPC_MAX_MESSAGE_LENGTH),('grpc.max_receive_message_length', c.GRPC_MAX_MESSAGE_LENGTH)]
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)


    # GA START====================================================
    attrs = c.ATTRS
    k = c.K
    
    next_gens_np = lir_and_ga_first(stub)
    pca = PCA(n_components=2)

    xs, ys, w, b = data.load_data()
    pca = pca.fit(xs)


    res_diffs = []
    loop = c.ITERATION
    for i in range(loop):
        t1 = time.time()
        set_config_lir(stub, len(next_gens_np), 0)
        set_config_lir(stub, len(next_gens_np), 1)
        next_gens_np, current_diff = lir_and_ga(next_gens_np, i, stub)
        res_diffs.append(current_diff)
        data_size = len(next_gens_np)
        t2 = time.time()
        print(f"\n\nloop for lir_and_ga {i} / {loop} done: time = {t2-t1}")
    print(f"res_diffs: {res_diffs}")
    np.savetxt("goals/red_diffs.txt", res_diffs)
    

    # KMEANS START====================================================
    pb_config = kmeans_model.kmeansConfig_pb2.PB_KMeansConfig()
    pb_config.attrs = attrs
    pb_config.k = k
    pb_config.data_size = data_size
    stub.upload_config_for_kmeans(pb_config)

    pb_datas = common_model.doubleList_pb2.PB_DoubleList()
    pb_datas.data[:] = next_gens_np.reshape(-1)
    pb_datas.size = data_size
    stub.upload_data_for_kmeans(pb_datas)

    pb_ks = common_model.doubleList_pb2.PB_DoubleList()
    ks = np.random.rand(c.K, c.ATTRS)
    pb_ks.data[:] = ks.reshape(-1)
    stub.upload_ks_for_kmeans(pb_ks)

    noise = stub_noise.get_noise_all(noise_model.noise_query_pb2.PB_NoiseQuery())
    assert len(noise.data) == data_size * attrs
    noise_np = np.zeros((data_size, attrs))
    for i in range(data_size):
      for j in range(attrs):
        noise_np[i][j] = noise.data[i*attrs + j]
    next_gens_np = next_gens_np.reshape(data_size, attrs)
    next_gens_wo_noise = next_gens_np - noise_np
    next_gens_pca = pca.transform(next_gens_wo_noise)

    ax = plt.subplot()
    for i in range(k):
        #print(f"{dec_ks[i][0]}, {dec_ks[i][1]}, {colors[i]}")
        ax.scatter(ks[i][0], ks[i][1], color=colors[i], marker="x", s=100)
    
    for i in range(data_size):
        ax.scatter(next_gens_pca[i][0], next_gens_pca[i][1], color="b")
    ax.set_ylim(-2,2)
    ax.set_xlim(-2,2)
    plt.savefig(f"images/initial.png")
    plt.close()

    
    for loop in range(c.KMEANS_ROUND):
        print(f"kmeans loop: {loop}/{c.KMEANS_ROUND}============")
        t1_kmean_round = time.time()
        res = stub.update_distance_raw_for_kmeans(common_model.message_pb2.PB_Message())

        stub.upload_distance_for_kmeans(res)

        res_ks = stub.update_ks_raw_for_kmeans(common_model.message_pb2.PB_Message())
        res_ks = json.loads(res_ks.message)


        res = stub.get_service_for_kmeans(common_model.message_pb2.PB_Message())
        service = CKKSEncryptionServiceWrapper.deserialize_encryption_service(serialized_obj=json.loads(res.ckks_enc_service_encoded), poly_modulus_degree=c.LEVEL1_CKKS_POLY_MODULUS_DEGREE, scale=c.LEVEL1_CKKS_SCALE, include_galois_key=False)
        print("got service")

        res = stub.get_datas_for_kmeans(common_model.message_pb2.PB_Message())
        print("got data")
        assert len(res.data) == data_size * 2
        tmp = CKKSSerializeService.deserialize_ctxt_1d(res.data, service, True)
        print("desered data")
        tmp = service.decrypt_2d_as_coeff(tmp)
        print("decd data")
        dec_data = []
        for i in range(len(tmp)):
            dec_data.append(tmp[i][:attrs])
        print(dec_data)
        # res = stub.get_ks_for_kmeans(common_model.message_pb2.PB_Message())
        # print("got ks")
        # assert len(res.data) == k
        # tmp = CKKSSerializeService.deserialize_ctxt_1d(res.data, service, True)
        # dec_ks_tmp = service.decrypt_2d_as_coeff(tmp)
        # dec_ks = []
        # for i in range(len(dec_ks_tmp)):
        #     dec_ks.append(dec_ks_tmp[i][:attrs])
        # print(dec_ks)

        print(f"\n\nhere!!!!!")
        print(res_ks)
        dec_ks = []
        for i in range(len(res_ks.keys())):
            dec_ks.append(res_ks[str(i)])




        
        res = stub.get_cluster_for_kmeans(common_model.message_pb2.PB_Message())
        print("got cluster")
        print(json.loads(res.message))
        cluster = json.loads(res.message)

        dec_data = pca.transform(dec_data)
        dec_ks = pca.transform(dec_ks)

        ax = plt.subplot()
        for i in range(k):
            #print(f"{dec_ks[i][0]}, {dec_ks[i][1]}, {colors[i]}")
            ax.scatter(dec_ks[i][0], dec_ks[i][1], color=colors[i], marker="x", s=100)
        
        for i in range(data_size):
            index = return_cluster_index(i, cluster)
            #print(f"{dec_data[i][0]}, {dec_data[i][1]}, {colors[index]}")
            ax.scatter(dec_data[i][0], dec_data[i][1], color=colors[index])
        ax.set_ylim(-2,2)
        ax.set_xlim(-2,2)
        plt.savefig(f"images/tmp_{loop}.png")
        
        plt.close()
        t2_kmean_round = time.time()
        print(f"\nkmean_round: {t2_kmean_round - t1_kmean_round}")






