# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from grpc_python.model import encServiceEncoded_pb2 as model_dot_encServiceEncoded__pb2
from grpc_python.model import encryptedData_pb2 as model_dot_encryptedData__pb2
from grpc_python.model import message_pb2 as model_dot_message__pb2
from grpc_python.model import otInput_pb2 as model_dot_otInput__pb2
from grpc_python.model import treeConfig_pb2 as model_dot_treeConfig__pb2


class CalculationStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.transfer_ces = channel.unary_unary(
                '/Calculation/transfer_ces',
                request_serializer=model_dot_encServiceEncoded__pb2.PB_EncServiceEncoded.SerializeToString,
                response_deserializer=model_dot_message__pb2.PB_Message.FromString,
                )
        self.transfer_config = channel.unary_unary(
                '/Calculation/transfer_config',
                request_serializer=model_dot_treeConfig__pb2.PB_TreeConfig.SerializeToString,
                response_deserializer=model_dot_message__pb2.PB_Message.FromString,
                )
        self.compare_threshold_and_data = channel.unary_unary(
                '/Calculation/compare_threshold_and_data',
                request_serializer=model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
                response_deserializer=model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
                )
        self.remove_gamma = channel.unary_unary(
                '/Calculation/remove_gamma',
                request_serializer=model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
                response_deserializer=model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
                )
        self.send_result_by_ot = channel.unary_unary(
                '/Calculation/send_result_by_ot',
                request_serializer=model_dot_otInput__pb2.PB_OtInput.SerializeToString,
                response_deserializer=model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
                )
        self.send_result_by_ot_with_weights = channel.unary_unary(
                '/Calculation/send_result_by_ot_with_weights',
                request_serializer=model_dot_otInput__pb2.PB_OtInput.SerializeToString,
                response_deserializer=model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
                )


class CalculationServicer(object):
    """Missing associated documentation comment in .proto file."""

    def transfer_ces(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def transfer_config(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def compare_threshold_and_data(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def remove_gamma(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def send_result_by_ot(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def send_result_by_ot_with_weights(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_CalculationServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'transfer_ces': grpc.unary_unary_rpc_method_handler(
                    servicer.transfer_ces,
                    request_deserializer=model_dot_encServiceEncoded__pb2.PB_EncServiceEncoded.FromString,
                    response_serializer=model_dot_message__pb2.PB_Message.SerializeToString,
            ),
            'transfer_config': grpc.unary_unary_rpc_method_handler(
                    servicer.transfer_config,
                    request_deserializer=model_dot_treeConfig__pb2.PB_TreeConfig.FromString,
                    response_serializer=model_dot_message__pb2.PB_Message.SerializeToString,
            ),
            'compare_threshold_and_data': grpc.unary_unary_rpc_method_handler(
                    servicer.compare_threshold_and_data,
                    request_deserializer=model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
                    response_serializer=model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
            ),
            'remove_gamma': grpc.unary_unary_rpc_method_handler(
                    servicer.remove_gamma,
                    request_deserializer=model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
                    response_serializer=model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
            ),
            'send_result_by_ot': grpc.unary_unary_rpc_method_handler(
                    servicer.send_result_by_ot,
                    request_deserializer=model_dot_otInput__pb2.PB_OtInput.FromString,
                    response_serializer=model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
            ),
            'send_result_by_ot_with_weights': grpc.unary_unary_rpc_method_handler(
                    servicer.send_result_by_ot_with_weights,
                    request_deserializer=model_dot_otInput__pb2.PB_OtInput.FromString,
                    response_serializer=model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'Calculation', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class Calculation(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def transfer_ces(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Calculation/transfer_ces',
            model_dot_encServiceEncoded__pb2.PB_EncServiceEncoded.SerializeToString,
            model_dot_message__pb2.PB_Message.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def transfer_config(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Calculation/transfer_config',
            model_dot_treeConfig__pb2.PB_TreeConfig.SerializeToString,
            model_dot_message__pb2.PB_Message.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def compare_threshold_and_data(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Calculation/compare_threshold_and_data',
            model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
            model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def remove_gamma(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Calculation/remove_gamma',
            model_dot_encryptedData__pb2.PB_EncryptedData.SerializeToString,
            model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def send_result_by_ot(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Calculation/send_result_by_ot',
            model_dot_otInput__pb2.PB_OtInput.SerializeToString,
            model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def send_result_by_ot_with_weights(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Calculation/send_result_by_ot_with_weights',
            model_dot_otInput__pb2.PB_OtInput.SerializeToString,
            model_dot_encryptedData__pb2.PB_EncryptedData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
