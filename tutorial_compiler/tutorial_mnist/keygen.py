# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

if __name__ == "__main__":
  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  # call input_client to generate keys
  print("call gen_key")
  api_input_client.gen_key(NoParam())
