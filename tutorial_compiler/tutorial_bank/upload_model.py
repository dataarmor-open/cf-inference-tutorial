# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

def dispense_data():
  dataset = np.loadtxt("data_banknote_authentication.txt", delimiter=",")

  x = dataset[:, 0:4]
  y = dataset[:, 4]

  x = np.array(x)
  y = np.array(y)

  x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

  return x_train, x_test, y_train, y_test



if __name__ == "__main__":
  # set model index
  model_index = 3

  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  # call model_client to upload pickle file from modelfiles/pkl/{model_name}.pkl
  print("call compile_model_from_local_pkl")
  model_bin = ModelBinarySklearn()
  with open('lr_bank.pkl', 'rb') as f:
    model = pickle.load(f)

  batch_size = 10

  model_bin.bin = pickle.dumps(model)
  model_bin.batch_size = batch_size
  model_bin.model_index = model_index
  #model_info = ModelInfo()
  #model_info.model_name = "lr_bank"
  #model_info.batch_size = 5
  #tmp = api_model_client.compile_model_from_local_pkl(model_info)
  api_model_client.compile_model_from_binary_sklearn(model_bin)
