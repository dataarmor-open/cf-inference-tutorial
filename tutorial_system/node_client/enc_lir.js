import { dynamic_load_input_client } from "./utils.js";

class ENC_LIR {
    constructor(proto_path, input_client_host, input_client_port, model_index) {
        this.stub = dynamic_load_input_client(proto_path, input_client_host, input_client_port)
        this.model_index = model_index
        this.set_noise_index()
    }

    async keygen() {
        return new Promise((resolve, reject) => this.stub.gen_key_for_lir({ model_index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("gen_key_for_lir: done")
            resolve(response)
        }))
    }

    async reset_training() {
        return new Promise((resolve, reject) => this.stub.reset_training_for_lir({ model_index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("reset_training_for_lir: done")
            resolve(response)
        }))
    }

    async set_noise_index() {
        return new Promise((resolve, reject) => this.stub.set_noise_index({ index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("set_noise_index: done")
            resolve(response)
        }))

    }

    async set_config(attrs, bs, ss, eta, update_option, lmb) {
        const pb_config = {
            attrs: attrs,
            bs: bs,
            ss: ss,
            eta: eta,
            update_option: update_option,
            model_index: this.model_index
        }

        if (lmb !== "undefined") {
            pb_config["lmb"] = lmb
        }
        // if (is_use_warm_up !== "undefined") {
        //     pb_config["is_use_warm_up"] = is_use_warm_up
        //     pb_config["eta"] = initial_eta
        // }

        return new Promise((resolve, reject) => this.stub.upload_config_for_training_for_lir(pb_config, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("upload_config_for_training_for_lir: done")
            resolve(response)
        }))

    }

    async save_key_on_server(name) {
        const message = {
            message: name,
            model_index: this.model_index
        }
        return new Promise((resolve, reject) => this.stub.lir_save_secret_key(message, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_save_secret_key: done")
            resolve(response)
        }))

    }

    async load_key_on_server(name) {
        const message = {
            message: name,
            model_index: this.model_index
        }
        return new Promise((resolve, reject) => this.stub.lir_load_secret_key(message, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_load_secret_key: done")
            resolve(response)
        }))
    }

    async transfer_ces_from_preprocess_to_lir() {
        return new Promise((resolve, reject) => this.stub.transfer_ces_for_preprocess_to_lir({ model_index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("transfer_ces_from_preprocess_to_lir: done")
            resolve(response)
        }))

    }

    async initialize_weight_with_zero_for_lir_level1() {
        return new Promise((resolve, reject) => this.stub.initialize_weight_with_zero_for_lir_level1({ message: "server", model_index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("initialize_weight_with_zero_for_lir_level1: done")
            resolve(response)
        }))

    }

    async train_one_epoch_level1(i) {
        return new Promise((resolve, reject) => this.stub.train_one_epoch_level1({ model_index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log(`train_one_epoch_level1: ${i} ==========`)
            resolve(response)
        }))

    }

    async calculate_loss_for_test_data_level1(save_model_name) {
        return new Promise((resolve, reject) => this.stub.calculate_loss_for_test_data_level1({ model_index: this.model_index, message: save_model_name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("calculate_loss_for_test_data_level1: done")
            resolve(response)
        }))

    }

    async fit_level1(epoch) {
        this.loss_list = []
        // this.stub.initialize_weight_with_zero_for_lir_level1(
        //     { message: "server", model_index: this.model_index },
        //     (err, response) => {
        //         console.log("initialize_weight_with_zero_for_lir_level1: done")
        //     }
        // )
        await this.initialize_weight_with_zero_for_lir_level1()

        for (let i = 0; i < epoch; i++) {
            // this.stub.train_one_epoch_level1({ model_index: this.model_index }, (err, response) => {
            //     console.log(`train_one_epoch_level1: ${i} / ${epoch} ==========`)
            // })
            await this.train_one_epoch_level1(i)
            // this.stub.calculate_loss_for_test_data_level1({ model_index: this.model_index, message: save_model_name }, (err, response) => {
            //     this.loss_list.push(response.data[0])
            // })
            const res_loss = await this.calculate_loss_for_test_data_level1("tmp")
            console.log(`loss: ${res_loss.data}`)
        }
    }

    async save_model_on_server(name) {
        return new Promise((resolve, reject) => this.stub.lir_save_model({ message: name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_save_model: done")
            resolve(response)
        }))
    }

    async load_model_on_server(name) {
        return new Promise((resolve, reject) => this.stub.lir_load_model({ message: name }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_load_model: done")
            resolve(response)
        }))
    }

    async predict_already_send() {
        return new Promise((resolve, reject) => this.stub.lir_predict_test_data({ model_index: this.model_index }, (err, response) => {
            if (err) {
                return reject(err)
            }
            console.log("lir_predict_test_data: done")
            resolve(response.data)
        }))
    }

}

export { ENC_LIR }