from re import L
import sys
from turtle import color

sys.path.append("../")
from cf_core import CKKSSerializeService
import grpc
import random
import numpy as np
import pickle
import time
import json
from utils import CKKSEncryptionServiceWrapper
import matplotlib.pyplot as plt
from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc
from grpc_service import common_model
from grpc_service import ga_model
from grpc_service import noise_model
from grpc_service import kmeans_model
from sklearn.preprocessing import MinMaxScaler
import constants_client as c
from sklearn.datasets import make_regression
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split


def _make_pb_from_doubleList(doubleList):
    if len(doubleList.shape) == 2:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = np.ravel(doubleList)
        pb.size = doubleList.shape[1]
    else:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = doubleList
        pb.size = 0
    return pb


if __name__ == "__main__":
    print("hello, world")
    options = [
        ("grpc.max_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
        ("grpc.max_receive_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
    ]
    channel_input_client = grpc.insecure_channel(f"{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}", options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    stub.preprocess_gen_key(common_model.message_pb2.PB_Message())
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs, ys = make_regression(n_samples=3, n_features=3, random_state=1)
    X_train, X_test, y_train, y_test = train_test_split(xs, ys, test_size=0.1, random_state=1)
    print(X_train)
    pb_x = _make_pb_from_doubleList(X_train)
    stub.transfer_x_train(pb_x)

    pb_x = _make_pb_from_doubleList(X_test)
    stub.transfer_x_test(pb_x)

    pb_y = _make_pb_from_doubleList(y_train)
    stub.transfer_y_train(pb_y)

    pb_y = _make_pb_from_doubleList(y_test)
    stub.transfer_y_test(pb_y)

    stub.transfer_data_to_svr_from_preprocess(common_model.message_pb2.PB_Message())
    stub.svr_convert_preprocess_data(common_model.message_pb2.PB_Message())
