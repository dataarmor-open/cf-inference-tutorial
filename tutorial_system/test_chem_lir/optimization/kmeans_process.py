import sys
import json
sys.path.append("../")
sys.path.append("../../")
from grpc_service import kmeans_model
from grpc_service import common_model
from cf_core import CKKSSerializeService
from optimization.test_set_config import set_config_lir, set_config_kmeans, set_config_lir_for_batch_prediction
from optimization.test_upload import update_lir_model, upload_lir_model_raw

import matplotlib.pyplot as plt
from utils import CKKSEncryptionServiceWrapper
import constants_client as c
from optimization import plot_utils

def _return_cluster_index(x, cluster):
    for k,v in cluster.items():
        if x in v:
            return int(k)

def kmeans_main(stub):
    # 各データとクラスター重心の距離を計算（結果が並び替えられたものがresとして返却、input_clientで復号済み）
    pb = common_model.message_pb2.PB_Message()
    # KMEANSサービスに対して、各データと各クラスター重心との距離を並び替えて返却するか明記する
    pb.is_postprocess_perm = c.POSTPROCESS_PERM_DISTANCE_KMEANS_SERVICE
    res = stub.update_distance_for_kmeans_stream(pb)

    # 復号された距離情報を再暗号化して送信
    stub.upload_distance_for_kmeans(res)

    # 送信された距離データに基づきクラスター重心を更新
    pb = common_model.message_pb2.PB_Message()
    # KMEANSサービスに対して、各データと各クラスター重心との距離を逆並び替えしてからクラスター重心を更新するように明記
    pb.is_preprocess_deperm = c.PREPROCESS_DEPERM_DISTANCE_KMEANS_SERVICE
    stub.update_ks_for_kmeans(pb)


def get_cluster_centroids(stub, attrs, k):
    # input_client から秘密鍵を取得
    res = stub.get_service_for_kmeans(common_model.message_pb2.PB_Message())
    service = CKKSEncryptionServiceWrapper.deserialize_encryption_service(serialized_obj=json.loads(res.ckks_enc_service_encoded), poly_modulus_degree=2**13, scale=2**40, include_galois_key=False)

    # input_client を経由し、kmeans サービスから暗号化されたクラスター重心データを取得
    res = stub.get_ks_for_kmeans(common_model.message_pb2.PB_Message())
    assert len(res.data) == k
    tmp = CKKSSerializeService.deserialize_ctxt_1d(res.data, service, True)

    # 秘密鍵を使ってクラスタリング重心データを復号
    dec_ks_tmp = service.decrypt_2d_as_coeff(tmp)
    dec_ks = []
    for i in range(len(dec_ks_tmp)):
        dec_ks.append(dec_ks_tmp[i][:attrs])

    
    # input_client を経由し、kmeans サービスからクラスター情報（どのデータがどのクラスターに属するか）を取得
    res = stub.get_cluster_for_kmeans(common_model.message_pb2.PB_Message())
    cluster = json.loads(res.message)

    return dec_ks, cluster



def plot_kmeans_data(stub, next_gens_np, attrs, k, colors, pca, loop_index):
    # input_client から秘密鍵を取得
    res = stub.get_service_for_kmeans(common_model.message_pb2.PB_Message())
    service = CKKSEncryptionServiceWrapper.deserialize_encryption_service(serialized_obj=json.loads(res.ckks_enc_service_encoded), poly_modulus_degree=2**13, scale=2**40, include_galois_key=False)

    # input_client を経由し、kmeans サービスから暗号化されたクラスタリング対象データを取得
    res = stub.get_datas_for_kmeans_stream(common_model.message_pb2.PB_Message())
    assert len(res.data) == len(next_gens_np) * 2
    tmp = CKKSSerializeService.deserialize_ctxt_1d(res.data, service, True)

    # 秘密鍵を使ってクラスタリング対象データを復号
    tmp = service.decrypt_2d_as_coeff(tmp)
    dec_data = []
    for i in range(len(tmp)):
        dec_data.append(tmp[i][:attrs])

    # input_client を経由し、kmeans サービスから暗号化されたクラスター重心データを取得
    res = stub.get_ks_for_kmeans_stream(common_model.message_pb2.PB_Message())
    assert len(res.data) == k
    tmp = CKKSSerializeService.deserialize_ctxt_1d(res.data, service, True)

    # 秘密鍵を使ってクラスタリング重心データを復号
    dec_ks_tmp = service.decrypt_2d_as_coeff(tmp)
    dec_ks = []
    for i in range(len(dec_ks_tmp)):
        dec_ks.append(dec_ks_tmp[i][:attrs])

    
    # input_client を経由し、kmeans サービスからクラスター情報（どのデータがどのクラスターに属するか）を取得
    res = stub.get_cluster_for_kmeans(common_model.message_pb2.PB_Message())
    cluster = json.loads(res.message)

    # プロットするためにPCA（２次元）を適用
    dec_data = pca.transform(dec_data)
    dec_ks = pca.transform(dec_ks)

    ax = plt.subplot()
    for i in range(k):
        #print(f"{dec_ks[i][0]}, {dec_ks[i][1]}, {colors[i]}")
        ax.scatter(dec_ks[i][0], dec_ks[i][1], color=colors[i], marker="x", s=100)
    
    for i in range(len(next_gens_np)):
        index = _return_cluster_index(i, cluster)
        #print(f"{dec_data[i][0]}, {dec_data[i][1]}, {colors[index]}")
        ax.scatter(dec_data[i][0], dec_data[i][1], color=colors[index])
    ax.set_ylim(-2,2)
    ax.set_xlim(-2,2)
    plt.savefig(f"images/tmp_{loop_index}.png")
    
    plt.close()


def predict_centroid_and_plot(stub, ks):
    set_config_lir_for_batch_prediction(stub, ks.shape[0], 0)
    set_config_lir_for_batch_prediction(stub, ks.shape[0], 1)
    upload_lir_model_raw(stub, 0)
    upload_lir_model_raw(stub, 1)

    pb = common_model.doubleList_pb2.PB_DoubleList()
    pb.model_index = 0
    pb.data[:] = ks.reshape(-1)
    res1 = stub.predict_lir(pb).data

    pb = common_model.doubleList_pb2.PB_DoubleList()
    pb.model_index = 1
    pb.data[:] = ks.reshape(-1)
    res2 = stub.predict_lir(pb).data

    plot_utils.plot_scatter(res1, res2, "kmeans_ks_centroid", True)


def predict_selected_datas_from_all_generation_and_plot(stub, bs, chosen_data):
    set_config_lir_for_batch_prediction(stub, bs, 0)
    set_config_lir_for_batch_prediction(stub, bs, 1)
    upload_lir_model_raw(stub, 0)
    upload_lir_model_raw(stub, 1)

    pb = common_model.doubleList_pb2.PB_DoubleList()
    pb.data[:] = chosen_data[:bs].reshape(-1)
    pb.model_index = 0
    res1 = stub.predict_lir(pb).data
    pb = common_model.doubleList_pb2.PB_DoubleList()
    pb.data[:] = chosen_data[:bs].reshape(-1)
    pb.model_index = 1
    res2 = stub.predict_lir(pb).data

    
    plot_utils.plot_scatter(res1, res2, "kmeans_chosen_data", False)