import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
import numpy as np
import pickle


from sklearn.decomposition import PCA

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc

import data
from optimization import ga_process
from optimization import kmeans_process
from optimization import plot_utils

import constants_client as c


def main():
    colors = plot_utils.get_colors()
    options = [('grpc.max_message_length', c.GRPC_MAX_MESSAGE_LENGTH),('grpc.max_receive_message_length', c.GRPC_MAX_MESSAGE_LENGTH)]

    ## input_client への接続を確立
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    ## noise サービスへの接続を確立(チュートリアルのために接続、普通は接続を制限している)
    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)


    # 遺伝アルゴリズム最適化スタート====================================================
    k = c.K
    attrs = c.ATTRS

    # 第一世代のみlir_and_ga にはファイルからロードしたデータを使用する（もしくは自分でランダム生成する）
    # lir_and_ga の is_need_denoise はFalseに設定する（ロードしたデータにはノイズが含まれていない)
    # 最適化するデータをロード
    xs, ys1, ys2 = data.get_data()
    # サービスの利用自体には必要ないが、プロットするためにPCAを行う
    pca = PCA(n_components=2)
    pca = pca.fit(xs)


    # KMEANS START====================================================
    # GAにより得られた全世代データと、それらの予測結果をロード
    all_children_denoised, all_children_results_depermed = ga_process.load_all_generations_data()

    all_children_denoised = all_children_denoised.reshape(-1, all_children_denoised.shape[2])
    all_children_results_depermed = all_children_results_depermed.reshape(-1, all_children_results_depermed.shape[2])

    # クラスターの重心、クラスター情報を取得
    ks, cluster = kmeans_process.get_cluster_centroids(stub, attrs, k)
    ks = np.array(ks)
    print(ks.shape)

    with open(f"{c.KMEANS_RESULT_FOLDER_NAME}/ks.pkl", "wb") as f:
        pickle.dump(ks, f)

    with open(f"{c.KMEANS_RESULT_FOLDER_NAME}/cluster.pkl", "wb") as f:
        pickle.dump(cluster, f)


    # 重心に対してLIRサービスによる予測を実行
    kmeans_process.predict_centroid_and_plot(stub, ks)


    # 全世代のデータに対して、一定領域のデータのみを抽出
    chosen_data, chosen_ys = ga_process.choose_data_with_condition(all_children_denoised[:len(all_children_results_depermed)], all_children_results_depermed)

    # 全世代のデータに対してLIRサービスによる推論を実行したいが、まずはバッチサイズ（データを何個暗号に詰められるか）を計算する
    # もし全てのデータを一つの暗号に詰められない場合、初めの100 個のみを推論することにする。
    if c.ATTRS * len(chosen_data) > c.SS // 2:
        bs = 100
    else:
        bs = len(chosen_data)

    # LIRによる予測を行い、結果をプロットする
    kmeans_process.predict_selected_datas_from_all_generation_and_plot(stub, bs, chosen_data)


if __name__ == "__main__":
    print("hello, world")
    main()
