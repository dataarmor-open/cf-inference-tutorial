import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
import numpy as np

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc

from grpc_service import common_model
from grpc_service import lir_model
from grpc_service import ga_model
from grpc_service import kmeans_model

import data


import constants_client as c

from optimization.test_set_noise import set_initial_noise

def set_config_lir_for_batch_prediction(stub, bs, model_index):
    pb_config = lir_model.linearRegressionConfig_pb2.PB_LinearRegressionConfig()
    pb_config.attrs = c.ATTRS
    pb_config.bs = bs
    pb_config.ss = c.SS
    pb_config.model_index = model_index

    stub.upload_config_for_inference_for_lir(pb_config)

def set_config_lir(stub, repeat, model_index):
    pb_config = lir_model.linearRegressionConfig_pb2.PB_LinearRegressionConfig()
    pb_config.attrs = c.ATTRS
    pb_config.bs = 1
    pb_config.ss = c.SS
    pb_config.repeat = repeat
    pb_config.model_index = model_index

    stub.upload_config_for_inference_for_lir(pb_config)


def set_config_ga(stub):
    pb_config = ga_model.gaConfig_pb2.PB_GAConfig()
    pb_config.size = c.GA_DATA_SIZE
    pb_config.crossover_probability = c.CROSSOVER_PROBABILITY
    pb_config.mutation_indivisual_probability = c.MUTATION_INDIVISUAL_PROBABILITY
    pb_config.mutation_feature_probability = c.MUTATION_FEATURE_PROBABILITY
    pb_config.attrs = c.ATTRS
    pb_config.crossover_config[:] = c.CROSSOVER_CONFIG
    pb_config.elite_percentage = c.ELITE_PERCENTAGE
    pb_config.number_of_goals = c.NUMBER_OF_GOALS

    stub.upload_config_for_ga(pb_config)

    pb_feature_config_list = ga_model.gaFeatureConfigList_pb2.PB_GAFeatureConfigList()
    for i in range(pb_config.attrs):
        pb_feature_config = ga_model.gaFeatureConfig_pb2.PB_GAFeatureConfig()
        data = pb_feature_config_list.data.add()
        data.feature_min = 0
        data.feature_max = 1
    
    stub.upload_feature_config_for_ga(pb_feature_config_list)

    stub.initialize_mask_for_ga(common_model.message_pb2.PB_Message())


def set_config_kmeans(stub, data_size):
    data_size = data_size
    attrs = c.ATTRS
    k = c.K


    pb_config = kmeans_model.kmeansConfig_pb2.PB_KMeansConfig()
    pb_config.attrs = attrs
    pb_config.k = k
    pb_config.data_size = data_size
    stub.upload_config_for_kmeans(pb_config)


def main():
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)

    xs, ys1, ys2 = data.get_data()

    set_config_lir(stub, len(xs), 0)
    set_config_lir(stub, len(xs), 1)
    set_config_ga(stub)
    #set_config_kmeans(stub)
    set_initial_noise(stub_noise, len(xs), c.ATTRS, 0)
    set_initial_noise(stub_noise, len(xs), c.ATTRS, 1)


if __name__ == "__main__":
    print("hello, world")
    main()