# Copyright (c) EAGLYS Inc. All rights reserved.
import time
import grpc
import pickle
import keras
from keras.datasets import mnist
from sklearn.model_selection import train_test_split
import numpy as np
import sys
from keras.models import Model

from saas.input_client.service.input_client_pb2_grpc import InputClientStub
from saas.input_client.service.input_client_pb2 import NoParam, Tensor
from saas.model_client.service.model_client_pb2_grpc import ModelClientStub
from saas.model_client.service.model_client_pb2 import ModelInfo, ModelBinaryKeras

import network_constants
import network_utils

def dispense_data():
  (pre_x_train, pre_y_train), (x_test, y_test) = mnist.load_data()
  x_train, x_valid, y_train, y_valid = train_test_split(pre_x_train, pre_y_train, test_size=0.2)

  x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
  x_valid = x_valid.reshape(x_valid.shape[0], 28, 28, 1)

  y_train = keras.utils.to_categorical(y_train, 10)
  y_valid = keras.utils.to_categorical(y_valid, 10)

  return x_train, y_train, x_valid, y_valid


if __name__ == "__main__":
  # set model index
  model_index = 2

  model = keras.models.load_model('modelfiles/h5/cnn_mnist.h5')

  model.summary()
  #sys.exit()

  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  # call input_client to generate keys
  print("call gen_key")
  api_input_client.gen_key(NoParam())

  # call model_client to upload h5 file from modelfiles/h5/{model_name}.h5
  print("call load_and_compile_model")
  model_info = ModelBinaryKeras()
  model_info.config = pickle.dumps(model.get_config())
  model_info.weights = pickle.dumps(model.get_weights())
  model_info.type_info = 'sequential'
  # if intermediate_output is not "none", encrypt model until set layer
  model_info.intermediate_output = 'activation_2'
  model_info.model_index = model_index

  api_model_client.compile_model_from_binary_keras(model_info)


  print("call prediction")
  # prepare data for inference
  x, y, _, _ = dispense_data()
  test_data = x[0]

  t1 = time.time()
  # measure time for cipher inference
  # expect data.shape as (c, h, w)
  data_for_cf = test_data.transpose(2,0,1)
  # expect data as list, not np.ndarray
  data_for_cf = data_for_cf.tolist()
  # to call predict, pack pickle.dump(data_for_cf) into Tensor.data

  # call prediction
  input_data = Tensor()
  input_data.data = pickle.dumps(data_for_cf)
  input_data.model_index = model_index
  # if is_receive_intermediate = True, get output from intermediate layer
  # if false, get entire prediction result, but calculation is done in raw data after intermediate_layer
  input_data.is_receive_intermediate = True

  result = api_input_client.predict(input_data)

  # load result
  result = pickle.loads(result.data)

  t2 = time.time()

  # predict with raw input but output is from middle layer
  layer_name = 'activation_2'
  hidden_layer_model = Model(inputs=model.input, outputs=model.get_layer(layer_name).output)
  result_raw = hidden_layer_model.predict(test_data.reshape(1, 28, 28, 1))


  print("\n===============")
  print("cipher_result")
  print(result)
  print("\n===============")
  print("raw_result")
  print(result_raw)

  print("\n===============")
  print(f'time for cipher prediction >>> {t2-t1}')


