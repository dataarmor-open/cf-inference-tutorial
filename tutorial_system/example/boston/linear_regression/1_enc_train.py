import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston

sys.path.append("../../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lir import ENC_LIR

C = 10
epsilon = 0.1

C_plain = 10
epsilon_plain = 0.1
boston = load_boston()
xs = boston.data
ys = boston.target

if __name__ == "__main__":
    print("start load key on server.")
    preprocess = ENC_PREPROCESS()
    preprocess.reset()
    preprocess.load_key_on_server("tutorial_of_boston_data")

    lir0 = ENC_LIR(model_index=0)
    lir0.load_key_on_server("tutorial_of_boston_data")

    print("start transfer data.")
    preprocess.transfer_data(xs, ys)
    preprocess.train_test_split(train_size=0.8, random_state=1)

    print("start standard scaler.")
    preprocess.minmax_scaler_fit()
    preprocess.save_minmax_scaler_on_server("bostom_min_max")
    preprocess.minmax_scaler_transform_x_train()
    preprocess.minmax_scaler_transform_x_test()
    preprocess.send_lir()

    print("start lir train.")
    model_name = "tmp_model1"
    lir0.fit_level1(model_name)
    lir0.save_model_on_server("boston_lir_model")
