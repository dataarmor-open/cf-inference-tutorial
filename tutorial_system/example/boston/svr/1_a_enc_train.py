import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
import time
import datetime

sys.path.append("../../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lssvr import ENC_LSSVR

C = 10
epsilon = 0.1

C_plain = 10
epsilon_plain = 0.1
boston = load_boston()
xs = boston.data
ys = boston.target

if __name__ == "__main__":
    start = time.time()
    print("start load key on server.")
    # preprocessサーバーの"tutorial_of_boston_data"という鍵を読み込む
    preprocess = ENC_PREPROCESS()
    preprocess.reset()
    preprocess.load_key_on_server("tutorial_of_boston_data")

    # SVRサーバーの"tutorial_of_boston_data"という鍵を読み込む
    lssvr = ENC_LSSVR()
    lssvr.load_key_on_server("tutorial_of_boston_data")

    print("start transfer data.")
    # preprocessサーバーにデータを送信する
    preprocess.transfer_data(xs, ys)
    preprocess.save_input_x_on_server("tutorial_of_boston_data")
    preprocess.save_input_y_on_server("tutorial_of_boston_data")

    # preprocessサーバーでデータを学習用と検証用に分割する
    preprocess.train_test_split(test_size=0.1, random_state=1)

    print("start standard scaler.")
    # 標準化のためにpreprocessサーバーの学習用データの平均と分散を求め、保存する
    preprocess.standard_scaler_fit()
    preprocess.save_standard_scaler_on_server("boston_standard_scaler")
    # preprocessサーバーの学習用データを標準化する
    preprocess.standard_scaler_transform_x_train()
    # preprocessサーバーの検証用データを標準化する
    preprocess.standard_scaler_transform_x_test()
    # これらのデータをSVRサーバーに送信する
    preprocess.send_svr()

    print("start svr train.")
    # SVRサーバーに学習用のパラメータを送信する
    lssvr.set_config(C, epsilon)
    # 学習を開始する
    lssvr.train_with_score()
    # 学習後のモデルを保存する
    lssvr.save_model_on_server("boston_lssvr_model")
    end = time.time()
    print(f"calcuration time: {datetime.timedelta(seconds = end-start)}")
