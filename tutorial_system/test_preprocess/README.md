# How to use GateAI Preprocess

## Preprocess モジュールでできること
GateAI の Preprocess モジュールを使うと、

複数パーティが暗号化して持ち寄ったデータに対して

- `min-max normalization`
- `Z-score normalization`

のいずれかを実行することができます。

## Preprocess モジュール使用のために立ち上げ必要なサービス

このモジュールを使用する際、GateAIシステムの以下のサービスを利用する必要があります。

- `input_client`
- `preprocess`


## チュートリアルコードの解説

これらのサービスをdocker-compose を使って立ち上げた後、  
`main.py` のチュートリアルをフォローしてみます。

### 鍵ペアの生成

`stub.gen_key_for_minmax()`
により、input_client はpreprocess モジュールに必要な鍵を生成します。
生成された鍵のうち、公開鍵のみがpreprocess サービスへと送信されます。
（復号に必要な秘密鍵はinput_client が保持します。）

### 設定の初期化

サービスの初期化を行います。
(`stub.clear_data_for_minmax()`)


### パーティ数の設定
main.py のチュートリアルでは、

`party_num=10`
とし、10のパーティが参加するようなシナリオを設定します。


### データの送信
各パーティがデータをinput_clientへと送信します。(`stub.send_data_for_minmax()`)
input_client は送信されたデータを暗号化し、preprocess サービスへと送信します。

### 処理の実行
全てのパーティがデータを送信した後、いずれかのパーティ（代表者）がminmaxを実行するクエリを実行します。
(`stub.apply_minmax()`)

### 結果のダウンロード
minmax が実行された後、各パーティは結果を受け取ることができます。
(`stub.get_minmax_data()`)



以上が Preprocess モジュールの解説となります。
お疲れ様でした。