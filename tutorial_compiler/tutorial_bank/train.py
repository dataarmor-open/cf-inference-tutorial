# Copyright (c) EAGLYS Inc. All rights reserved.

import pickle
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split


if __name__ == "__main__":
  dataset = np.loadtxt("data_banknote_authentication.txt", delimiter=",")

  x = dataset[:, 0:4]
  y = dataset[:, 4]

  x = np.array(x)
  y = np.array(y)

  x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

  clf = LogisticRegression()
  clf.fit(x_train, y_train)

  print(f'model_score >>> {clf.score(x_test, y_test)}')

  with open('lr_bank.pkl', 'wb') as f:
    pickle.dump(clf, f)
