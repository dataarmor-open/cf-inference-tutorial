# constants_client.py の解説

ここでは、GateAI システムを使用するときに設定可能な、
constants_client.py の設定パラメータについて解説します。

GateAIシステムには、

- input_client
- lir
- svr
- ga
- kmeans
- noise
- preprocess 

が存在し、各サービスを使用するときのパラメータの設定をすることで、
お持ちのデータに合わせたカスタマイズされた分析を行うことが可能です。

`tutorial_system/constants_client.py` をご覧ください。  

### GateAI への接続に必要なパラメータ
- `INPUT_CLIENT_HOST`: input_client サービスへ接続するためのホスト情報
- `INPUT_CLIENT_PORT`: input_client サービスへ接続するためのポート情報
- `NOISE_HOST`: noise サービスへ接続するためのホスト情報
- `NOISE_PORT`: noise サービスへ接続するためのポート情報

### シード固定に関するパラメータ
- `IS_FIXING_RANDOM_SEED`: シード値を固定するかのブーリアン
- `RANDOM_SEED`: シード値を固定する際の値


### 線形回帰学習時のグリッドサーチに関するパラメータ
- `IS_GRID_SEARCH`: グリッドサーチを行うかどうかのブーリアン
- `LIR_GRID_SEARCH_RESULT_FOLDER`: グリッドサーチを行う際の結果を格納するフォルダへのパス
- `ETA_LIST`: グリッドサーチ時に試行する学習率のリスト
- `LMB_LIST`: グリッドサーチ時に試行するリッジ回帰の際の制約の強さのリスト（リッジ回帰を指定したときのみ有効）


### オーバーフローに関するパラメータ
- `IS_OVERFLOW_DETECTION`: 暗号空間でのオーバーフローを推測するかのブーリアン
- `OVERFLOW_THRESHOLD`: 上の`IS_OVERFLOW_DETECTION`がTrueだった場合、学習パラメータ（重み）の絶対値がこの閾値より大きくなると、暗号空間でのオーバーフローが起きたと推測し、学習を中止する




### 線形回帰に関連するパラメータ
- `LIR_TRAIN_RESULT_FOLDER_NAME`: 線形回帰の学習結果を格納するフォルダへのパス
- `LIR_DATA_SIZE`: 線形回帰の学習に使用するデータ数
- `ETA`: 線形回帰学習時の学習率
- `UPDATE_OPTION`: 学習時の制約条件 ["none", "l2"] から選択
- `LMB`: l2制約条件を選択したときの、制約の強さ（大きいと強い）
- `IS_ACCUMURATE_UPDATE`: Trueにした場合、`input_client.update_with_accumulated_parameters_for_lir` をコールするまで重みパラメータの更新を行わず、更新値をバッファに格納します。明示的なタイミングで重みパラメータを更新したい場合、Trueとしてください。
- `ACCUMURATE_UPDATE_FREQUENCY`: 上の`IS_ACCUMURATE_UPDATE`がTrue の場合、`ACCUMURATE_UPDATE_FREQUENCY`回ミニバッチを学習させたあと、重みの更新を実行する
- `LEARNING_EPOCH`: 線形回帰学習の際のトータルエポック数
- `ATTRS`: 学習データの次元数
- `BS`: 学習時のバッチサイズ `BS*ATTRS < SS/2`を満たす必要があります。
- `SS`: 暗号パラメータ、デフォルト値の8192を使用してください。

### 線形回帰学習時の学習率をWarm Up するための設定パラメータ
- `IS_USE_WARM_UP`: Warm Up を使用するかどうかのブーリアン
以下は`IS_USE_WARM_UP`がTrueの時有効
- `INITIAL_ETA`: 最初のエポックでの学習率
- `MAX_ETA`: Warm Up の際の最大学習率
- `MIN_ETA`: Warm Up　後の最小学習率
- `RISE_NUM`: Warm Up 時に `INITIAL_ETA` から`MAX_ETA`　へと遷移するために必要なエポック数
- `DOWN_NUM`: Warm Up 後に `MAX_ETA` から `MIN_ETA` へと遷移するために必要なエポック数



### 遺伝アルゴリズムに関連するパラメータ
- `LIR_RESULT_FOLDER_FOR_GA`: 遺伝アルゴリズムに使用する学習済みモデルファイルが格納されているフォルダへのパス
- `GA_RESULT_FOLDER_NAME`: 遺伝アルゴリズムの結果を格納するフォルダへのパス
- `IS_CHOOSE_FROM_CONDITION`: 遺伝アルゴリズムの結果から、どのデータをクラスタリングするか決定する際に、後述のMIN, MAXの範囲から指定するかどうか。Trueであれば範囲から選択し、Falseであれば後述のGOALに近いものを後述の`NUM_CHOOSE_FROM_GENERATIONS`個選択する
- `GA_DATA_SIZE`:初期世代のデータ数
- `CROSSOVER_PROBABILITY`: 個体に対して交差が起きる確率
- `MUTATION_INDIVISUAL_PROBABILITY`: 個体に対して突然変異が起きる確率
- `MUTATION_FEATURE_PROBABILITY`: 突然変異発生時に、各次元に対して変異が適用される確率
- `ELITE_PERCENTAGE`: 親個体からエリートを選出する割合
- `CROSSOVER_CONFIG `: 交差を行いたい次元に対して１を配置、そうでない次元に対しては０を配置したリスト, 例えば10次元あるデータの前半３個、後半３個の次元に対して交差を行いたいときは[1,1,1,0,0,0,0,1,1,1]とする。
    CROSSOVER_CONFIG で1と設定された特徴量のみ突然変異が発生する。
- `NUMBER_OF_GOALS`: 遺伝アルゴリズムを適用する目的変数の次元
- `GOAL`: 遺伝アルゴリズムで目標値となる値。例えば、２変数に対して最適化をする上で、変数１は1.0, 変数２は2.0に近づくように最適化したいときは、[1, 2]と設定する。
- `ITERATION`: 代何世代まで作るか
以下のパラメータは、前述の`IS_CHOOSE_FROM_CONDITION`がTrueの時に有効
- `CHOOSE_Y1_MIN`: 全世代の中から適した予測値のデータを選ぶときの特徴量１の下限
- `CHOOSE_Y1_MAX`: 全世代の中から適した予測値のデータを選ぶときの特徴量１の上限
- `CHOOSE_Y2_MIN`: 全世代の中から適した予測値のデータを選ぶときの特徴量２の下限
- `CHOOSE_Y2_MAX`: 全世代の中から適した予測値のデータを選ぶときの特徴量２の上限



### kmeansクラスタリングに関連するパラメータ
- `KMEANS_RESULT_FOLDER_NAME`: クラスタリングの結果を格納するためのフォルダへのパス
- `K`: クラスターの数
- `KMEANS_ROUND`: kmeans 何回重心をアップデートするか


### ノイズサービスに関連するパラメータ (基本的にディフォルト値を使う)

- `PREPROCESS_DEPERM_DIFF_GA_SERVICE`: GAサービスがクライアントから、目標値との差分データをもらったときに、前処理として逆並び替えをするかどうか（Trueとする）
- `PREPROCESS_DENOISE_PARENTS_GA_SERVICE`: GAサービスがクライアントから、親世代のデータをもらったときに、前処理としてノイズ除去をするかどうか（Noneとする)
- `POSTPROCESS_NOISE_RESULT_GA_SERVICE`: GAサービスが結果として得られた次世代データに対してノイズを付与するかどうか
- `POSTPROCESS_PERM_RESULT_LIR_SERVICE`: LIRサービスが結果として得られた予測変数を並び替えて返すかどうか(Trueとする)
- `PREPROCESS_DENOISE_DATA_LIR_SERVICE`: LIRサービスがクライアントから得たデータに対して、予測前にノイズを除去するかどうか（None)とする
- `POSTPROCESS_PERM_DISTANCE_KMEANS_SERVICE`: KMEANSサービスが、計算された入力データとクラスター重心データの距離を並び替えて返却するかどうか（True)
- `PREPROCESS_DEPERM_DISTANCE_KMEANS_SERVICE`: KMEANSサービスが、クラスター重心の更新の際に、計算された入力データとクラスター重心データの距離を逆並び替えしてから開始するかどうか（True)


Noneがディフォルト値である項目は、プロトコルに対して値が変動することがあるから。


### 結果をプロットする時のパラメータ
plot_utils.py で参照される。
- `PLOT_MIN_X`: X軸の最小値
- `PLOT_MAX_X`: X軸の最大値
- `PLOT_MIN_Y`: Y軸の最小値
- `PLOT_MAX_Y`: Y軸の最大値
- `SCATTER_SIZE`: 散布図のドットの大きさ


## 遺伝アルゴリズム使用時の特定カラムの値の固定

上述の

CROSSOVER_CONFIG
が1に設定されたカラムは、

- ２点交叉
- 突然変異

の対象となります。

したがって、遺伝アルゴリズムを使用する際に、データの特定のカラムの値を固定したい場合、以下の設定をしてください。

- 対象カラムの`CROSSOVER_CONFIG`を０にする
- 最初の親データ（ランダムに生成、もしくはベースとなるデータセットがあると考えられる）に対して、対象カラムのデータは固定したい値に（前もって）設定しておく

