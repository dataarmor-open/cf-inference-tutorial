//var async = require('async');
import async from "async"
//var fs = require('fs'); var parseArgs = require('minimist');
//var path = require('path');
//var _ = require('lodash');
import grpc from "@grpc/grpc-js"
import protoLoader from "@grpc/proto-loader"
//var grpc = require('@grpc/grpc-js');
//var protoLoader = require('@grpc/proto-loader');
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

class f64_vector {
    constructor(data) {
        this.attrs = data.length
        this.data = data
    }

    shape() {
        console.log(`(${this.attrs})`)
    }
}

class f64_matrix {
    constructor(attrs, data) {
        this.attrs = attrs
        this.ds = data.length / attrs
        this.data = data
    }

    get(i) {
        const tmp = this.data.slice(this.attrs * i, this.attrs * (i + 1))
        const res = new f64_vector(tmp)
        return res
    }

    shape() {
        console.log(`(${this.attrs}, ${this.ds})`)
    }
}

const shuffle = ([...array]) => {
    for (let i = array.length - 1; i >= 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

const get_train_test_indices = (ds, test_ratio = 0.2) => {
    if (test_ratio < 0 || test_ratio > 1) {
        throw ("invalid test_ratio")
    }
    const array = Array.from(Array(ds), (v, k) => k)
    const shuffled_array = shuffle(array)
    const test_size = parseInt(ds * test_ratio)

    const test_indices = shuffled_array.slice(0, test_size)
    const train_indices = shuffled_array.slice(test_size, ds)

    return [train_indices, test_indices]

}

const get_random_scalar = () => {
    return getRandomArbitrary(-1, 1)
}

const get_random_vector = (attrs) => {
    var tmp = new Float64Array(attrs)
    for (let i = 0; i < attrs; i++) {
        tmp[i] = getRandomArbitrary(-1, 1)
    }
    const res = new f64_vector(tmp)
    return res
}

const get_random_matrix = (attrs, ds) => {
    var tmp = new Float64Array(attrs * ds)
    for (let i = 0; i < attrs * ds; i++) {
        tmp[i] = getRandomArbitrary(-1, 1)
    }
    const res = new f64_matrix(attrs, tmp)
    return res
}

const matrix_mul_vector = (m, x) => {
    if (m.constructor !== f64_matrix || x.constructor !== f64_vector) {
        throw ("invalid type")
    }
    if (m.attrs != x.attrs) {
        throw ("attrs mismatch")
    }

    var tmp = new Float64Array(m.ds)
    for (let i = 0; i < m.ds; i++) {
        tmp[i] = inner(m.get(i), x)
    }
    const res = new f64_vector(tmp)
    return res

}

const inner = (x1, x2) => {
    if (x1.constructor !== f64_vector || x2.constructor !== f64_vector) {
        throw ("invalid type")
    }
    if (x1.attrs != x2.attrs) {
        throw ("attrs mismatch")
    }
    var tmp = new Float64Array(x1.attrs)
    for (let i = 0; i < x1.attrs; i++) {
        tmp[i] = x1.data[i] * x2.data[i]
    }

    var res = tmp[0]
    for (let i = 1; i < x1.attrs; i++) {
        res += tmp[i]
    }
    return res
}

const vector_mul_vector = (x1, x2) => {
    if (x1.constructor !== f64_vector || x2.constructor !== f64_vector) {
        throw ("invalid type")
    }
    if (x1.attrs != x2.attrs) {
        throw ("attrs mismatch")
    }
    var tmp = new Float64Array(x1.attrs)
    for (let i = 0; i < x1.attrs; i++) {
        tmp[i] = x1[i] * x2[i]
    }
    const res = new f64_vector(tmp)
    return res
}

const vector_add_vector = (x1, x2) => {
    if (x1.constructor !== f64_vector || x2.constructor !== f64_vector) {
        throw ("invalid type")
    }
    if (x1.attrs != x2.attrs) {
        throw ("attrs mismatch")
    }
    var tmp = new Float64Array(x1.attrs)
    for (let i = 0; i < x1.attrs; i++) {
        tmp[i] = x1[i] + x2[i]
    }
    const res = new f64_vector(tmp)
    return res
}

const vector_add_scalar = (x, s) => {
    if (x.constructor !== f64_vector) {
        throw ("invalid type")
    }
    var tmp = new Float64Array(x.attrs)
    for (let i = 0; i < x.attrs; i++) {
        tmp[i] = x.data[i] + s
    }
    const res = new f64_vector(tmp)
    return res
}


const get_random_dataset = (attrs, ds) => {
    const xs = get_random_matrix(attrs, ds)
    const w = get_random_vector(attrs)
    const b = get_random_scalar()
    const tmp = matrix_mul_vector(xs, w)
    const ys = vector_add_scalar(tmp, b)

    return [xs, ys, w, b]
}


const dynamic_load_input_client = (proto_path, input_client_host, input_client_port) => {
    var PROTO_PATH = proto_path
    var packageDefinition = protoLoader.loadSync(
        PROTO_PATH,
        {
            keepCase: true,
            longs: String,
            enums: String,
            defaults: true,
            oneofs: true
        });

    var input_client = grpc.loadPackageDefinition(packageDefinition);
    var client = new input_client.InputClient(`${input_client_host}:${input_client_port}`,
        grpc.credentials.createInsecure());
    return client;
}

export {
    dynamic_load_input_client, f64_matrix, f64_vector,
    matrix_mul_vector, vector_add_vector, inner, vector_add_scalar,
    get_random_matrix, get_random_vector, get_random_scalar, get_random_dataset,
    get_train_test_indices
}