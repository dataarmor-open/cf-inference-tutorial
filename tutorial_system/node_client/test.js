var async = require('async');
var fs = require('fs'); var parseArgs = require('minimist');
var path = require('path');
var _ = require('lodash');
var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');

const load_input_client = () => {
  var PROTO_PATH = '/Users/kmihara/Documents/cf-inference-tutorial/tutorial_system/proto/input_client.proto'
  var packageDefinition = protoLoader.loadSync(
      PROTO_PATH,
      {keepCase: true,
       longs: String,
       enums: String,
       defaults: true,
       oneofs: true
      });

  var input_client = grpc.loadPackageDefinition(packageDefinition);
  var client = new input_client.InputClient('localhost:5101',
                                         grpc.credentials.createInsecure());
  return client;

}

client = load_input_client()

var message = {
  model_index: 0
}

client.gen_key_for_lir(message, (error, res) => {
  console.log("done");
})
