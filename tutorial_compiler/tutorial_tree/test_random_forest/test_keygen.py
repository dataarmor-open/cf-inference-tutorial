import pickle
import grpc
import sys
sys.path.append("../")

from sklearn import datasets

from grpc_python import input_client_pb2_grpc
from grpc_python import model_client_pb2_grpc

from grpc_python.model import message_pb2
from grpc_python.model import randomForestModel_pb2
from grpc_python.model import treeParameter_pb2
from grpc_python.model import data_pb2



if __name__ == "__main__":
    print("hello, world")

    channel_input_client = grpc.insecure_channel('localhost:5011', options=[])
    api_input_client = input_client_pb2_grpc.InputClientStub(channel_input_client)

    channel_model_client = grpc.insecure_channel('localhost:5012', options=[])
    api_model_client = model_client_pb2_grpc.ModelClientStub(channel_model_client)

    api_input_client.gen_key(message_pb2.PB_Message())
