import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
import numpy as np
import pickle


from sklearn.decomposition import PCA

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc

from grpc_service import common_model
from grpc_service import noise_model
from grpc_service import kmeans_model

from optimization import ga_process
from optimization import kmeans_process
from optimization import plot_utils

import constants_client as c


def main():
    colors = plot_utils.get_colors()
    options = [('grpc.max_message_length', c.GRPC_MAX_MESSAGE_LENGTH),('grpc.max_receive_message_length', c.GRPC_MAX_MESSAGE_LENGTH)]

    ## input_client への接続を確立
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    ## noise サービスへの接続を確立(チュートリアルのために接続、普通は接続を制限している)
    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)


    # 遺伝アルゴリズム最適化スタート====================================================

    # 第一世代のみlir_and_ga にはファイルからロードしたデータを使用する（もしくは自分でランダム生成する）
    # lir_and_ga の is_need_denoise はFalseに設定する（ロードしたデータにはノイズが含まれていない)
    # 最適化するデータをロード
    #xs, ys, w, b = data.load_data()
    xs = np.random.randn(c.GA_DATA_SIZE, c.ATTRS)
    # サービスの利用自体には必要ないが、プロットするためにPCAを行う
    pca = PCA(n_components=2)
    pca = pca.fit(xs)


    # GAにより得られた全世代データと、それらの予測結果をロード
    all_children_denoised, all_children_results_depermed = ga_process.load_all_generations_data()

    all_children_denoised = all_children_denoised.reshape(-1, all_children_denoised.shape[2])
    all_children_results_depermed = all_children_results_depermed.reshape(-1, all_children_results_depermed.shape[2])


    # LIRにより学習された重みをロード
    #for model_index in [0,1]:
    #model_index = 0
    for model_index in [0, 1]:
        print(f"\n\nLIR{model_index} assertion=========================================")
        with open(f"{c.LIR_RESULT_FOLDER_FOR_GA}/w_b{model_index}.pkl", "rb") as f:
            w_b = pickle.load(f)
        w = w_b[:c.ATTRS]
        b = w_b[-1]


        # 第一世代以降(all_children_denoised)をLIRによって予測し、結果がall_children_results_depermed
        # と正しいことを確認する

        for i in range(len(all_children_results_depermed)):
            res = np.dot(all_children_denoised[i], w) + b
            #print(f"{res.shape}, {all_children_results_depermed.shape}")
            # print(f"{res}")
            # print(f"{all_children_results_depermed[i][0]}")
            print(f"{res}, {all_children_results_depermed[i][model_index]}")
            assert np.isclose(res, all_children_results_depermed[i][model_index], rtol=1e-02), f"\nnot close\n{res}\n{all_children_results_depermed[i][model_index]}"

if __name__ == "__main__":
    print("hello, world")









