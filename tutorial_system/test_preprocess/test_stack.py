from re import L
import sys
from turtle import color

from cf_core import CKKSSerializeService
import grpc
import random
import numpy as np
import pickle
import time
import json
import matplotlib.pyplot as plt

# from ..grpc_service import noise_service_pb2_grpc

# from ..grpc_service import ga_mo/del
# from ..grpc_service import noise_model
# from ..grpc_service import kmeans_model
from sklearn.preprocessing import MinMaxScaler
from sklearn.datasets import make_regression
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split

sys.path.append("../")
from grpc_service import common_model
from grpc_service.input_client_pb2_grpc import InputClientStub
import constants_client as c
from sklearn.model_selection import GridSearchCV, KFold, train_test_split
from sklearn.model_selection import ShuffleSplit
from sklearn.preprocessing import MinMaxScaler
from cf_core import CKKSEncryptionService

from SealMatrix import CKKSCmatrix
from SealMatrix import CKKSCvector
from SealMatrix import CKKSCveclist
from SealMatrix import CKKSMatrixService
from SealMatrix import CKKSMatrixdistance


def make_pb_from_doubleList(doubleList):
    if len(doubleList.shape) == 2:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = np.ravel(doubleList)
        pb.size = doubleList.shape[1]
    else:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = doubleList
        pb.size = 0
    return pb


def make_doubleList_from_pb(pb_doubleList):
    if pb_doubleList.size is not 0:
        tmp = np.array(pb_doubleList.data)
        data = tmp.reshape(-1, pb_doubleList.size)
    else:
        data = np.array(pb_doubleList.data)
    return data


def make_pb_from_trainTestIndices(train_indices, test_indices):
    pb = common_model.trainTestIndices_pb2.PB_TrainTestIndices()
    pb.train[:] = train_indices
    pb.test[:] = test_indices
    return pb


options = [
    ("grpc.max_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
    ("grpc.max_receive_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
]
channel_input_client = grpc.insecure_channel(f"{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}", options=options)
stub = InputClientStub(channel_input_client)
stub.preprocess_gen_key(common_model.message_pb2.PB_Message())
stub.preprocess_reset_data(common_model.message_pb2.PB_Message())


def test_x_hstack():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=100, n_features=20, random_state=1)
    xs = np.hstack((xs_1, xs_2))
    pb_xs_1 = make_pb_from_doubleList(xs_1)
    stub.preprocess_transfer_x_hstack(pb_xs_1)
    pb_xs_2 = make_pb_from_doubleList(xs_2)
    stub.preprocess_transfer_x_hstack(pb_xs_2)
    pb_xs = stub.debug_insecure_get_x(common_model.message_pb2.PB_Message())
    res = make_doubleList_from_pb(pb_xs)
    assert np.isclose(xs, res, rtol=1e-02, atol=0).all()


def test_x_vstack():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=80, n_features=50, random_state=1)
    xs = np.vstack((xs_1, xs_2))
    pb_xs_1 = make_pb_from_doubleList(xs_1)
    stub.preprocess_transfer_x_vstack(pb_xs_1)
    pb_xs_2 = make_pb_from_doubleList(xs_2)
    stub.preprocess_transfer_x_vstack(pb_xs_2)

    pb_xs = stub.debug_insecure_get_x(common_model.message_pb2.PB_Message())
    res = make_doubleList_from_pb(pb_xs)
    assert np.isclose(xs, res, rtol=1e-02, atol=0).all()


def test_y_vstack():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=80, n_features=50, random_state=1)
    xs = np.hstack((ys_1, ys_2))
    pb_ys_1 = make_pb_from_doubleList(ys_1)
    stub.preprocess_transfer_y_vstack(pb_ys_1)
    pb_ys_2 = make_pb_from_doubleList(ys_2)
    stub.preprocess_transfer_y_vstack(pb_ys_2)

    pb_ys = stub.debug_insecure_get_y(common_model.message_pb2.PB_Message())
    res = make_doubleList_from_pb(pb_ys)
    assert np.isclose(xs, res, rtol=1e-02, atol=0).all()


def test_train_test_split():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=80, n_features=50, random_state=1)
    xs = np.vstack((xs_1, xs_2))
    ys = np.hstack((ys_1, ys_2))

    pb_xs_1 = make_pb_from_doubleList(xs_1)
    stub.preprocess_transfer_x_vstack(pb_xs_1)
    pb_xs_2 = make_pb_from_doubleList(xs_2)
    stub.preprocess_transfer_x_vstack(pb_xs_2)

    pb_ys_1 = make_pb_from_doubleList(ys_1)
    stub.preprocess_transfer_y_vstack(pb_ys_1)
    pb_ys_2 = make_pb_from_doubleList(ys_2)
    stub.preprocess_transfer_y_vstack(pb_ys_2)

    ss = ShuffleSplit(n_splits=3)
    for train_indices, test_indices in ss.split(xs, ys):
        plain_x_train = xs[train_indices, :]
        plain_x_test = xs[test_indices, :]
        plain_y_train = ys[train_indices]
        plain_y_test = ys[test_indices]

        pb = make_pb_from_trainTestIndices(train_indices, test_indices)
        stub.preprocess_train_test_split_with_indices(pb)

        pb_x_train = stub.debug_insecure_get_x_train(common_model.message_pb2.PB_Message())
        dec_x_train = make_doubleList_from_pb(pb_x_train)
        assert np.isclose(plain_x_train, dec_x_train, rtol=1e-02, atol=0).all()

        pb_x_test = stub.debug_insecure_get_x_test(common_model.message_pb2.PB_Message())
        dec_x_test = make_doubleList_from_pb(pb_x_test)
        assert np.isclose(plain_x_test, dec_x_test, rtol=1e-02, atol=0).all()

        pb_y_train = stub.debug_insecure_get_y_train(common_model.message_pb2.PB_Message())
        dec_y_train = make_doubleList_from_pb(pb_y_train)
        assert np.isclose(plain_y_train, dec_y_train, rtol=1e-02, atol=0).all()

        pb_y_test = stub.debug_insecure_get_y_test(common_model.message_pb2.PB_Message())
        dec_y_test = make_doubleList_from_pb(pb_y_test)
        assert np.isclose(plain_y_test, dec_y_test, rtol=1e-02, atol=0).all()


def test_kfold():
    xs, ys = make_regression(n_samples=20, n_features=6, random_state=1)
    kf = KFold(n_splits=10, shuffle=True)
    for train_index, test_index in kf.split(xs, ys):
        print("train_index:", train_index, "test_index:", test_index)


def test_max():
    xs, ys = make_regression(n_samples=20, n_features=6, random_state=1)
    scalar = MinMaxScaler()
    scalar.fit(xs)
    service = CKKSEncryptionService(1, pow(2, 40), [60, 40, 60], 2**13)
    mat_service = CKKSMatrixService(service)

    enc_xs = mat_service.encrypt_veclist(xs)
    # max
    maxes = enc_xs
    while 1:
        # server
        if maxes.plain_ds % 2 is not 0:
            tmp = int((maxes.plain_ds - 1) // 2)
            set_first = np.arange(tmp)
            set_second = np.arange(tmp, tmp * 2)
            mat_seed = maxes.extract_in_row([tmp * 2])
        else:
            set_first = np.arange(int(maxes.plain_ds // 2))
            set_second = np.arange(int(maxes.plain_ds // 2), maxes.plain_ds)
            mat_seed = None
        mat_first = maxes.extract_in_row(set_first)
        mat_second = maxes.extract_in_row(set_second)
        mat_result = mat_first - mat_second
        rand_scale = np.random.rand(mat_result.plain_ds, mat_result.plain_attrs)
        rand_sign = np.random.choice([1, -1], (mat_result.plain_ds, mat_result.plain_attrs))
        rand = rand_scale * rand_sign
        noised_result = mat_service.veclist_mul_numpy(mat_result, rand)

        # client
        res = mat_service.decrypt_veclist(noised_result)
        send = np.sign(res)

        # server
        result = rand_sign * send
        greater = np.where(result == 1, 1, 0)
        less = np.where(result == 1, 0, 1)

        enc_greater = mat_service.encrypt_veclist(greater)
        enc_less = mat_service.encrypt_veclist(less)

        greater_mat_first = mat_first * enc_greater
        greater_mat_second = mat_second * enc_less
        maxes = greater_mat_first + greater_mat_second

        # client
        dec_maxes = mat_service.decrypt_veclist(maxes)
        maxes = mat_service.encrypt_veclist(dec_maxes)

        # server
        if mat_seed is not None:
            maxes = maxes.vstack(mat_seed)

        if maxes.plain_ds == 1:
            max = maxes
            break
    dec_max = mat_service.decrypt_veclist(max)[0]
    assert np.isclose(scalar.data_max_, dec_max, rtol=1e-02, atol=0).all()


def test_max_grpc():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=80, n_features=50, random_state=1)
    xs = np.vstack((xs_1, xs_2))
    ys = np.hstack((ys_1, ys_2))

    pb_xs_1 = make_pb_from_doubleList(xs_1)
    stub.preprocess_transfer_x_vstack(pb_xs_1)
    pb_xs_2 = make_pb_from_doubleList(xs_2)
    stub.preprocess_transfer_x_vstack(pb_xs_2)

    pb_ys_1 = make_pb_from_doubleList(ys_1)
    stub.preprocess_transfer_y_vstack(pb_ys_1)
    pb_ys_2 = make_pb_from_doubleList(ys_2)
    stub.preprocess_transfer_y_vstack(pb_ys_2)

    ss = ShuffleSplit(n_splits=3)

    for train_indices, test_indices in ss.split(xs, ys):
        plain_x_train = xs[train_indices, :]
        plain_x_test = xs[test_indices, :]
        plain_y_train = ys[train_indices]
        plain_y_test = ys[test_indices]
        scalar = MinMaxScaler()
        scalar.fit(plain_x_train)
        print(np.argmax(plain_x_train, axis=0))

        pb = make_pb_from_trainTestIndices(train_indices, test_indices)
        stub.preprocess_train_test_split_with_indices(pb)

        pb_x_train = stub.debug_insecure_get_x_train(common_model.message_pb2.PB_Message())
        dec_x_train = make_doubleList_from_pb(pb_x_train)
        # assert np.isclose(plain_x_train, dec_x_train, rtol=1e-02, atol=0).all()

        pb_x_test = stub.debug_insecure_get_x_test(common_model.message_pb2.PB_Message())
        dec_x_test = make_doubleList_from_pb(pb_x_test)
        # assert np.isclose(plain_x_test, dec_x_test, rtol=1e-02, atol=0).all()

        pb_y_train = stub.debug_insecure_get_y_train(common_model.message_pb2.PB_Message())
        dec_y_train = make_doubleList_from_pb(pb_y_train)
        # assert np.isclose(plain_y_train, dec_y_train, rtol=1e-02, atol=0).all()

        pb_y_test = stub.debug_insecure_get_y_test(common_model.message_pb2.PB_Message())
        dec_y_test = make_doubleList_from_pb(pb_y_test)
        # assert np.isclose(plain_y_test, dec_y_test, rtol=1e-02, atol=0).all()

        stub.preprocess_apply_max_of_x_train(common_model.message_pb2.PB_Message())
        pb_max = stub.debug_insecure_get_max(common_model.message_pb2.PB_Message())
        dec_max = make_doubleList_from_pb(pb_max)
        assert np.isclose(scalar.data_max_, dec_max, rtol=1e-02, atol=0).all()
        break


def test_min_grpc():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=80, n_features=50, random_state=1)
    xs = np.vstack((xs_1, xs_2))
    ys = np.hstack((ys_1, ys_2))

    pb_xs_1 = make_pb_from_doubleList(xs_1)
    stub.preprocess_transfer_x_vstack(pb_xs_1)
    pb_xs_2 = make_pb_from_doubleList(xs_2)
    stub.preprocess_transfer_x_vstack(pb_xs_2)

    pb_ys_1 = make_pb_from_doubleList(ys_1)
    stub.preprocess_transfer_y_vstack(pb_ys_1)
    pb_ys_2 = make_pb_from_doubleList(ys_2)
    stub.preprocess_transfer_y_vstack(pb_ys_2)

    ss = ShuffleSplit(n_splits=3)

    for train_indices, test_indices in ss.split(xs, ys):
        plain_x_train = xs[train_indices, :]
        plain_x_test = xs[test_indices, :]
        plain_y_train = ys[train_indices]
        plain_y_test = ys[test_indices]
        scalar = MinMaxScaler()
        scalar.fit(plain_x_train)

        pb = make_pb_from_trainTestIndices(train_indices, test_indices)
        stub.preprocess_train_test_split_with_indices(pb)

        pb_x_train = stub.debug_insecure_get_x_train(common_model.message_pb2.PB_Message())
        dec_x_train = make_doubleList_from_pb(pb_x_train)
        # assert np.isclose(plain_x_train, dec_x_train, rtol=1e-02, atol=0).all()

        pb_x_test = stub.debug_insecure_get_x_test(common_model.message_pb2.PB_Message())
        dec_x_test = make_doubleList_from_pb(pb_x_test)
        # assert np.isclose(plain_x_test, dec_x_test, rtol=1e-02, atol=0).all()

        pb_y_train = stub.debug_insecure_get_y_train(common_model.message_pb2.PB_Message())
        dec_y_train = make_doubleList_from_pb(pb_y_train)
        # assert np.isclose(plain_y_train, dec_y_train, rtol=1e-02, atol=0).all()

        pb_y_test = stub.debug_insecure_get_y_test(common_model.message_pb2.PB_Message())
        dec_y_test = make_doubleList_from_pb(pb_y_test)
        # assert np.isclose(plain_y_test, dec_y_test, rtol=1e-02, atol=0).all()

        stub.preprocess_apply_min_of_x_train(common_model.message_pb2.PB_Message())
        pb_min = stub.debug_insecure_get_min(common_model.message_pb2.PB_Message())
        dec_min = make_doubleList_from_pb(pb_min)
        assert np.isclose(scalar.data_min_, dec_min, rtol=1e-02, atol=0).all()
        break


def test_min():
    xs, ys = make_regression(n_samples=20, n_features=6, random_state=1)
    scalar = MinMaxScaler()
    scalar.fit(xs)
    service = CKKSEncryptionService(1, pow(2, 40), [60, 40, 60], 2**13)
    mat_service = CKKSMatrixService(service)

    enc_xs = mat_service.encrypt_veclist(xs)
    # max
    mins = enc_xs
    while 1:
        # server
        if mins.plain_ds % 2 is not 0:
            tmp = int((mins.plain_ds - 1) // 2)
            set_first = np.arange(tmp)
            set_second = np.arange(tmp, tmp * 2)
            mat_seed = mins.extract_in_row([tmp * 2])
        else:
            set_first = np.arange(int(mins.plain_ds // 2))
            set_second = np.arange(int(mins.plain_ds // 2), mins.plain_ds)
            mat_seed = None
        mat_first = mins.extract_in_row(set_first)
        mat_second = mins.extract_in_row(set_second)
        mat_result = mat_first - mat_second
        rand_scale = np.random.rand(mat_result.plain_ds, mat_result.plain_attrs)
        rand_sign = np.random.choice([1, -1], (mat_result.plain_ds, mat_result.plain_attrs))
        rand = rand_scale * rand_sign
        noised_result = mat_service.veclist_mul_numpy(mat_result, rand)

        # client
        res = mat_service.decrypt_veclist(noised_result)
        send = np.sign(res)

        # server
        result = rand_sign * send
        greater = np.where(result == 1, 1, 0)
        less = np.where(result == 1, 0, 1)

        enc_greater = mat_service.encrypt_veclist(greater)
        enc_less = mat_service.encrypt_veclist(less)

        greater_mat_first = mat_first * enc_less
        greater_mat_second = mat_second * enc_greater
        mins = greater_mat_first + greater_mat_second

        # client
        dec_mins = mat_service.decrypt_veclist(mins)
        mins = mat_service.encrypt_veclist(dec_mins)
        if mat_seed is not None:
            mins = mins.vstack(mat_seed)

        if mins.plain_ds == 1:
            min = mins
            break
    dec_min = mat_service.decrypt_veclist(min)[0]
    assert np.isclose(scalar.data_min_, dec_min, rtol=1e-02, atol=0).all()


def test_minmax():
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    xs_1, ys_1 = make_regression(n_samples=100, n_features=50, random_state=1)
    xs_2, ys_2 = make_regression(n_samples=80, n_features=50, random_state=1)
    xs = np.vstack((xs_1, xs_2))
    ys = np.hstack((ys_1, ys_2))

    pb_xs_1 = make_pb_from_doubleList(xs_1)
    stub.preprocess_transfer_x_vstack(pb_xs_1)
    pb_xs_2 = make_pb_from_doubleList(xs_2)
    stub.preprocess_transfer_x_vstack(pb_xs_2)

    pb_ys_1 = make_pb_from_doubleList(ys_1)
    stub.preprocess_transfer_y_vstack(pb_ys_1)
    pb_ys_2 = make_pb_from_doubleList(ys_2)
    stub.preprocess_transfer_y_vstack(pb_ys_2)

    ss = ShuffleSplit(n_splits=3)

    for train_indices, test_indices in ss.split(xs, ys):
        plain_x_train = xs[train_indices, :]
        plain_x_test = xs[test_indices, :]
        plain_y_train = ys[train_indices]
        plain_y_test = ys[test_indices]
        scalar = MinMaxScaler()
        scalar.fit(plain_x_train)

        pb = make_pb_from_trainTestIndices(train_indices, test_indices)
        stub.preprocess_train_test_split_with_indices(pb)

        pb_x_train = stub.debug_insecure_get_x_train(common_model.message_pb2.PB_Message())
        dec_x_train = make_doubleList_from_pb(pb_x_train)
        assert np.isclose(plain_x_train, dec_x_train, rtol=1e-03, atol=0).all()

        pb_x_test = stub.debug_insecure_get_x_test(common_model.message_pb2.PB_Message())
        dec_x_test = make_doubleList_from_pb(pb_x_test)
        assert np.isclose(plain_x_test, dec_x_test, rtol=1e-03, atol=0).all()

        stub.preprocess_apply_min_of_x_train(common_model.message_pb2.PB_Message())
        pb_min = stub.debug_insecure_get_min(common_model.message_pb2.PB_Message())
        dec_min = make_doubleList_from_pb(pb_min)
        assert np.isclose(scalar.data_min_, dec_min, rtol=1e-03, atol=0).all()

        stub.preprocess_apply_max_of_x_train(common_model.message_pb2.PB_Message())
        pb_max = stub.debug_insecure_get_max(common_model.message_pb2.PB_Message())
        dec_max = make_doubleList_from_pb(pb_max)
        assert np.isclose(scalar.data_max_, dec_max, rtol=1e-03, atol=0).all()

        stub.preprocess_apply_fit_minmax(common_model.message_pb2.PB_Message())
        pb_diff = stub.debug_insecure_get_inv_diff(common_model.message_pb2.PB_Message())
        dec_diff = make_doubleList_from_pb(pb_diff)
        assert np.isclose(1 / (scalar.data_max_ - scalar.data_min_), dec_diff, rtol=1e-03, atol=0).all()

        plain_x_test = scalar.transform(plain_x_test)
        stub.preprocess_apply_minmax_transform_x_test(common_model.message_pb2.PB_Message())
        pb_x_test = stub.debug_insecure_get_x_test(common_model.message_pb2.PB_Message())
        dec_x_test = make_doubleList_from_pb(pb_x_test)
        # res = np.isclose(plain_x_test, dec_x_test, rtol=1e-03, atol=1e-06)
        # for i in range(res.shape[0]):
        #     for j in range(res.shape[1]):
        #         if res[i][j] == False:
        #             print(plain_x_test[i][j],dec_x_test[i][j])
        assert np.isclose(plain_x_test, dec_x_test, rtol=1e-03, atol=1e-06).all()

        plain_x_train = scalar.transform(plain_x_train)
        stub.preprocess_apply_minmax_transform_x_train(common_model.message_pb2.PB_Message())
        pb_x_train = stub.debug_insecure_get_x_train(common_model.message_pb2.PB_Message())
        dec_x_train = make_doubleList_from_pb(pb_x_train)
        # print("a")
        # res = np.isclose(plain_x_train, dec_x_train, rtol=1e-03, atol=1e-06)
        # for i in range(res.shape[0]):
        #     for j in range(res.shape[1]):
        #         if res[i][j] == False:
        #             print(plain_x_train[i][j],dec_x_train[i][j])
        assert np.isclose(plain_x_train, dec_x_train, rtol=1e-03, atol=1e-06).all()

        break
