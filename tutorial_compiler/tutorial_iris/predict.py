# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

def parse_time_stats(test_dict):
  dense_time = 0
  conv_time = 0
  communication_time = 0
  other_time = 0
  total_time = 0
  for k, v in test_dict.items():
    total_time += v
    if k is None:
      continue
    if k.lower().startswith('conv'):
      conv_time += v
    elif k.lower().startswith('dense'):
      dense_time += v
    elif k.lower().startswith('communication'):
      communication_time += v
    else:
      other_time += v

  print('\n=== time stats ==== ')
  print(f'total_time >>> {total_time}')
  print(f'conv_time  >>> {conv_time}')
  print(f'dense_time >>> {dense_time}')
  print(f'communication_time >>> {communication_time}')
  print(f'other_time >>> {other_time}')


def dispense_data():
  iris = load_iris()

  data_x = iris.data
  data_y = iris.target
  data_y = np_utils.to_categorical(data_y)

  x_train, x_test, y_train, y_test = train_test_split(data_x, data_y, test_size=0.2)

  return x_train, x_test, y_train, y_test

if __name__ == "__main__":
  model_name = 'nn_iris'

  # set model index
  model_index = 0

  # raw inference for comparison
  model = keras.models.load_model(f"{model_name}.h5")


  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()


  # prepare data for inference
  x, y, _, _  = dispense_data()
  test_data = x[0]

  # call input_cleint to do inference
  print("call predict")

  t1 = time.time()

  data_for_cf = test_data.tolist()
  input_data = Tensor()
  input_data.data = pickle.dumps(data_for_cf)
  input_data.model_index = model_index

  # call prediction
  result = api_input_client.predict(input_data)
  result = pickle.loads(result.data)

  t2 = time.time()

  #result_raw = model.predict(test_data)
  result_raw = model.predict(test_data.reshape(1,4))

  print("\n===============")
  print("cipher_result")
  print(result)
  print(np.argmax(result))
  print("\n===============")
  print("raw_result")
  print(result_raw)
  print(np.argmax(result_raw))

  print("\n===============")
  print(f'time for cipher prediction >>> {t2-t1}')

  time_stats = api_input_client.get_time_stats(NoParam(model_index=0))
  parse_time_stats(pickle.loads(time_stats.data))



