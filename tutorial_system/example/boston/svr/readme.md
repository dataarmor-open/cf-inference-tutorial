# ボストンの住宅価格データセットを用いたサポートベクター回帰モデルの学習と、推論の使用方法の解説

本チュートリアルではボストンの住宅価格データセットを暗号化した状態で、サポートベクター回帰によって学習、推論します。

0. `0_enc_keygen.py`　鍵の生成
1. `1_a_enc_train.py` データセットをサーバーに送信した後、前処理をしたあと、学習する。
2. `2_enc_predict.py` 学習済みモデルでの推論。

を行います。また、1.の代わりに`1_b_enc_gridsearch.py`を行うとパラメータを探索しながら学習を行います。

## 実行方法
検証用コンテナ内で、
```
    > source setup.sh
    > cd tutorial_system/example/boston/svr
    > python 0_enc_keygen.py
    > python 1_a_enc_train.py
    > python 2_enc_predict.py
```
によって動作が確認できます。

## 実行時間の目安
本チュートリアルは暗号文で行われるため、実行時間が非常に長くなっています。下記にそれぞれのプログラムの実行時間の目安を記します。

|  プログラム  |  実行時間  |
| ---- | ---- |
|  `0_enc_keygen.py`  |  2秒  |
|  `1_a_enc_train.py`  |  26分  |
|  `1_b_enc_gridsearch.py`  |  14時間  |
|  `2_enc_predict.py`  |  18秒  |
