import random
import numpy as np
from cf_core import CKKSSerializeService
from cf_core import CKKSCipherUtils

from grpc_service import input_client_pb2
from grpc_service import input_client_pb2_grpc
from grpc_service import preprocess_pb2
from grpc_service import preprocess_pb2_grpc
from grpc_service import common_model
from grpc_service import noise_service_pb2_grpc
from SealMatrix import CKKSCmatrix
from SealMatrix import CKKSCvector
from SealMatrix import CKKSCveclist
from SealMatrix import CKKSMatrixService
from SealMatrix import CKKSMatrixdistance


class GrpcConverter:
    def __init__(self, service):
        self.service = service

    def make_encList_from_streamed_pb(self, streamed_pb):
        request_list = []
        for el in streamed_pb:
            attrs = el.size
            request_list = request_list + list(el.data)
        encList = CKKSSerializeService.deserialize_ctxt_1d(
            request_list, self.service, False
        )
        return encList, attrs

    def make_enc_from_pb(self, pb):
        enc = CKKSSerializeService.deserialize_ctxt(pb.data, self.service, False)
        attrs = pb.index
        return enc, attrs

    def make_doubleList_from_pb(self, pb_doubleList):
        if pb_doubleList.size is not 0:
            tmp = np.array(pb_doubleList.data)
            data = tmp.reshape(-1, pb_doubleList.size)
        else:
            data = np.array(pb_doubleList.data)
        return data

    def make_intList_from_pb(self, pb_intList):
        if pb_intList.size is not 0:
            tmp = np.array(pb_intList.data)
            data = tmp.reshape(-1, pb_intList.size)
        else:
            data = np.array(pb_intList.data)
        return data

    def make_indexIntList_from_pb(self, pb_intList):
        if pb_intList.size is not 0:
            tmp = np.array(pb_intList.data)
            data = tmp.reshape(-1, pb_intList.size)
        else:
            data = np.array(pb_intList.data)
        return pb_intList.index, data

    def make_pb_from_doubleList(self, doubleList):
        if len(doubleList.shape) == 2:
            pb = common_model.doubleList_pb2.PB_DoubleList()
            pb.data[:] = np.ravel(doubleList)
            pb.size = doubleList.shape[1]
        else:
            pb = common_model.doubleList_pb2.PB_DoubleList()
            pb.data[:] = doubleList
            pb.size = 0
        return pb

    def make_pb_from_intList(self, intList):
        if len(intList.shape) == 2:
            pb = common_model.intList_pb2.PB_IntList()
            pb.data[:] = np.ravel(intList)
            pb.size = intList.shape[1]
        else:
            pb = common_model.intList_pb2.PB_IntList()
            pb.data[:] = intList
            pb.size = 0
        return pb

    def make_pb_from_indexIntList(self, index, intList):
        if len(intList.shape) == 2:
            pb = common_model.indexIntList_pb2.PB_IndexIntList()
            pb.data[:] = np.ravel(intList)
            pb.size = intList.shape[1]
            pb.index = index
        else:
            pb = common_model.indexIntList_pb2.PB_IndexIntList()
            pb.data[:] = intList
            pb.size = 0
            pb.index = index
        return pb

    def make_stream_pb_from_enc_List(self, enc_list, attrs, size=100):
        enc_list_encoded = CKKSSerializeService.serialize_ctxt_1d(enc_list)
        while len(enc_list_encoded) > 0:
            if len(enc_list_encoded) < size:
                pb_return = common_model.encList_pb2.PB_EncList()
                pb_return.data[:] = enc_list_encoded
                pb_return.size = attrs
                del enc_list_encoded[: len(enc_list_encoded)]
                yield pb_return

            pb_return = common_model.encList_pb2.PB_EncList()
            pb_return.data[:] = enc_list_encoded[:size]
            pb_return.size = attrs
            del enc_list_encoded[:size]
            yield pb_return

    def make_pb_from_enc(self, enc, attrs):
        pb = common_model.enc_pb2.PB_Enc()
        pb.data = CKKSSerializeService.serialize_ctxt(enc)
        pb.index = attrs
        return pb

    def make_pb_from_CKKSCveclist(self, matrix: CKKSCveclist):
        pb_CKKSCmatrix = common_model.CKKSCmatrix_pb2.PB_CKKSCmatrix()
        ser = CKKSSerializeService.serialize_ctxt_1d(matrix.matrix)
        pb_CKKSCmatrix.matrix[:] = ser
        pb_CKKSCmatrix.plain_ds = matrix.plain_ds
        pb_CKKSCmatrix.plain_attrs = matrix.plain_attrs
        pb_CKKSCmatrix.level = matrix.level
        return pb_CKKSCmatrix

    def make_CKKSCveclist_from_pb(self, pb):
        mat = CKKSCveclist()
        mat.matrix = CKKSSerializeService.deserialize_ctxt_1d(
            pb.matrix, self.service, False
        )
        mat.plain_ds = pb.plain_ds
        mat.plain_attrs = pb.plain_attrs
        mat.level = pb.level
        return mat

    def make_pb_from_CKKSCvector(self, vector: CKKSCvector):
        pb_CKKSCvector = common_model.CKKSCvector_pb2.PB_CKKSCvector()
        ser = CKKSSerializeService.serialize_ctxt(vector.vector)
        pb_CKKSCvector.vector = ser
        pb_CKKSCvector.plain_attrs = vector.plain_attrs
        pb_CKKSCvector.sum_place = vector.sum_place
        pb_CKKSCvector.level = vector.level
        pb_CKKSCvector.trans = vector.trans
        pb_CKKSCvector.coeff = vector.coeff
        return pb_CKKSCvector

    def make_CKKSCvector_from_pb(self, pb):
        vec = CKKSCvector()
        vec.vector = CKKSSerializeService.deserialize_ctxt(
            pb.vector, self.service, False
        )
        vec.plain_attrs = pb.plain_attrs
        vec.sum_place = pb.sum_place
        vec.level = pb.level
        vec.trans = pb.trans
        vec.coeff = pb.coeff
        return vec

    def make_CKKSCmatrix_from_pb(self, pb):
        mat = CKKSCmatrix()
        mat.matrix = CKKSSerializeService.deserialize_ctxt_1d(
            pb.matrix, self.service, False
        )
        mat.plain_ds = pb.plain_ds
        mat.plain_attrs = pb.plain_attrs
        mat.max_pack = pb.max_pack
        mat.last_pack = pb.last_pack
        mat.level = pb.level
        mat.trans = pb.trans
        mat.coeff = pb.coeff
        mat.sum_place = pb.sum_place
        return mat

    def make_pb_from_CKKSCmatrix(self, matrix: CKKSCmatrix):
        pb_CKKSCmatrix = common_model.CKKSCmatrix_pb2.PB_CKKSCmatrix()
        ser = CKKSSerializeService.serialize_ctxt_1d(matrix.matrix)
        pb_CKKSCmatrix.matrix[:] = ser
        pb_CKKSCmatrix.plain_ds = matrix.plain_ds
        pb_CKKSCmatrix.plain_attrs = matrix.plain_attrs
        pb_CKKSCmatrix.max_pack = matrix.max_pack
        pb_CKKSCmatrix.last_pack = matrix.last_pack
        pb_CKKSCmatrix.level = matrix.level
        pb_CKKSCmatrix.trans = matrix.trans
        pb_CKKSCmatrix.coeff = matrix.coeff
        pb_CKKSCmatrix.sum_place = matrix.sum_place
        return pb_CKKSCmatrix


def make_AzureBlobMessage(container_id: str, filepath: str, blob_name: str = ""):
    """
    Create AzureBlobMessage

    Parameters
    ----------
    container_id : str
    filepath : str
    blob_name : str, ""

    Returns
    -------
    ABMessage : AzureBlobMessage
    """
    ABMessage = common_model.azureBlob_pb2.AzureBlobMessage()
    ABMessage.container_id = container_id
    ABMessage.filepath = filepath
    ABMessage.blob_name = blob_name
    return ABMessage
