import grpc
import pickle
import random
import sys
import pandas as pd
import time

sys.path.append("../")

from sklearn import datasets
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from grpc_python import input_client_pb2_grpc
from grpc_python import model_client_pb2_grpc

from grpc_python.model import message_pb2
from grpc_python.model import lightGbmModel_pb2
from grpc_python.model import treeParameter_pb2
from grpc_python.model import data_pb2

import network_utils

def dispense_data():
    housing = datasets.fetch_california_housing()

    X = housing['data']
    y = housing['target']

    df_X = pd.DataFrame(X, columns=housing['feature_names'])
    scaler = StandardScaler()
    scaler.fit(df_X.values)
    df_X = scaler.transform(df_X.values)

    X_train, X_test, y_train, y_test = train_test_split(df_X, y,
                                                        test_size=0.2,
                                                        random_state=0)

    return X_train, X_test, y_train, y_test


if __name__ == "__main__":
    print("hello, world")

    api_input_client = network_utils.get_input_client_stub()

    api_model_client = network_utils.get_model_client_stub()

    with open("lgbm_regression50.pkl", "rb") as f:
      model = pickle.load(f)

    xs, _, ys, _ = dispense_data()

    for i in range(10):
      rand = random.randint(0, len(xs)-1)


      t1_cipher = time.time()
      pb_for_predict_lgbm_regression = data_pb2.PB_Data()
      pb_for_predict_lgbm_regression.data = pickle.dumps(xs[rand])
      res = api_input_client.predict_lgbm_regression(pb_for_predict_lgbm_regression)
      t2_cipher = time.time()

      t1_raw = time.time()
      raw_res = model.predict(xs[rand:rand+1])
      t2_raw = time.time()

      print("\n")
      print(f"cipher_prediction_time: {t2_cipher-t1_cipher}")
      print(f"raw_prediction_time   : {t2_raw-t1_raw}")
      print(f"cipher_res: {pickle.loads(res.data)}")
      print(f"raw_res   : {raw_res[0]}")
      print(f"y_res     : {ys[rand]}")

