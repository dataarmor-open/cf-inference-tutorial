import { get_random_matrix, get_random_scalar, get_random_vector, matrix_mul_vector, vector_add_scalar } from "../utils.js";

const attrs = 4
const ds = 20
const xs = get_random_matrix(attrs, ds)
const w = get_random_vector(attrs)
const b = get_random_scalar()
const tmp = matrix_mul_vector(xs, w)
const ys = vector_add_scalar(tmp, b)
// console.log(xs)
console.log(ys)