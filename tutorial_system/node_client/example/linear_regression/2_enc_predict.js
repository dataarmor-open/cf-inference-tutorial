import { ENC_LIR } from "../../enc_lir.js"
import { ENC_PREPROCESS } from "../../enc_preprocess.js"
import { get_random_dataset, get_random_matrix } from "../../utils.js"
import { attrs } from "./constants.js"

const proto_path = '/Users/kmihara/Documents/cf-inference-tutorial/tutorial_system/proto/input_client.proto'
const input_client_host = "localhost"
const input_client_port = "5101"


const preprocess = new ENC_PREPROCESS(proto_path, input_client_host, input_client_port)
const lir = new ENC_LIR(proto_path, input_client_host, input_client_port, 0)


await preprocess.load_key_on_server("tutorial")
await preprocess.load_minmax_scaler_on_server("tutorial")

await lir.load_key_on_server("tutorial")
await lir.load_model_on_server("lir_model")

const x_test = get_random_matrix(attrs, 10)

await preprocess.transfer_x_for_predict(x_test)
await preprocess.minmax_scaler_transform_x_test()
await preprocess.send_lir_only_test()
const y_hat = await lir.predict_already_send()
console.log(y_hat)