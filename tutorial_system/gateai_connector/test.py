glo_1 = "define in global"


def print_test():
    print("=====I'm in print test=====")
    print(f"print_test 1:{glo_1}")
    print(f"print_test 2:{glo_2}")  # Is this print or not?


if __name__ == "__main__":
    print("=====I'm in main function=====")
    print(f"main 1:{glo_1}")
    glo_2 = "define in main"
    print(f"main 2:{glo_2}")
    print_test()
