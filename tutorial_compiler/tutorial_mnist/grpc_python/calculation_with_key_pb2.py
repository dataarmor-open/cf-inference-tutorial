# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: calculation_with_key.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from grpc_python.model import binModel_pb2 as model_dot_binModel__pb2
from grpc_python.model import noParam_pb2 as model_dot_noParam__pb2
from grpc_python.model import encServiceEncoded_pb2 as model_dot_encServiceEncoded__pb2
from grpc_python.model import tensor_pb2 as model_dot_tensor__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='calculation_with_key.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x1a\x63\x61lculation_with_key.proto\x1a\x14model/binModel.proto\x1a\x13model/noParam.proto\x1a\x1dmodel/encServiceEncoded.proto\x1a\x12model/tensor.proto2\xd1\x01\n\x12\x43\x61lculationWithKey\x12-\n\x14transfer_local_model\x12\t.BinModel\x1a\x08.NoParam\"\x00\x12\x37\n\x15transfer_enc_services\x12\x12.EncServiceEncoded\x1a\x08.NoParam\"\x00\x12\'\n\x11start_calculation\x12\x07.Tensor\x1a\x07.Tensor\"\x00\x12*\n\x13get_time_statistics\x12\x08.NoParam\x1a\x07.Tensor\"\x00\x62\x06proto3'
  ,
  dependencies=[model_dot_binModel__pb2.DESCRIPTOR,model_dot_noParam__pb2.DESCRIPTOR,model_dot_encServiceEncoded__pb2.DESCRIPTOR,model_dot_tensor__pb2.DESCRIPTOR,])



_sym_db.RegisterFileDescriptor(DESCRIPTOR)



_CALCULATIONWITHKEY = _descriptor.ServiceDescriptor(
  name='CalculationWithKey',
  full_name='CalculationWithKey',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=125,
  serialized_end=334,
  methods=[
  _descriptor.MethodDescriptor(
    name='transfer_local_model',
    full_name='CalculationWithKey.transfer_local_model',
    index=0,
    containing_service=None,
    input_type=model_dot_binModel__pb2._BINMODEL,
    output_type=model_dot_noParam__pb2._NOPARAM,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='transfer_enc_services',
    full_name='CalculationWithKey.transfer_enc_services',
    index=1,
    containing_service=None,
    input_type=model_dot_encServiceEncoded__pb2._ENCSERVICEENCODED,
    output_type=model_dot_noParam__pb2._NOPARAM,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='start_calculation',
    full_name='CalculationWithKey.start_calculation',
    index=2,
    containing_service=None,
    input_type=model_dot_tensor__pb2._TENSOR,
    output_type=model_dot_tensor__pb2._TENSOR,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='get_time_statistics',
    full_name='CalculationWithKey.get_time_statistics',
    index=3,
    containing_service=None,
    input_type=model_dot_noParam__pb2._NOPARAM,
    output_type=model_dot_tensor__pb2._TENSOR,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_CALCULATIONWITHKEY)

DESCRIPTOR.services_by_name['CalculationWithKey'] = _CALCULATIONWITHKEY

# @@protoc_insertion_point(module_scope)
