
# 最適化 サービスの使用方法

このドキュメントでは、GateAIサービス群の内の一つ、

- GA サービス
- KMEANS サービス

の使用方法を解説します。


本チュートリアルは、

- GAサービスを用いて暗号状態で遺伝アルゴリズムを実行したときの結果の表示
- GAサービスを用いて行った遺伝アルゴリズムにかかる時間の計測
- KMEANS サービスを用いて暗号状態でクラスタリングを実行したときの結果の表示
- KMEANS サービスを用いて暗号状態でクラスタリングを実行したときの時間の計測

について検証できるチュートリアルとなっています。

### 注意
GAサービスを用いて実行する遺伝アルゴリズムには、シュミレーターとなるAIモデルが必要です。
本チュートリアルでは、対象となるAIモデルはすでにGateAIもしくはScikitlearnにて学習済みであることが前提となっています。


## 遺伝アルゴリズムによる最適化、kmeans クラスタリングの検証

チュートリアル実行によって生成される結果については、
後続の **最適化、kmeansクラスタリングの結果ファイルの解説**

をご覧ください。


#### 遺伝アルゴリズム

```
python main.py
```
  
の実行により、  

- 遺伝アルゴリズムによる最適化（20世代まで計算(constants_client.ITERATION), ２変数の最大化)
- kmeans クラスタリングの実行(k=10(constants_client.K))

をが行われます。
実行すると、  


```
loop for lir_and_ga 11 / 20 done: time = 1.8595495223999023
(2, 16)
current_diff: [0.93118088 0.92445263]
```

上記のようなログが出力されます。  
それぞれ、  

- 各世代生成にかかった時間（秒）
- (目的変数の次元, 生成された次世代の数)
- 目標値(constants_client.GOAL)との差異（次世代の値の平均値との差異）

を表示しています。  
また、生成された各世代がどのように目的値に近づいていくか、その様子のプロットが  

```
result_ga/plot_{世代番号}.png
result_ga/plot_all_generation_{世代番号}.png
```

として保存されます。  

プログラムを確認するために、main.pyをご覧ください。

```
from ga import GA
from kmeans import KMEANS
```

GAは遺伝アルゴリズムを実行するのに必要なクラスです。
KMEANSはkmeansクラスタリングを実行するのに必要なクラスです。

main.pyの

```
def test_ga_after_train(stub, stub_noise)
```

をご覧ください。

```
    lir0 = LIR(stub=stub, model_index=0)
    lir1 = LIR(stub=stub, model_index=1)
    lirs = [lir0, lir1]
    lir0.set_config_for_inference_for_optimization()
    lir1.set_config_for_inference_for_optimization()
```

遺伝アルゴリズムには、
- 対象とするモデル（シミュレータとしての役割）
が必要です。今回は、
2変数を対象とした最適化を実行するため、

- lir0
- lir1

のクラスのインスタンスを用意する必要があります。
これらのインスタンスは lir0, lir1 サービス（コンテナ）
と紐ついており、それらのサービスをシミュレータとしてGAクラスが実行されます。

`lir.set_config_for_inference_for_optimization()`

を実行し、lirサービスのコンフィグを最適化用に設定しています。

```
    ga = GA(stub=stub, model_index=[0, 1], prediction_models=lirs, noise_stub=stub_noise)
    ga.keygen()
    ga.set_config()
    ga.apply()
```

GAクラスのコンストラクトを行っています。
GAサービスには

- stub: input_client のスタブ
- noise_stub: noise_service へのスタブ
- model_index: [0, 1] 使用するlirサービスのインデックス
- prediction_models: 使用するLIRクラスのリスト

が必要となっています。
それらが準備できたら、

- `ga.keygen()` を実行しGAサービス用の暗号鍵ペアの生成
- `ga.set_config()`を実行しコンフィグを設定
- `ga.apply()`を実行し遺伝アルゴリズムを実行

という流れになっています。
GAサービスの実行に必要な設定は、GAクラスのコンストラクトの時に設定可能であり、
設定可能なパラメータは下記をご覧ください。


### GA サービスの設定パラメータ

- stub: input_client へのスタブ
- model_index: 使用する推論モデルのインデックス　[0,1]であれば、lir0, lir1 を使用
- prediction_models: 使用する推論モデルのクラスインスタンスのリスト
- noise_stub: ノイズサービスへのスタブ、デバッグ用に使用
- ga_data_size: 遺伝アルゴリズムの最初の世代を何件作るか
- crossover_probability: 交叉発生確率
- mutation_indivisual_probability: 個体を変異させる確率
- mutation_feature_probability: 個体が変異する際、各特徴量が変異する確率
- attrs: データのカラム数（特徴量の数）
- crossover_config: どのカラムを交叉させたいか指定するリスト
- elite_percentage: エリート個体の選出割合
- number_of_goals: 目的変数の個数
- goal: 目的変数の値のリスト
- iteration: 第何世代まで最適化するか
- ga_result_folder_name: 結果を保存するフォルダ名
- is_choose_from_condition: 検証のために、各世代から目的変数が一定の場所にある個体を選出するかどうか。もしFalseであれば、目的変数に近い個体を自動的に選出。Trueであれば、条件に合ったものだけを選出
- conditions: is_choose_from_condition がTrueのときは、conditions に適合したものだけを選出










## kmeans クラスタリング

続けて、遺伝アルゴリズムの次世代生成が終了した時点で、  
kmeans クラスタリングが実行されます。  

kmeans の実行中には、  

```
kmeans loop: 1/5============
kmean_round: 5.174499
```

のようにログが出力されます。  
この数値は、kmeans の重心を更新する１イテレーション  
（これをconstants_client.KMEANS_ROUND 回繰り返します。）  
にかかった時間を表示しています。  

プログラムを確認するために、main.pyをご覧ください。

```
from kmeans import KMEANS
```

KMEANSはkmeansクラスタリングを実行するのに必要なクラスです。

main.pyの

```
def test_kmeans(stub, stub_noise, ga)
```

をご覧ください。

```
    chosen_data, chosen_ys = ga.extract_data_from_all_generations()

    kmeans = KMEANS(stub, len(chosen_data))
    kmeans.keygen()
    kmeans.set_config()
    kmeans.upload_datas(chosen_data)
    kmeans.upload_initial_centroids()
    kmeans.apply()

```

kmeansを実行する時に、
遺伝アルゴリズムの結果を利用していますので、ga（GAのインスタンス）
を `chosen_data, chosen_ys = ga.extract_data_from_all_generations()`を実行することで
取得しています。　

その後、

- KMEANS クラスのコンストラクト、
- 鍵生成、
- コンフィグの設定、
- データの送信、
- セントロイドの初期位置の決定
- クラスタリングの実行

を実行しています。


KMEANSサービスの実行に必要な設定は、KMEANSクラスのコンストラクトの時に設定可能であり、
設定可能なパラメータは下記をご覧ください。


### KMEANS サービスの設定パラメータ

```

- stub: GateAI input_client へのスタブ
- noise_stub: GateAI noise_service へのスタブ（デバッグ用に使用）
- k: kmeans クラスタリングのクラスター数
- attrs: クラスタリング対象データの特徴量数
- data_size: クラスタリング対象データのデータ数
- round: kmeans クラスタリングを実行する時の重心を更新するイテレーション回数
- kmeans_result_folder_name: クラスタリング結果のフォルダ名

```


以上でチュートリアルは終了となります。  
ここまで読んでいただき誠にありがとうございました。  

チュートリアルに使用した全てのDockerコンテナの削除は、
実行環境コンテナから

```
exit
```

で抜けた後、　　

```
sh down.sh
```

を行うことで実行できます。  



以下は、お使いのデータに対して学習を行う際に設定していただく、  
`constants_client.py`
についての詳細となります。  


## 最適化、kmeansクラスタリングの結果ファイルの解説

最適化、クラスタリングの結果として、

- ga_res
- ga_plot
- kmeans_res
- kmeans_plot

フォルダーが生成されます。
それぞれに生成されるファイル、画像の解説をします。

### `ga_res`

**all_generations_denoised.pkl**
全世代のデータが格納されているpickleファイル

**all_prediction_results_depermed.pkl**
全世代に対するLIRサービスによる推論結果が格納されているpickleファイル

**res_diffs.pkl**
各世代の、LIRサービスによる推論結果と目標値（constants_client.GOAL)の間の距離の平均値を格納しているpickleファイル


### `ga_plot`

**plot_{i}.png**
各世代における目的変数のプロット
ただし、y0, y1 には並び替えが施されていることに注意。つまり、データに対してy0, y1 の値は世代内で並び替えられている

**plot_all_generation_{i}.png**
各世代における目的変数のプロット
上のplot_{i}.png
とは違い、クライアント側で明示的に並び替えを解除しているため、本来の推論値を持ったデータがプロットされている


### `kmeans_res`

**cluster.pkl**
kmeans クラスタリングによる各データのクラスタリング所属情報

**ks.pkl**
kmeans クラスタリングによる各クラスターの重心情報

### `kmeans_plot`

**kmeans_chosen_data.png**
全世代の中から、特定の予測領域に限定したデータに対してLIRサービスを用いて推論を行った時の結果

**kmeans_ks_centroid.png**
各クラスターの重心に対してLIRサービスを用いて推論を行った時の結果

