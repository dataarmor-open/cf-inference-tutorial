import numpy as np
import pandas as pd
import pickle

import lightgbm as lgb

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler

def dispense_data():
    housing = datasets.fetch_california_housing()

    X = housing['data']
    y = housing['target']

    df_X = pd.DataFrame(X, columns=housing['feature_names'])
    scaler = StandardScaler()
    scaler.fit(df_X.values)
    df_X = scaler.transform(df_X.values)

    X_train, X_test, y_train, y_test = train_test_split(df_X, y,
                                                        test_size=0.2,
                                                        random_state=0)
    

    return X_train, X_test, y_train, y_test


def prepare_tree(model_path, n_estimators):

    X_train, X_test, y_train, y_test = dispense_data()


    lgb_train = lgb.Dataset(X_train, y_train,
                            free_raw_data=False)

    lgb_eval = lgb.Dataset(X_test, y_test, reference=lgb_train)

    # パラメータを設定
    params = {'task': 'train',                # 学習、トレーニング ⇔　予測predict
                'boosting_type': 'gbdt',        # 勾配ブースティング
                'objective': 'regression',      # 目的関数：回帰
                'metric': 'rmse',               # 分類モデルの性能を測る指標
                'n_estimators': n_estimators,
                'max_depth': 3,
                'learning_rate': 0.1 }          # 学習率（初期値0.1）

    # 学習
    evaluation_results = {}                              # 学習の経過を保存する箱
    model = lgb.train(params,                            # 上記で設定したパラメータ
                        lgb_train,                         # 使用するデータセット
                        num_boost_round=1000,              # 学習の回数
                        valid_names=['train', 'valid'],    # 学習経過で表示する名称
                        valid_sets=[lgb_train, lgb_eval],  # モデル検証のデータセット
                        evals_result=evaluation_results,   # 学習の経過を保存
                        early_stopping_rounds=20,          # アーリーストッピング
                        verbose_eval=0)                    # 学習の経過の非表示


    y_pred = model.predict(X_test, num_iteration=model.best_iteration)
    print(len(y_pred))
    print(y_pred)
    print(y_test)
    print(mean_squared_error(y_pred, y_test))





    pickle.dump(model, open(model_path, 'wb'))

if __name__ == "__main__":
  n_estimators_list = [1, 3, 5, 10, 50, 100]
  for n_estimators in n_estimators_list:
    prepare_tree(f"lgbm_regression{n_estimators}.pkl", n_estimators)
