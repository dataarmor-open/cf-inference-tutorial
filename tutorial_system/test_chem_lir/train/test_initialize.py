import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
from grpc_service import input_client_pb2_grpc

from grpc_service import common_model

import constants_client as c

def main():
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    stub.initialize_weight_with_zero_for_lir(common_model.message_pb2.PB_Message())


if __name__ == "__main__":
    print("hello, world")
    main()

    



