import sys
import numpy as np
import chem_data

sys.path.append("../../")
from gateai_connector.enc_lssvr import ENC_LSSVR
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_model_selection import gridsearchCV
from sklearn.model_selection import GridSearchCV, KFold, train_test_split

if __name__ == "__main__":
    preprocess = ENC_PREPROCESS()
    lssvr = ENC_LSSVR()
    preprocess.load_key_on_server("test")
    lssvr.load_key_on_server("test")
    for i in range(2):
        preprocess.reset()
        preprocess.load_input_x_on_server(f"test_chem")
        preprocess.load_input_y_on_server(f"test_chem_{i}")

        kf = KFold(n_splits=3, shuffle=True)
        params_cnt = 5
        params = {"regressor__C": np.logspace(-2, 2, params_cnt), "regressor__epsilon": np.logspace(-2, 2, params_cnt)}
        gridsearch = gridsearchCV(preprocess, params, cv=kf)
        print(gridsearch["best_score_"])
        print(gridsearch["best_C"])
        print(gridsearch["best_epsilon"])
