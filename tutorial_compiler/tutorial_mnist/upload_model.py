# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

if __name__ == "__main__":
  # set model index
  model_index = 2

  model = keras.models.load_model('cnn_mnist.h5')

  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()


  # call model_client to upload h5 file from modelfiles/h5/{model_name}.h5
  print("call load_and_compile_model")
  model_info = ModelBinaryKeras()
  model_info.config = pickle.dumps(model.get_config())
  model_info.weights = pickle.dumps(model.get_weights())
  model_info.type_info = 'sequential'
  model_info.intermediate_output = 'none'
  model_info.model_index = model_index

  api_model_client.compile_model_from_binary_keras(model_info)

