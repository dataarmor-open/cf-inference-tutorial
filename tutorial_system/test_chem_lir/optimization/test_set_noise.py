import sys
sys.path.append("../")

import grpc
import numpy as np

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc
from grpc_service import noise_model

import constants_client as c

def remove_permutation_from_distance(distance, permutation, data_size, k):
    assert len(distance.shape) == 1
    assert distance.shape[0] == data_size * k
    res = np.zeros_like(distance)
    for i in range(data_size * k):
        res[permutation[i]] = distance[i]
    return res

def set_initial_noise(stub_noise, data_size, attrs, model_index):
    pb_noise_config = noise_model.noise_config_pb2.PB_NoiseConfig()
    pb_noise_config.attrs = attrs
    pb_noise_config.size = data_size
    pb_noise_config.index = model_index
    stub_noise.set_noise_config(pb_noise_config)

    pb_perm_config = noise_model.permutation_config_pb2.PB_PermutationConfig()
    pb_perm_config.size = data_size
    pb_perm_config.index = model_index
    stub_noise.set_permutation_config(pb_perm_config)


if __name__ == "__main__":
    print("hello, world")
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_SERVICE_HOST}:{c.NOISE_SERVICE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)

    set_initial_noise(stub_noise, 1000, 50)

    res = stub_noise.get_noise_all(noise_model.noise_query_pb2.PB_NoiseQuery())
    print(res.data)




