import pickle
import grpc
import sys
sys.path.append("../")

from sklearn import datasets

from grpc_python import input_client_pb2_grpc
from grpc_python import model_client_pb2_grpc

from grpc_python.model import message_pb2
from grpc_python.model import randomForestModel_pb2
from grpc_python.model import treeParameter_pb2
from grpc_python.model import data_pb2

import network_utils


if __name__ == "__main__":
    print("hello, world")

    api_input_client = network_utils.get_input_client_stub()

    api_model_client = network_utils.get_model_client_stub()

    api_input_client.gen_key(message_pb2.PB_Message())
