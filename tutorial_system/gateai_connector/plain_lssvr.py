from statistics import mode
import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.utils import check_X_y, check_array
from sklearn.exceptions import NotFittedError
from scipy.sparse.linalg import lsmr
from sklearn.preprocessing import normalize
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


class LSSVR_GRAD(BaseEstimator, RegressorMixin):
    """Least Squares Support Vector Regression.
    Parameters
    ----------
    C : float, default=2.0
        Regularization parameter. The strength of the regularization is
        inversely proportional to C. Must be strictly positive.
    kernel : {'linear', 'rbf'}, default='linear'
        Specifies the kernel type to be used in the algorithm.
        It must be 'linear', 'rbf' or a callable.
    gamma : float, default = None
        Kernel coefficient for 'rbf'
    Attributes
    ----------
    support_: boolean np.array of shape (n_samples,), default = None
        Array for support vector selection.
    alpha_ : array-like
        Weight matrix
    bias_ : array-like
        Bias vector
    """

    def __init__(self, C=None, kernel="rbf", gamma="auto", epsilon=None):
        self.C = C
        self.kernel = kernel
        self.gamma = gamma
        self.epsilon = epsilon

        self.support_ = None
        self.support_vectors_ = None
        self.bias_ = None
        self.alpha_ = None

    def make_problem(self, K, y):
        omega = K.copy()
        for i in range(omega.shape[1]):
            omega[i][i] = omega[i][i] + 1 / self.C

        D = np.empty(np.array(omega.shape) + 1)
        D[1:, 1:] = omega
        D[0, 0] = 0
        D[0, 1:] = 1
        D[1:, 0] = 1

        shape = np.array(y.shape)
        shape[0] += 1

        t = np.empty(shape)
        t[0] = 0
        t[1:] = y
        # print("D=",D.shape)
        beta = np.empty(np.count_nonzero(self.support_ == True) + 1)
        beta[0:] = 1
        # print("D = ", D.shape)
        # print("beta = ", beta.shape)
        ata = np.dot(D.T, D)
        atone = np.dot(D.T, t)
        return ata, beta, atone

    def solve_problem(self, ata, beta, atone, target_rate):
        self.learning_rate = 2 / np.trace(ata)

        # print("lr_trace = ", self.learning_rate)
        # print("lr_l2 = ", 2/np.linalg.norm(ata, 2))
        self.learning_epoch = 100000
        # while 1:
        #     self.bias_ = np.array(beta[0])
        #     self.alpha_ = np.array(beta[1:])
        #     mse_first = mean_squared_error(self.y_for_training,self.predict(self.X_for_training))
        #     for i in range(10):
        #         p = np.dot(ata, beta)
        #         p = p - atone
        #         beta = beta - self.learning_rate * p
        #         self.bias_ = np.array(beta[0])
        #         self.alpha_ = np.array(beta[1:])
        #     mse_second = mean_squared_error(self.y_for_training,self.predict(self.X_for_training))
        #     if mse_first < mse_second:
        #         # print("{:.2f},->,{:.2f}:too large learning_rate. Multiply learning_rate by 0.9",mse_first,mse_second)
        #         eig = np.linalg.eigvals(ata)
        #         ans = np.max(eig) - np.min(eig)
        #         trace = np.trace(ata)
        #         l2 = np.linalg.norm(ata, 2)
        #         # print(ans,":",trace,":",l2)
        #         self.learning_rate = self.learning_rate * 0.8
        #     else:
        #         break
        _mse = 0
        for i in range(self.learning_epoch):
            p = np.dot(ata, beta)
            p = p - atone
            beta = beta - self.learning_rate * p
            self.bias_ = np.array(beta[0])
            self.alpha_ = np.array(beta[1:])
            if i % 100 == 0:
                mse = mean_squared_error(self.y_for_training, self.predict(self.X_for_training))
                if _mse != 0:
                    impl_mse = (np.abs(mse - _mse) / mse) * 100
                    if i == 100 and _mse < mse:
                        raise ValueError("learning_rate error")
                    if impl_mse < target_rate or _mse < mse:
                        # print("epoch : ",i)
                        break
                _mse = mse

    def solve_problem_decided_epoch(self, ata, beta, atone):
        self.learning_rate = 2 / np.trace(ata)
        self.learning_epoch = 500
        for i in range(self.learning_epoch):
            p = np.dot(ata, beta)
            p = p - atone
            beta = beta - self.learning_rate * p
        alpha = beta[1:]
        return beta[0], alpha

    def _fit(self, X, y, support, target_rate):
        """Fit the model according to the given training data.
        Parameters
        ----------
        X : {array-like, sparse matrix} of shape (n_samples, n_features)
            Training data
        y : array-like of shape (n_samples,) or (n_samples, n_targets)
            Target values.sv
        support : boolean np.array of shape (n_samples,), default = None
            Array for support vector selection.
        Returns
        -------
        self : object
            An instance of the estimator.
        """
        X, y = check_X_y(X, y, multi_output=True, dtype="float")  # check form
        if len(support) == 0:
            self.support_ = np.ones(X.shape[0], dtype=bool)
        else:
            self.support_ = check_array(support, ensure_2d=False, dtype="bool")
        self.support_vectors_ = X[self.support_, :]
        # print("support_vector = ", self.support_vectors_.shape)
        K = self.kernel_func(X, self.support_vectors_)
        # print("K = ", K.shape)
        ata, beta, atone = self.make_problem(K, y)
        self.solve_problem(ata, beta, atone, target_rate)
        return self

    def fit(self, X, y, support=[]):
        self.X_for_training = X
        self.y_for_training = y

        if self.gamma == "auto":
            self.gamma = 1 / (X.shape[1] * np.var(X))
        support = []

        if self.epsilon is 0:
            self._fit(X, y, support, 1)
        if self.epsilon is not 0:
            self._fit(X, y, support, 10)
            support = self.update_support_vector(X, y)
            self._fit(X, y, support, 1)
        return self

    def predict(self, X):
        """
        Predict using the estimator.
        Parameters
        ----------
        X : array-like or sparse matrix, shape (n_samples, n_features)
            Samples.
        Returns
        -------
        y : array-like of shape (n_samples,) or (n_samples, n_targets)
            Returns predicted values.
        """

        if not hasattr(self, "support_vectors_"):
            raise NotFittedError

        X = check_array(X, ensure_2d=False)
        K = self.kernel_func(X, self.support_vectors_)
        ANS = (K @ self.alpha_) + self.bias_
        return ANS

    def kernel_func(self, u, v):
        if self.kernel == "linear":
            ans = np.dot(u, v.T)  # matrix * matrix?
            return ans

        elif self.kernel == "rbf":
            return rbf_kernel(u, v, gamma=self.gamma)

        elif callable(self.kernel):
            if hasattr(self.kernel, "gamma"):
                return self.kernel(u, v, gamma=self.gamma)
            else:
                return self.kernel(u, v)
        else:
            # default to linear
            return np.dot(u, v.T)

    # def score(self, X, y):
    #     from scipy.stats import pearsonr
    #     p, _ = pearsonr(y, self.predict(X))
    #     return p ** 2
    def score(self, X, y):
        y_hat = self.predict(X)
        return mean_squared_error(y_hat, y)

    def norm_weights(self):
        A = self.alpha_.reshape(-1, 1) @ self.alpha_.reshape(-1, 1).T

        W = A @ self.K_[self.support_, :]
        return np.sqrt(np.sum(np.diag(W)))

    def update_support_vector(self, X_train, y_train):
        y_train_predict = self.predict(X_train)
        diff = np.abs(y_train_predict - y_train)
        support = np.ones(X_train.shape[0], dtype=bool)
        assert self.epsilon is not None
        for i in range(len(support)):
            if diff[i] < self.epsilon:
                support[i] = False
            else:
                support[i] = True
        if np.count_nonzero(support) == 0:
            raise ValueError("support is nothing. epsilon is too large")
        return support


def normalize(v, axis=-1, order=2):
    l2 = np.linalg.norm(v, ord=order, axis=axis, keepdims=True)
    l2[l2 == 0] = 1
    return v / l2
