import pandas as pd

df = pd.read_excel("dummy.xlsx", engine='openpyxl')
# print(df.head())
# print(df.columns)
print(f"len: {len(df)}")
df = df.dropna(how="any")
print(f"len: {len(df)}")
df.to_csv("tmp.csv", index=None)
