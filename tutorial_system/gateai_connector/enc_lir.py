import sys
import os
import grpc
from tabnanny import check

sys.path.append("../")
import constants_client as c
import time
import itertools
import shutil
from tqdm import tqdm
import random
import numpy as np
import json

# import optimization

from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

from grpc_service import common_model
from grpc_service import noise_model
from grpc_service import lir_model

from grpc_service import input_client_pb2_grpc

if c.IS_FIXING_RANDOM_SEED:
    random.seed(c.RANDOM_SEED)
    np.random.seed(c.RANDOM_SEED)


class ENC_LIR(object):
    def __init__(
        self,
        model_index,
        attrs=c.ATTRS,
        bs=c.BS,
        ss=c.SS,
        epoch=c.LEARNING_EPOCH,
        eta=c.ETA,
        lmb=c.LMB,
        update_option=c.UPDATE_OPTION,
        is_use_warm_up=c.IS_USE_WARM_UP,
        initial_eta=c.INITIAL_ETA,
        max_eta=c.MAX_ETA,
        min_eta=c.MIN_ETA,
        rise_num=c.RISE_NUM,
        down_num=c.DOWN_NUM,
        is_overflow_detection=c.IS_OVERFLOW_DETECTION,
        overflow_threshold=c.OVERFLOW_THRESHOLD,
        early_stop_threshold=c.EARLY_STOP_THRESHOLD,
    ):
        channel_input_client = grpc.insecure_channel(f"{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}", options=[])
        stub_input_client = input_client_pb2_grpc.InputClientStub(channel_input_client)
        self.stub = stub_input_client
        self.model_index = model_index
        self.attrs = attrs
        self.bs = bs
        self.ss = ss
        self.epoch = epoch
        self.eta = eta
        self.lmb = lmb
        self.update_option = update_option
        self.is_use_warm_up = is_use_warm_up
        self.initial_eta = initial_eta
        self.max_eta = max_eta
        self.min_eta = min_eta
        self.rise_num = rise_num
        self.down_num = down_num
        self.is_overflow_detection = is_overflow_detection
        self.overflow_threshold = overflow_threshold
        self.loss_list = []

        self.early_stop_flag = False
        self.early_stop_threshold = early_stop_threshold
        self.early_stop_count = 0

        self.mae_info = {}
        self.mse_info = {}
        self.r2_info = {}

        self.set_noise_index()
        self.set_config()

    def keygen(self):
        self.stub.gen_key_for_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))
        # self.keygen_for_training()
        # self.keygen_for_inference()

    def save_key_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        pb_message.model_index = self.model_index
        self.stub.lir_save_secret_key(pb_message)

    def load_key_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        pb_message.model_index = self.model_index
        self.stub.lir_load_secret_key(pb_message)

    # def keygen_level1(self):
    #     self.keygen_for_training_level1()
    #     self.keygen_for_inference()

    # def keygen_for_training(self):
    #     self.stub.gen_key_for_training_for_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))

    # def keygen_for_training_level1(self):
    #     self.stub.gen_key_for_training_for_lir_level1(common_model.message_pb2.PB_Message(model_index=self.model_index))

    def transfer_ces_from_preprocess_to_lir(self):
        self.stub.transfer_ces_for_preprocess_to_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))

    def keygen_for_inference(self):
        self.stub.gen_key_for_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))

    def get_models_info_by_name(self, model_name):
        self.mae_info = self.stub.get_mae_info_for_lir(
            common_model.message_pb2.PB_Message(model_index=self.model_index, message=model_name)
        )
        self.mse_info = self.stub.get_mse_info_for_lir(
            common_model.message_pb2.PB_Message(model_index=self.model_index, message=model_name)
        )
        self.r2_info = self.stub.get_r2_info_for_lir(
            common_model.message_pb2.PB_Message(model_index=self.model_index, message=model_name)
        )

    def get_models_info_all(self):
        self.mae_info = json.loads(
            self.stub.get_mae_info_all_for_lir(
                common_model.message_pb2.PB_Message(model_index=self.model_index)
            ).message
        )
        self.mse_info = json.loads(
            self.stub.get_mse_info_all_for_lir(
                common_model.message_pb2.PB_Message(model_index=self.model_index)
            ).message
        )
        self.r2_info = json.loads(
            self.stub.get_r2_info_all_for_lir(
                common_model.message_pb2.PB_Message(model_index=self.model_index)
            ).message
        )

    def transfer_model_to_inference_by_name(self, name):
        self.stub.transfer_model_to_inference_by_name_for_lir(
            common_model.message_pb2.PB_Message(model_index=self.model_index, message=name)
        )

    def save_model_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.lir_save_model(pb_message)
    
    def load_model_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.lir_load_model(pb_message)

    def set_config_for_training(self, lmb=None):

        pb_config = lir_model.linearRegressionTrainConfig_pb2.PB_LinearRegressionTrainConfig()

        pb_config.attrs = self.attrs
        pb_config.bs = self.bs
        pb_config.ss = self.ss
        pb_config.eta = self.eta

        if lmb is not None:
            pb_config.lmb = lmb
        else:
            assert self.lmb is not None
            pb_config.lmb = self.lmb
        pb_config.update_option = self.update_option
        # pb_config.is_aself.umulate_update = self.IS_Aself.UMURATE_UPDATE
        pb_config.model_index = self.model_index

        if self.is_use_warm_up:
            pb_config.eta = self.initial_eta

        self.stub.upload_config_for_training_for_lir(pb_config)

    def set_config_for_inference(self):
        pb_config = lir_model.linearRegressionConfig_pb2.PB_LinearRegressionConfig()
        pb_config.attrs = self.attrs
        pb_config.bs = self.bs
        pb_config.ss = self.ss
        pb_config.model_index = self.model_index

        self.stub.upload_config_for_inference_for_lir(pb_config)

    def set_config_for_inference_for_optimization(self):
        pb_config = lir_model.linearRegressionConfig_pb2.PB_LinearRegressionConfig()
        pb_config.attrs = self.attrs
        pb_config.bs = 1
        pb_config.ss = self.ss
        pb_config.model_index = self.model_index

        self.stub.upload_config_for_inference_for_lir(pb_config)

        self.stub.set_noise_index(noise_model.noise_query_pb2.PB_NoiseQuery(index=self.model_index))

    def set_config(self):
        self.set_config_for_training()
        self.set_config_for_inference()

    def reset_training(self):
        pb = common_model.message_pb2.PB_Message(model_index=self.model_index)
        self.stub.reset_training_for_lir(pb)

    def save_best_model_to_model_info_with_name_for_lir(self, model_name):
        pb = common_model.message_pb2.PB_Message(model_index=self.model_index, message=model_name)
        self.stub.reset_training_for_lir(pb)

    def reset_training_data(self):
        pb = common_model.message_pb2.PB_Message(model_index=self.model_index)
        self.stub.reset_training_data_for_lir(pb)

    def upload_existing_model(self, model):
        pb_w_b = common_model.doubleList_pb2.PB_DoubleList()
        w_b_np = np.zeros(self.attrs + 1)
        w_b_np[: self.attrs] = model.coef_
        w_b_np[self.attrs] = model.intercept_
        pb_w_b.data[:] = w_b_np
        pb_w_b.model_index = self.model_index

        self.stub.upload_weight_and_bias_for_lir(pb_w_b)

    def _unison_shuffled_copies(self, a, b):
        assert len(a) == len(b)
        p = np.random.permutation(len(a))
        return a[p], b[p]

    def predict_batch(self, xs):
        pb = common_model.doubleList_pb2.PB_DoubleList()
        xs_flatten = xs.reshape(-1)
        pb.model_index = self.model_index
        pb.data[:] = xs_flatten
        res = self.stub.predict_lir(pb).data

        return res

    def predict_one_repeat_stream(self, xs, is_preprocess_denoise, is_postprocess_perm):
        pb = common_model.doubleList_pb2.PB_DoubleList()
        xs_flatten = xs.reshape(-1)
        pb.model_index = self.model_index
        pb.data[:] = xs_flatten
        pb.is_preprocess_denoise = is_preprocess_denoise
        pb.is_postprocess_perm = is_postprocess_perm
        res = self.stub.predict_lir_one_repeat_stream(pb).data
        return res
    
    def predict_already_send(self):
        pb = common_model.message_pb2.PB_Message()
        pb.model_index = self.model_index
        res = self.stub.lir_predict_test_data(pb).data
        return res

    def upload_train_data(self, train_x, train_y):
        pb = common_model.doubleList_pb2.PB_DoubleList()
        size = len(train_x)
        pb.model_index = self.model_index
        train_np = np.zeros(size * self.attrs + size)
        train_np[: size * self.attrs] = train_x.reshape(-1)
        train_np[size * self.attrs :] = train_y.reshape(-1)
        pb.data[:] = train_np
        pb.size = size
        self.stub.upload_train_data(pb)

    def upload_test_data(self, test_x, test_y):
        pb = common_model.doubleList_pb2.PB_DoubleList()
        size = len(test_x)
        pb.model_index = self.model_index
        test_np = np.zeros(size * self.attrs + size)
        test_np[: size * self.attrs] = test_x.reshape(-1)
        test_np[size * self.attrs :] = test_y.reshape(-1)
        pb.data[:] = test_np
        pb.size = size
        self.stub.upload_test_data(pb)

    def set_noise_index(self):
        self.stub.set_noise_index(noise_model.noise_query_pb2.PB_NoiseQuery(index=self.model_index))

    # def fit(self):
    #     self.loss_list = []
    #     self.stub.initialize_weight_with_zero_for_lir(common_model.message_pb2.PB_Message(message="server", model_index=self.model_index))
    #     self.set_noise_index()

    #     # 学習のメインループ（LEARNING_EPOCHをエポックとする）
    #     t_start = time.time()
    #     for i in tqdm(range(self.epoch)):
    #         pb_send = common_model.message_pb2.PB_Message(message="server", model_index=self.model_index)
    #         res = self.stub.train_one_epoch(pb_send)

    #         #self._debug_get_w_and_b_then_evaluate(xs_test, ys_test)
    #         res_loss = self.stub.calculate_loss_for_test_data(common_model.message_pb2.PB_Message(message="server", model_index=self.model_index))

    #         avg_loss = np.average(res_loss.data)
    #         #avg_loss = self._get_loss(xs_test, ys_test)
    #         self.loss_list.append(avg_loss)
    #         self._save_best_model(avg_loss, "test_model")
    #         print("\ncurrent loss========================")
    #         print(self.loss_list)

    #         # Loss のみを返却するので、ロスの値を用いてオーバーフローであるかを検証する
    #         if self.is_overflow_detection:
    #             overflow_flag = self._evaluate_overflow(avg_loss)
    #             if overflow_flag:
    #                 raise

    #         self._handle_warmup(i)

    #         if self.early_stop_flag:
    #             print("\nearly stop called, done========================")
    #             mae = self.stub.calculate_mae_for_test_data(common_model.message_pb2.PB_Message())
    #             r2 = self.stub.calculate_r2_for_test_data(common_model.message_pb2.PB_Message())
    #             print(f"mae: {mae}")
    #             print(f"r2: {r2}")
    #             break

    #     print("\ntraining done========================")
    #     mae = self.stub.calculate_mae_for_test_data(common_model.message_pb2.PB_Message())
    #     r2 = self.stub.calculate_r2_for_test_data(common_model.message_pb2.PB_Message())
    #     print(f"mae: {mae}")
    #     print(f"r2: {r2}")

    def _calculate_metrics(self, save_name):
        mae = self.stub.calculate_mae_for_test_data(
            common_model.message_pb2.PB_Message(message=save_name, model_index=self.model_index)
        )
        r2 = self.stub.calculate_r2_for_test_data(
            common_model.message_pb2.PB_Message(message=save_name, model_index=self.model_index)
        )
        print(f"mae: {mae}")
        print(f"r2: {r2}")

    def fit_level1(self, save_model_name):
        self.loss_list = []
        self.stub.initialize_weight_with_zero_for_lir_level1(
            common_model.message_pb2.PB_Message(message="server", model_index=self.model_index)
        )

        # 学習のメインループ（LEARNING_EPOCHをエポックとする）
        t_start = time.time()
        for i in tqdm(range(self.epoch)):
            pb_send = common_model.message_pb2.PB_Message(model_index=self.model_index)
            res = self.stub.train_one_epoch_level1(pb_send)

            res_loss = self.stub.calculate_loss_for_test_data_level1(
                common_model.message_pb2.PB_Message(model_index=self.model_index, message=save_model_name)
            )

            self.loss_list.append(res_loss.data[0])
            self._save_best_model(res_loss.data[0], save_model_name)
            tqdm.write(f"loss: {self.loss_list[-1]}")

            self._calculate_metrics(save_model_name)

            # Loss のみを返却するので、ロスの値を用いてオーバーフローであるかを検証する
            if self.is_overflow_detection:
                overflow_flag = self._evaluate_overflow(res_loss.data[0])
                if overflow_flag:
                    raise Exception(f"overflow detected {res_loss.data[0]}")

            self._handle_warmup(i)

            if self.early_stop_flag:
                tqdm.write("\nearly stop called, done========================")
                self._calculate_metrics(save_model_name)
                break

        tqdm.write("\ntraining done========================")
        self._calculate_metrics(save_model_name)

    # def fit_level1_via_preprocess(self, save_model_name):
    #    self.loss_list = []
    #    self.stub.initialize_weight_with_zero_for_lir_level1(common_model.message_pb2.PB_Message(message="server", model_index=self.model_index))
    #    self.stub.set_noise_index(noise_model.noise_query_pb2.PB_NoiseQuery(index=self.model_index))
    #
    #    # 学習のメインループ（LEARNING_EPOCHをエポックとする）
    #    t_start = time.time()
    #    for i in tqdm(range(self.epoch)):
    #        pb_send = common_model.message_pb2.PB_Message(model_index=self.model_index)
    #        res = self.stub.train_one_epoch_level1(pb_send)

    #        res_loss = self.stub.calculate_loss_for_test_data_level1(common_model.message_pb2.PB_Message(model_index=self.model_index, message=save_model_name))

    #        self.loss_list.append(res_loss.data[0])
    #        self._save_best_model(res_loss.data[0], save_model_name)
    #        print(f"loss: {self.loss_list[-1]}")

    #        self._calculate_metrics(save_model_name)

    #        # Loss のみを返却するので、ロスの値を用いてオーバーフローであるかを検証する
    #        if self.is_overflow_detection:
    #            overflow_flag = self._evaluate_overflow(res_loss.data[0])
    #            if overflow_flag:
    #                raise

    #        self._handle_warmup(i)

    #        if self.early_stop_flag:
    #            print("\nearly stop called, done========================")
    #            self._calculate_metrics(save_model_name)
    #            break

    #    print("\ntraining done========================")
    #    self._calculate_metrics(save_model_name)

    def _get_pb_for_input(self, xs, ys):
        xs_flatten = xs.reshape(-1)
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[: self.attrs * self.bs] = xs_flatten[: self.bs * self.attrs]
        pb.data[self.attrs * self.bs :] = ys[: self.bs]
        pb.model_index = self.model_index
        return pb

    def _debug_get_w_and_b_then_evaluate(self, xs_test, ys_test):
        # 重みを取得する関数はデバッグ用として使用する
        w_and_b = self.stub.get_w_and_b_for_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))
        assert (len(w_and_b.data)) == self.attrs + 1

        res_w_b = np.array(w_and_b.data)
        res_w = res_w_b[: self.attrs]
        res_b = res_w_b[-1]

        # 平文で計算された重みと、GateAIで計算された重みを用いた予測の精度を評価
        y_hat_cipher = np.dot(xs_test, res_w) + res_b
        ys_test_squeeze = np.squeeze(ys_test)

        print(
            "LIR_GateAI: MAE = ",
            mean_absolute_error(ys_test_squeeze, y_hat_cipher),
            "R2 = ",
            r2_score(ys_test_squeeze, y_hat_cipher),
        )
        # print("LIR_SKL: MAE = ",mean_absolute_error(ys_test_squeeze, y_hat), "R2 = ", r2_score(ys_test_squeeze, y_hat))

    def _evaluate_overflow(self, avg_loss):
        if abs(avg_loss) > self.overflow_threshold:
            print(f"\nOVERFLOW DETECTED!!, ABORT.")
            return True
        else:
            return False

    def _get_loss(self, xs_test, ys_test):
        loss_inner_list = []
        loss_loop = len(xs_test) // self.bs
        for j in range(loss_loop):
            pb = common_model.doubleList_pb2.PB_DoubleList()
            xs_flatten = xs_test.reshape(-1)
            pb.data[: self.attrs * self.bs] = xs_flatten[j * self.bs * self.attrs : (j + 1) * self.bs * self.attrs]
            pb.data[self.attrs * self.bs :] = ys_test[j * self.bs : (j + 1) * self.bs]
            pb.model_index = self.model_index
            loss = self.stub.calculate_loss_for_test_data_batch(pb)
            # print(f"\nLoss================={j} th loop")
            loss_inner_list.append(loss.data)
            # print(loss_inner_list)
        avg_loss = np.average(loss_inner_list)

        # print(avg_loss)

        return avg_loss

    def _handle_warmup(self, current_epoch):
        # 学習率のウォームアップを実施
        if self.is_use_warm_up:
            pb_config = self._generate_pb_config()
            pb_config = self._calculate_lr_warmup(pb_config, current_epoch)
            self.stub.upload_config_for_training_for_lir(pb_config)

        else:
            pass

    def _save_best_model(self, avg_loss, save_file_name):
        # ロスがベストだった時、その時の重みをサーバサイドで保存
        if len(self.loss_list) > 1:
            if avg_loss < np.min(self.loss_list[:-1]):
                print(f"\nbest model will be saved in server... loss: {avg_loss}")
                self.stub.save_best_model_for_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))
                self.stub.save_current_best_model_to_file_for_lir(
                    common_model.message_pb2.PB_Message(message=save_file_name, model_index=self.model_index)
                )
                self.stub.transfer_current_best_model_to_inference_for_lir(
                    common_model.message_pb2.PB_Message(model_index=self.model_index)
                )
                self.stub.save_best_model_to_model_info_with_name_for_lir(
                    common_model.message_pb2.PB_Message(model_index=self.model_index, message=save_file_name)
                )
                self.early_stop_count = 0
            else:
                self.early_stop_count += 1
                if self.early_stop_count > self.early_stop_threshold:
                    self.early_stop_flag = True
        else:
            self.stub.save_best_model_for_lir(common_model.message_pb2.PB_Message(model_index=self.model_index))
            self.stub.save_current_best_model_to_file_for_lir(
                common_model.message_pb2.PB_Message(message="test_model_here", model_index=self.model_index)
            )
            self.stub.transfer_current_best_model_to_inference_for_lir(
                common_model.message_pb2.PB_Message(model_index=self.model_index)
            )
            self.stub.save_best_model_to_model_info_with_name_for_lir(
                common_model.message_pb2.PB_Message(model_index=self.model_index, message=save_file_name)
            )

    def _generate_pb_config(self):
        pb_config = lir_model.linearRegressionTrainConfig_pb2.PB_LinearRegressionTrainConfig()
        pb_config.attrs = self.attrs
        pb_config.bs = self.bs
        pb_config.ss = self.ss
        pb_config.lmb = self.lmb
        pb_config.update_option = self.update_option
        # pb_config.is_accumulate_update = c.IS_ACCUMURATE_UPDATE
        pb_config.model_index = self.model_index

        return pb_config

    def _calculate_lr_warmup(self, pb_config, i):
        if i < self.rise_num:
            current_eta = self.initial_eta + (self.max_eta - self.initial_eta) / (self.rise_num) * i
            pb_config.eta = current_eta
        elif self.rise_num <= i and i < (self.rise_num + self.down_num):
            current_eta = self.max_eta - (i - self.rise_num) * (self.max_eta - self.min_eta) / self.down_num
            pb_config.eta = current_eta
        elif i >= (self.rise_num + self.down_num):
            current_eta = self.min_eta
            pb_config.eta = current_eta
        else:
            raise Exception("warm up went something wrong")
        return pb_config
