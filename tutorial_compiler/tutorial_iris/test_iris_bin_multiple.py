# Copyright (c) EAGLYS Inc. All rights reserved.

import time
import grpc
import numpy as np
import pickle
import sys
sys.path.append("../")

import keras
from keras.utils import to_categorical, np_utils

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

from saas.input_client.service.input_client_pb2_grpc import InputClientStub
from saas.input_client.service.input_client_pb2 import NoParam, Tensor
from saas.model_client.service.model_client_pb2_grpc import ModelClientStub
from saas.model_client.service.model_client_pb2 import ModelInfo, ModelBinaryKeras

import network_constants
import network_utils


def dispense_data():
  iris = load_iris()

  data_x = iris.data
  data_y = iris.target
  data_y = np_utils.to_categorical(data_y)

  x_train, x_test, y_train, y_test = train_test_split(data_x, data_y, test_size=0.2)

  return x_train, x_test, y_train, y_test



def upload_model_to_gate_ai():
  # upload models with model_index to GateAI
  print("\n===============")
  print("upload models with model_index to GateAI ")
  for i in range(20):
    model = keras.models.load_model(f"modelfiles/h5/nn_iris_{i}.h5")

    # call model_client to upload h5 file from modelfiles/h5/{model_name}.h5
    print(f"call load_and_compile_model_from_local_h5 for model_index {i}")
    model_info = ModelBinaryKeras()
    model_info.config = pickle.dumps(model.get_config())
    model_info.weights = pickle.dumps(model.get_weights())
    model_info.type_info = 'sequential'
    model_info.intermediate_output = 'none'
    model_info.model_index = i
    api_model_client.compile_model_from_binary_keras(model_info)


def run_reference_with_gate_ai(x):
  test_datas = x[0:40]

  # call input_cleint to do inference
  print("\n===============")
  print("call predict")

  print("\n===============")
  print("cipher_result")
  results = []
  t1 = time.time()
  for i in range(20):
    print("\n============================")
    print(f"model loop: {i}")
    for j, test_data in enumerate(test_datas):
      print("\n")
      print(f"data loop: {j}")
      data_for_cf = test_data.tolist()
      input_data = Tensor()
      input_data.data = pickle.dumps(data_for_cf)
      input_data.model_index = i

      # call prediction
      #print(f"call predict for model_index {i}")
      result = api_input_client.predict(input_data)
      result = pickle.loads(result.data)
      print(result)
      results.append(np.argmax(result))

  t2 = time.time()


  #print("\n===============")
  #print("raw_result")
  #raw_results = []
  #for i in range(10):
  #  raw_model = keras.models.load_model(f"modelfiles/h5/nn_iris_{i}.h5")
  #  raw_result = raw_model.predict(test_data.reshape(1,4))
  #  print(raw_result)
  #  raw_results.append(np.argmax(raw_result))


  #print("\n===============")
  #print("summary: cipher_result")
  #print(np.average(results))
  #print("\n===============")
  #print("summary: raw_result")
  #print(np.average(raw_results))

  #print("\n===============")
  #print(f'time for cipher prediction >>> {t2-t1}')



if __name__ == "__main__":
  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  # call input_client to generate keys
  print("\n===============")
  print("call gen_key")
  api_input_client.gen_key(NoParam())

  upload_model_to_gate_ai()

  # prepare data for inference
  x, y, _, _  = dispense_data()
  run_reference_with_gate_ai(x)



