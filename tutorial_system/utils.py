import os
import base64
import shutil


from cf_core import CKKSEncryptionService


class CKKSEncryptionServiceWrapper:
    @staticmethod
    def serialize_encryption_service(
        enc_service, save_folder="enc_service_saved", include_galois_key=False
    ):
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        return CKKSEncryptionServiceWrapper.__serialize_encryption_service(
            enc_service, save_folder, include_galois_key
        )

    @staticmethod
    def serialize_encryption_service_only_public_key(
        enc_service, save_folder="enc_service_saved", include_galois_key=False
    ):
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        return CKKSEncryptionServiceWrapper.__serialize_encryption_service_only_public_key(
            enc_service, save_folder, include_galois_key
        )

    @staticmethod
    def __serialize_encryption_service(
        enc_service, save_folder="enc_service_saved", include_galois_key=False
    ):
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        enc_service.save_encryption_service(save_folder, include_galois_key)
        if include_galois_key:
            enc_service.save_galois_key(save_folder)

        result = {}
        for file_el in os.listdir(save_folder):
            if isinstance(file_el, bytes):
                file_el = file_el.decode()
            file = open(os.path.join(save_folder, file_el), "rb").read()

            if include_galois_key:
                if file_el == "glk.txt":
                    result["glk"] = base64.b64encode(file).decode("utf-8")
            if file_el == "parms.txt":
                result["parms"] = base64.b64encode(file).decode("utf-8")
            elif file_el == "pk.txt":
                result["pk"] = base64.b64encode(file).decode("utf-8")
            elif file_el == "rlk.txt":
                result["rlk"] = base64.b64encode(file).decode("utf-8")
            elif file_el == "sk.txt":
                result["sk"] = base64.b64encode(file).decode("utf-8")
            else:
                # raise Exception(f'some unkonown file {file_el} in enc_service_saved')
                pass

        return result

    @staticmethod
    def __serialize_encryption_service_only_public_key(
        enc_service, save_folder="enc_service_saved", include_galois_key=False
    ):
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        enc_service.save_parameters(save_folder)
        enc_service.save_public_key(save_folder)

        if include_galois_key:
            enc_service.save_galois_key(save_folder)

        result = {}
        for file_el in os.listdir(save_folder):
            if isinstance(file_el, bytes):
                file_el = file_el.decode()
            file = open(os.path.join(save_folder, file_el), "rb").read()

            if include_galois_key:
                if file_el == "glk.txt":
                    result["glk"] = base64.b64encode(file).decode("utf-8")
            if file_el == "parms.txt":
                result["parms"] = base64.b64encode(file).decode("utf-8")
            elif file_el == "pk.txt":
                result["pk"] = base64.b64encode(file).decode("utf-8")
            elif file_el == "rlk.txt":
                result["rlk"] = base64.b64encode(file).decode("utf-8")
            else:
                # raise Exception(f'some unkonown file {file_el} in enc_service_saved')
                pass

        return result

    @staticmethod
    def deserialize_encryption_service(
        serialized_obj, poly_modulus_degree, scale=2.0 ** 40, save_folder="enc_service_saved", include_galois_key=False
    ):
        if os.path.exists(save_folder):
            shutil.rmtree(save_folder)
        os.makedirs(save_folder)

        for key, value in serialized_obj.items():
            if key == "glk" and include_galois_key is False:
                continue
            if key not in ["glk", "pk", "sk", "parms", "rlk"]:
                raise ValueError("serialized_obj has invalid key: " + str(key))
            file_path = key + ".txt"
            dec_file = base64.b64decode(value)
            outfile = open(os.path.join(save_folder, file_path), "wb")
            outfile.write(dec_file)
            outfile.close()

        tmp_service = CKKSEncryptionService()
        tmp_service.load_encryption_service(
            scale=scale, poly_modulus_degree=poly_modulus_degree, folder_path=save_folder, is_include_galois_key=include_galois_key
        )

        return tmp_service

    @staticmethod
    def deserialize_encryption_service_only_public_key(
        serialized_obj, poly_modulus_degree, scale=2.0 ** 40, save_folder="enc_service_saved", include_galois_key=False
    ):
        if os.path.exists(save_folder):
            shutil.rmtree(save_folder)
        os.makedirs(save_folder)

        for key, value in serialized_obj.items():
            if key == "glk" and include_galois_key is False:
                continue
            if key not in ["glk", "pk", "sk", "parms", "rlk"]:
                raise ValueError("serialized_obj has invalid key: " + str(key))
            file_path = key + ".txt"
            dec_file = base64.b64decode(value)
            outfile = open(os.path.join(save_folder, file_path), "wb")
            outfile.write(dec_file)
            outfile.close()

        tmp_service = CKKSEncryptionService()
        tmp_service.load_encryption_service_only_public_key(
            scale=scale, poly_modulus_degree=poly_modulus_degree, folder_path=save_folder, is_include_galois_key=include_galois_key
        )

        return tmp_service

