# GateAIシステムの概要

## はじめに

GateAI システムには、以下のサービスが使用可能です。

- `input_client`
- `lir`
- `svr`
- `ga`
- `kmeans`
- `noise`
- `preprocess`


## 各サービスの概要
各サービスの役割について、簡単に概要を説明します。

### input_client
input_client は、

- `クライアントからのAPIリクエストの受け口としての役割`
- `鍵生成、鍵配布の役割`
- `計算プロセスのうち、一時復号が必要な部分の計算を担う役割`

を持っています。

格子暗号を用いた秘密計算に必要な鍵ペアを生成し、
公開鍵を各サービスに配布します。


### lir
lirサービス は、

- `線形回帰モデルの学習の役割`
- `線形回帰モデルの推論の役割`

を持っています。



### svr
svrサービス は、

- `サポートベクトル回帰モデルの学習の役割`
- `サポートベクトル回帰モデルの推論の役割`

を持っています。


### ga
gaサービス は、

- `遺伝アルゴリズムを用いた、入力最適化の役割`

を持っています。

### kmeans
kmeansサービス は、

- `kmeans クラスタリングを用いた、入力データのグループ化の役割`

を持っています。


### noise
noiseサービス は、

- `各サービスから出力された結果にノイズ等を加え、モデル情報の流出を防ぐ役割`

を持っています。


### preprocess
preprocess サービスは、


- `複数（もしくは単体）パーティが持ち寄ったデータに対して、標準化などの前処理を行う役割`

を持っています。


## GateAI サービスの立ち上げ方

GateAIサービスはDockerイメージの形で配布され、docker-compose を使ってデプロイすることができます。

gateai_system Docker イメージを取得後、以下を実行します。   

イメージ取得の際は、
- `Docker レジストリへのアクセスに必要なTLS鍵`
- `レジストリへのログインに必要なユーザネーム、パスワード`

をEAGLYSから受け取り、Docker pull にて取得してください。


GateAIのDockerイメージ本体、  
`gateai_system:latest`  

及び 検証環境のDockerイメージ  
`gateai_client:latest`   


が取得されていることをご確認ください。 


## チュートリアルレポジトリの取得及びコンテナの立ち上げ

```
git clone https://gitlab.com/dataarmor-open/cf-inference-tutorial.git
cd cf-inference-tutorial/tutorial_system
sh up.sh
```

以上の実行で、チュートリアルを実行するために、

- `input_client`
- `lir`
- `svr`
- `ga`
- `kmeans`
- `noise`
- `preprocess`

の全てのサービスが立ち上がります。

加えて、検証時にクライアント（データを所有し、input_clientへアクセスする人）
環境も必要となりますが、チュートリアルではそのclient 環境も生成されます。（docker-compose_client.yml)


`up.sh` の実行により、
検証環境であるclient Docker コンテナに入ります。  
チュートリアルは、client コンテナ内で実行しても良いですし、
ローカル環境にpython, grpc などの環境を用意することで実行可能です。
ローカル環境で実行する際は、

`cf-inference-tutorial` レポジトリ直下にて

`pip install -r requirements.txt`

を実行することで環境を準備できます。
また、python のバージョンについては`3.6`以上を推奨しています。

client Docker コンテナ内で、以下を実行します。  


## 各フォルダの概要

ここでは、各フォルダの役割、もしくは実行できるチュートリアルについて言葉で簡単に解説します。

### grpc_service

クライアントからinput_clientへの接続にはgrpc を使用しています。  
grpc_service には、proto ファイル（grpc で用いるサービスや、各APIの定義ファイル）をpython から参照可能にするためにビルドしたもの、が格納されています。

したがって、input_client のAPIを使用する時は
grpc_service をimport する必要があります。

input_client への接続方法については、各チュートリアルのソースコードに従ってください。

### proto

各サービスは先述の通り、grpc サービスとして展開されていますが、
protoフォルダの中には各サービスのgrpcの定義ファイルが格納されています。
どのようなAPIが存在しているか確認することが可能です。

### test_chem_lir

ダミーファイルをデータとし、
データに対して

- `線形回帰モデルの学習`
- `線形回帰モデル＋遺伝アルゴリズムを用いた入力の最適化`
- `遺伝アルゴリズムを施された各世代に対するkmeansクラスタリング`

を実行するためのチュートリアルフォルダです。


### test_preprocess

ランダムデータをpythonで生成し、それらのデータに対して

- `暗号状態での標準化`

を実行するためのチュートリアルフォルダです。


### test_random_svr

ダミーファイルをデータとし、
データに対して

- `svr モデルの学習`
- `svr モデルを使用した推論`

を実行します。




