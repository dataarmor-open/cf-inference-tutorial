from sklearn.datasets import make_regression
import grpc
import numpy as np
import sys

sys.path.append("../../")

from sklearn.model_selection import train_test_split
from grpc_service import input_client_pb2_grpc
from grpc_service import common_model
from sklearn.preprocessing import StandardScaler

from test_random_allprocess.utils.enc_lir import LIR


import constants_client as c

def _make_pb_from_doubleList(doubleList):
    if len(doubleList.shape) == 2:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = np.ravel(doubleList)
        pb.size = doubleList.shape[1]
    else:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = doubleList
        pb.size = 0
    return pb

def transfer_train_test_data(X_train, X_test, y_train, y_test, stub_input_client):
    # make preprocess key
    stub_input_client.gen_key_for_minmax(common_model.message_pb2.PB_Message())
    stub_input_client.clear_data_for_minmax(common_model.message_pb2.PB_Message())
    # transfer x_train
    pb_x = _make_pb_from_doubleList(X_train)
    stub_input_client.transfer_x_for_training(pb_x)
    # transfer x_test
    pb_x = _make_pb_from_doubleList(X_test)
    stub_input_client.transfer_x_for_testing(pb_x)
    # transfer y_train
    pb_y = _make_pb_from_doubleList(y_train)
    stub_input_client.transfer_y_for_training(pb_y)
    # transfer y_test
    pb_y = _make_pb_from_doubleList(y_test)
    stub_input_client.transfer_y_for_testing(pb_y)
    # transfer data to lir
    stub_input_client.transfer_data_to_lir_from_preprocess(common_model.message_pb2.PB_Message())

if __name__ == "__main__":
    xs, ys = make_regression(n_samples=c.LIR_DATA_SIZE,n_features=c.ATTRS,random_state=1)
    # xs, ys = make_regression(n_samples=10,n_features=10,random_state=1)

    print(xs.shape, ys.shape)

    # 標準化を行う
    X_train, X_test, y_train, y_test = train_test_split(xs, ys, test_size=0.1,random_state=1)
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub_input_client = input_client_pb2_grpc.InputClientStub(channel_input_client)
    lir0 = LIR(stub=stub_input_client, model_index=0)
    lir0.set_noise_index()
    lir0.set_config()
    lir0.reset_training()
    #lir1 = LIR(stub=stub, model_index=1)

    lir0.keygen_level1()
    lir0.transfer_ces_from_preprocess_to_lir()
    
    print("start transfer_train_data")
    transfer_train_test_data(X_train, X_test, y_train, y_test, stub_input_client)

    # convert lir shape
    stub_input_client.lir_convert_preprocess_data(common_model.message_pb2.PB_Message())

    model_name = "tmp_model1"
    lir0.fit_level1(model_name)
    quit()

