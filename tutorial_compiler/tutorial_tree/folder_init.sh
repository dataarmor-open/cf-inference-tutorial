rm -rf ./enc_service_saved &&\
rm -rf ./enc_service_deser_dump && \
mkdir -p ./enc_service_deser_dump/calc_with_key &&\
mkdir ./enc_service_deser_dump/calc_with_key/bfv_with_sk &&\
mkdir ./enc_service_deser_dump/calc_with_key/ckks_with_sk &&\
mkdir ./enc_service_deser_dump/calc_no_key &&\
mkdir ./enc_service_deser_dump/calc_no_key/bfv_no_sk &&\
mkdir ./enc_service_deser_dump/calc_no_key/ckks_no_sk &&\
mkdir ./enc_service_deser_dump/model_client &&\
mkdir ./enc_service_deser_dump/model_client/bfv_no_sk &&\
mkdir ./enc_service_deser_dump/model_client/ckks_no_sk &&\
mkdir ./enc_service_deser_dump/input_client &&\
mkdir ./enc_service_deser_dump/input_client/bfv_with_sk &&\
mkdir ./enc_service_deser_dump/input_client/ckks_with_sk &&\
mkdir ./enc_service_deser_dump/input_client/bfv_no_sk &&\
mkdir ./enc_service_deser_dump/input_client/ckks_no_sk &&\

rm -rf ./datas &&\
mkdir -p ./datas/input_client/keyfiles &&\
mkdir -p ./datas/input_client/keyfiles/bfv_with_sk &&\
mkdir -p ./datas/input_client/keyfiles/ckks_with_sk &&\
mkdir -p ./datas/input_client/keyfiles/bfv_no_sk &&\
mkdir -p ./datas/input_client/keyfiles/ckks_no_sk &&\
mkdir -p ./datas/input_client/modelfiles &&\
mkdir -p ./datas/model_client/keyfiles &&\
mkdir -p ./datas/model_client/keyfiles/bfv_no_sk &&\
mkdir -p ./datas/model_client/keyfiles/ckks_no_sk &&\
mkdir -p ./datas/model_client/modelfiles &&\
mkdir -p ./datas/calculation/keyfiles &&\
mkdir -p ./datas/calculation/keyfiles/bfv_no_sk &&\
mkdir -p ./datas/calculation/keyfiles/ckks_no_sk &&\
mkdir -p ./datas/calculation/modelfiles &&\
mkdir -p ./datas/calculation_with_key/keyfiles &&\
mkdir -p ./datas/calculation_with_key/keyfiles/bfv_with_sk &&\
mkdir -p ./datas/calculation_with_key/keyfiles/ckks_with_sk &&\
mkdir -p ./datas/calculation_with_key/modelfiles


mkdir -p ./modelfiles/h5 &&\
mkdir -p ./modelfiles/pkl &&\
mkdir -p ./modelfiles/dispensed_h5 &&\
mkdir -p ./modelfiles/intermediate_compare
