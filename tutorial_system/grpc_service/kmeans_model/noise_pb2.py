# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: kmeans_model/noise.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x18kmeans_model/noise.proto\"&\n\x08PB_Noise\x12\x0c\n\x04\x64\x61ta\x18\x01 \x03(\x01\x12\x0c\n\x04size\x18\x02 \x01(\x05\x62\x06proto3')



_PB_NOISE = DESCRIPTOR.message_types_by_name['PB_Noise']
PB_Noise = _reflection.GeneratedProtocolMessageType('PB_Noise', (_message.Message,), {
  'DESCRIPTOR' : _PB_NOISE,
  '__module__' : 'kmeans_model.noise_pb2'
  # @@protoc_insertion_point(class_scope:PB_Noise)
  })
_sym_db.RegisterMessage(PB_Noise)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PB_NOISE._serialized_start=28
  _PB_NOISE._serialized_end=66
# @@protoc_insertion_point(module_scope)
