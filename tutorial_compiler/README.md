
# GateAI Compiler の使用方法

## GateAI Compiler のサービス構成

GateAI Compiler は、  以下のサービス群から構成されています。

- `input_client`
- `model_client`
- `calculation`
- `calculation_with_key`


こちらに簡易的なシステム構成チュートリアルがありますので参照ください。   
以降のGateAI Compiler のコア機能の流れがビジュアライズされて解説されています。


https://eaglys.gitlab.io/gateai_open_easy_tutorial/


## GateAI Compiler のコア機能

これらサービス群を使用し、以下のような機能を提供します。

- `AIライブラリにて学習済みモデルを秘密計算可能にコンパイル(コンパイル)`
- `秘密計算モデルをホスティング(ホスティング)`
- `推論用データを暗号化し、推論を秘密計算実行 (推論)`


それぞれ詳細を解説します。

## コンパイル

### 機能概要

Keras, Scikitlearn, lightGBM　等のAIライブラリで学習されたモデルを入力とし、
モデル内部のデータ、計算グラフを読み取ることで  
秘密計算実行可能なモデルへとコンパイルする機能を持っています。

コンパイルされたモデルは、

- `暗号状態で実行される部分`
- `一時復号を行って信頼領域で実行される部分`

に自動的に切り分けられ、各サービスへと送信されます。


現在サポートされているKerasライブラリで使用されるレイヤ、
モデル一覧についてはお問い合わせください。　　
仮に現在サポートされていなくても、カスタマイズを加えることによって秘密計算モデルへとコンパイルできることもあります。　　


### 使用されるサービス

モデルのコンパイルは

- model_client

が実行します。  


先述のコンパイルされた後のモデルは、

- `calculation(暗号状態実行部分)`
- `calculation_with_key(一時復号実行部分)`

へと model_client から送信されます。


## ホスティング

### 機能概要

先述のコンパイルが行われ、model_client からcalculation, calculation_with_key  
へとモデルが配布されると、それらのモデルは自動的にホスティングされ、
ユーザからの入力を待つ待機状態となります。

### 使用されるサービス

- calculation
- calculation_with_key


## 推論


### 機能概要

ユーザがinput_client に向けてデータを送信すると、
input_client はデータを暗号化、その後モデル演算を実行する

- calcualtion_with_key
- calculation

へと送信します。

暗号化された入力データは、モデル推論が完了するまで２つのサービスをgrpc 通信を利用して行き来し、
モデル全体の演算が完了するとinput_client へと返却されます。
input_client は所持している秘密鍵を用いて結果を復号し、クライアントへと返却します。


### 使用されるサービス

- input_client
- calculation
- calculation_with_key



## GateAI Compiler のチュートリアル

GateAI Compiler を実際に使用する際に参照できるチュートリアルをご用意しております。
各チュートリアルはフォルダに格納されており、それぞれ

- tutorial_bank(ロジスティック回帰モデルのコンパイル)
- tutorial_iris(ニューラルネットモデルのコンパイル)
- tutorial_mnist(畳み込みニューラルネットモデルのコンパイル)
- tutorial_tree(ランダムフォレスト、LightGBMモデルのコンパイル)

の機能を試用することができます。  

詳細はそれぞれのフォルダをご覧ください。


## grpc_python について

このフォルダに格納されている `grpc_python` は、  
GateAI Compiler の各サービスが実装しているAPI（grpc)の定義ファイルをPython 用にビルドしたものです。
したがって、クライアントからGateAI Compiler のサービスに接続する際に、  
Python からこの`grpc_python`をimport する必要があります。  

詳細に関しては各チュートリアルフォルダ内のプログラムを参照ください。

