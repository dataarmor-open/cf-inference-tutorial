# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

def dispense_data():
  dataset = np.loadtxt("data_banknote_authentication.txt", delimiter=",")

  x = dataset[:, 0:4]
  y = dataset[:, 4]

  x = np.array(x)
  y = np.array(y)

  x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

  return x_train, x_test, y_train, y_test



if __name__ == "__main__":
  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  # call input_cleint to generate_keys
  print("call gen_key")
  api_input_client.gen_key(NoParam())
