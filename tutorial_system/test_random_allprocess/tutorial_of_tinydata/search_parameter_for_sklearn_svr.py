import sys

import numpy as np
import pickle
import time

from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.utils import check_X_y, check_array
from sklearn.exceptions import NotFittedError
from scipy.sparse.linalg import lsmr
from sklearn.preprocessing import normalize
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV, KFold, train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_boston
from sklearn.pipeline import Pipeline
from sklearn.datasets import make_regression


def train_raw():
    xs, ys = make_regression(n_samples=100,n_features=10,random_state=1)
    print(xs.shape, ys.shape)
    
    # 前処理に標準化を行うパイプラインを作成
    pipe = Pipeline([
        ('scaler', StandardScaler()),
        ('regressor', SVR()),
    ])
    
    # KFoldの設定
    kf = KFold(n_splits=10, shuffle=True)
    
    # SVRで、パラメータC, epsilonに対して、グリッドサーチを行う。
    params_cnt = 5
    params = {"regressor__C":np.logspace(-2,2,params_cnt), "regressor__epsilon":np.logspace(-2,2,params_cnt)}
    gridsearch = GridSearchCV(
        pipe, params, cv=kf, scoring="r2", return_train_score=True, verbose=1
    )
    gridsearch.fit(xs, ys)

    # gridsearchの結果をテストする。
    X_train, X_test, y_train, y_test = train_test_split(xs, ys, test_size=0.1, random_state=1)
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
        
    reg = SVR(
        C=gridsearch.best_params_["regressor__C"], 
        epsilon=gridsearch.best_params_["regressor__epsilon"]
    )
    reg.fit(X_train, y_train)
    y_hat = reg.predict(X_test)
    
    print("svr training by skl")
    print(f'chosen C: {gridsearch.best_params_["regressor__C"]}')
    print(f'chosen epsilon: {gridsearch.best_params_["regressor__epsilon"]}')
    print(f'model best_score: {gridsearch.best_score_}')
    

    print("sklearn SVR: = ",mean_absolute_error(y_test, y_hat), "R2 = ", r2_score(y_test, y_hat))
    coef=np.polyfit(y_test, y_hat, 1)
    print('sklearn SVR:y_true = ' + str(coef[0]) + ' + ' + str(coef[1]) + ' * y_predict')

if __name__ == "__main__":
    print("===train_raw ===")
    train_raw()