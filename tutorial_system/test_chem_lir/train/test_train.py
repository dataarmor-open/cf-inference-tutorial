import sys
sys.path.append("../../")
sys.path.append("../")

import grpc
import numpy as np
import pickle
import time
from tqdm import tqdm

from grpc_service import input_client_pb2_grpc

from grpc_service import common_model
from grpc_service import lir_model

import constants_client as c
import data 

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

def train_main(index, stub):
    # LIRサービスを使って学習を実行するメイン関数

    # index は２つ（複数）存在するサービスのうち、どちらを学習させるかを指定するインデックス
    assert index == 0 or index == 1

    # 重みの初期化を実行
    stub.initialize_weight_with_zero_for_lir(common_model.message_pb2.PB_Message())

    # データをロードする
    # 今回は正解となる回帰結果がys1, ys2と２つ存在する
    xs, ys1, ys2 = data.get_data()
    if index == 0:
        ys = ys1
    elif index == 1:
        ys = ys2


    # LIRサービスの学習の挙動をチェックするため、Scikitlearnで学習されたモデルの重み等をロードする
    with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/model{index}.pkl", 'rb') as f:
        raw_model = pickle.load(f)
    
    w = raw_model.coef_
    b = raw_model.intercept_


    # 正解データと、推論結果の平均二乗誤差を格納する　loss_label_list
    # 学習中の重みと、Scitkitlearnで学習された重みの二乗誤差を格納する loss_w_b_list
    loss_label_list = []
    loss_w_b_list = []


    # 学習のメインループ（LEARNING_EPOCHをエポックとする）
    t_start = time.time()
    for i in tqdm(range(c.LEARNING_EPOCH)):

        # データのシャッフル
        xs, ys = unison_shuffled_copies(xs, ys)
        xs_flatten = xs.reshape(-1)
        assert len(xs_flatten) == c.ATTRS * c.LIR_DATA_SIZE
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:c.ATTRS*c.BS] = xs_flatten[:c.BS * c.ATTRS]
        pb.data[c.ATTRS*c.BS:] = ys[:c.BS]
        pb.model_index = index
        
        # 1バッチの学習を実行
        res = stub.train_one_batch_for_lir(pb)
        assert len(res.data) == c.ATTRS + 1
        res_w_b = np.array(res.data)
        raw_w_b = np.zeros(c.ATTRS+1)
        raw_w_b[:c.ATTRS] = w
        raw_w_b[-1] = b

        # ロスなどを計算
        diff_w_b = res_w_b - raw_w_b
        mse = np.sum(diff_w_b * diff_w_b)


        if c.IS_USE_WARM_UP:
            pb_config = lir_model.linearRegressionTrainConfig_pb2.PB_LinearRegressionTrainConfig()
            pb_config.attrs = c.ATTRS
            pb_config.bs = c.BS
            pb_config.ss = c.SS
            pb_config.lmb = c.LMB
            pb_config.update_option = c.UPDATE_OPTION
            pb_config.is_accumulate_update = c.IS_ACCUMURATE_UPDATE
            pb_config.model_index = index

            if i < c.RISE_NUM:
                current_eta = c.INITIAL_ETA + (c.MAX_ETA - c.INITIAL_ETA)/(c.RISE_NUM - i) 
                pb_config.eta = current_eta
            elif c.RISE_NUM <= i and i < (c.RISE_NUM + c.DOWN_NUM):
                current_eta = c.MAX_ETA - (i - c.RISE_NUM) * (c.MAX_ETA - c.MIN_ETA)/c.DOWN_NUM
                pb_config.eta = current_eta
            elif i >= (c.RISE_NUM + c.DOWN_NUM):
                current_eta = c.MIN_ETA
                pb_config.eta = current_eta
            else:
                raise Exception("warm up went something wrong")
            stub.upload_config_for_training_for_lir(pb_config)
        
        else:
            current_eta = c.ETA

        # １バッチごとに重みを更新するのではなく、複数バッチ（constants_client.ACCUMURATE_UPDATE_FREQUENCY)ごとに更新したい場合
        if c.IS_ACCUMURATE_UPDATE:
            if i % c.ACCUMURATE_UPDATE_FREQUENCY == 0:
                # 更新したいタイミングで stub.update_with_accumulated_parameters_for_lir を呼ぶ
                res = stub.update_with_accumulated_parameters_for_lir(common_model.message_pb2.PB_Message())

        # print("\n\n")
        # print(f"res_w : {np.round(res.data[:c.ATTRS], 4)}")
        # print(f"true_w: {np.round(w, 4)}")
        # print(f"res_b : {np.round(res.data[-1], 4)}")
        # print(f"true_b: {np.round(b, 4)}")
        current_w = np.array(res.data[:c.ATTRS])
        current_b = np.array(res.data[-1])

        if c.IS_OVERFLOW_DETECTION:
            if any(abs(current_w) > c.OVERFLOW_THRESHOLD) or current_b > c.OVERFLOW_THRESHOLD:
                print(f"\nOVERFLOW DETECTED!!, ABORT.")
                print(f"w: {current_w}")
                print(f"b: {current_b}")
                return False

        tmp = np.dot(xs, current_w) + current_b

        diff = tmp-ys
        diff = np.sum(diff * diff)
        print(f"time: {time.time() - t_start}")
        print(f"loss: {diff}")
        print(f"w_b mse: {mse}")
        print(f"current_eta: {current_eta}")
        print("")

        loss_label_list.append(diff)
        loss_w_b_list.append(mse)


        # 現在の重み、ロスなどを保存
        with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/w_b{index}.pkl", "wb") as f:
            pickle.dump(list(res.data), f)

        with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/loss_label{index}.pkl", "wb") as f:
            pickle.dump(loss_label_list, f)

        with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/loss_w_b{index}.pkl", "wb") as f:
            pickle.dump(loss_w_b_list, f)
    return True
    

def main():
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    executed_without_overflow_list = []
    for i in range(2):
        res = train_main(i, stub)
        executed_without_overflow_list.append(res)
    
    return executed_without_overflow_list



if __name__ == "__main__":
    print("hello, world")

    main()




