import sys
import os
from tabnanny import check
sys.path.append("../")
import constants_client as c
import time
import itertools
import shutil
from tqdm import tqdm
import random
import numpy as np
import pickle
import matplotlib.pyplot as plt

import optimization

from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

from grpc_service import common_model
from grpc_service import noise_model
from grpc_service import lir_model
from grpc_service import ga_model

from lir import LIR

if c.IS_FIXING_RANDOM_SEED:
    random.seed(c.RANDOM_SEED)
    np.random.seed(c.RANDOM_SEED)


class GA(object):
    def __init__(self, stub, model_index, prediction_models, noise_stub=None, ga_data_size=c.GA_DATA_SIZE, crossover_probability=c.CROSSOVER_PROBABILITY,
        mutation_indivisual_probability=c.MUTATION_INDIVISUAL_PROBABILITY, mutation_feature_probability=c.MUTATION_FEATURE_PROBABILITY,
        attrs=c.ATTRS, crossover_config=c.CROSSOVER_CONFIG, elite_percentage=c.ELITE_PERCENTAGE, number_of_goals=c.NUMBER_OF_GOALS,
        goal=c.GOAL, iteration=c.ITERATION, ga_result_folder_name=c.GA_RESULT_FOLDER_NAME, is_choose_from_condition=c.IS_CHOOSE_FROM_CONDITION,
        conditions=c.CHOOSE_CONDITIONS
    ):
        assert isinstance(prediction_models, list)
        assert isinstance(model_index, list)
        
        self.stub = stub
        self.noise_stub = noise_stub
        self.model_index = model_index
        self.prediction_models = prediction_models
        self.ga_data_size = ga_data_size
        self.crossover_probability = crossover_probability
        self.mutation_indivisual_probability = mutation_indivisual_probability
        self.mutation_feature_probability = mutation_feature_probability
        self.attrs = attrs
        self.crossover_config = crossover_config
        self.elite_percentage = elite_percentage
        self.number_of_goals = number_of_goals
        self.goal = goal
        self.iteration = iteration
        self.ga_result_folder_name = ga_result_folder_name
        self.is_choose_from_condition = is_choose_from_condition
        self.conditions = conditions
    
    def keygen(self):
        self.stub.gen_key_for_ga(common_model.message_pb2.PB_Message())
    
    def set_config(self, size=None):
        pb_config = ga_model.gaConfig_pb2.PB_GAConfig()
        if size is not None:
            pb_config.size = size
        else:
            pb_config.size = self.ga_data_size

        pb_config.crossover_probability = self.crossover_probability
        pb_config.mutation_indivisual_probability = self.mutation_indivisual_probability
        pb_config.mutation_feature_probability = self.mutation_feature_probability
        pb_config.attrs = self.attrs
        pb_config.crossover_config[:] = self.crossover_config
        pb_config.elite_percentage = self.elite_percentage
        pb_config.number_of_goals = self.number_of_goals

        self.stub.upload_config_for_ga(pb_config)

        pb_feature_config_list = ga_model.gaFeatureConfigList_pb2.PB_GAFeatureConfigList()
        for i in range(pb_config.attrs):
            pb_feature_config = ga_model.gaFeatureConfig_pb2.PB_GAFeatureConfig()
            data = pb_feature_config_list.data.add()
            data.feature_min = 0
            data.feature_max = 1
        
        self.stub.upload_feature_config_for_ga(pb_feature_config_list)

        self.stub.initialize_mask_for_ga(common_model.message_pb2.PB_Message())

    
    def _send_parents_to_ga_service(self, xs, is_need_denoise):
        ## GAサービスへ、親データを送信する(input_client内で暗号化され、GAサービスへと送信される)
        ## xs.shape == (送信するデータの数, 属性数)
        print(xs.shape)
        assert len(xs.shape) == 2, f"{len(xs.shape)} != 2"
        assert xs.shape[1] == self.attrs
        pb_parents = common_model.doubleList_pb2.PB_DoubleList() 
        pb_parents.data[:] = xs.reshape(-1)
        pb_parents.size = xs.shape[0]
        ## GAサービスに送信する親データは、第一世代のみノイズを含んでおらず、
        ## 第二世代から（つまりGAサービスからの結果）はノイズを含んでいるので、
        ## 次世代を作るときにノイズ除去が必要かどうか明記する
        pb_parents.is_preprocess_denoise = is_need_denoise
        self.stub.upload_encrypted_parent_for_ga_stream(pb_parents)


    def _request_next_generation(self):
        ## GAサービスに対し、エリートのピックアップ、次世代データの生成を要求する
        pb = common_model.message_pb2.PB_Message()
        ## GAサービスに対し、次世代データにノイズを加えることを明示する
        pb.is_postprocess_noise = True
        next_gens = self.stub.query_for_ga_stream(pb)
        next_gens_np = np.array(next_gens.data)
        assert next_gens_np.shape[0] % self.attrs == 0
        next_gens_np = next_gens_np.reshape(-1, self.attrs)

        return next_gens_np


    def _send_diff_to_ga_service(self, diff_goals):
        ## 目標値との距離をflattenし、GAサービスへ送信する。GAサービスはこの情報をもとに、暗号化された親データからエリートをピックアップする。
        diff_goals = np.array(diff_goals)
        diff_goals_flatten = diff_goals.reshape(-1)
        assert diff_goals_flatten.shape[0] == self.number_of_goals * diff_goals.shape[1]
        pb_evaluation = common_model.doubleList_pb2.PB_DoubleList()
        pb_evaluation.data[:] = diff_goals_flatten
        pb_evaluation.size = diff_goals.shape[1]
        ## クライアントが所持するLIRサービスからの結果は並び替えが施されているので、GAサービスは逆並び替えを行う必要があることを明示する
        pb_evaluation.is_preprocess_deperm = True
        ## GAサービスへのアップロード
        self.stub.upload_evaluation_for_ga(pb_evaluation)
    
    def _predict_and_apply_ga(self, xs, is_denoise_needed_for_parents):
        res_list = []
        for el in self.prediction_models:
            res = el.predict_one_repeat_stream(xs, is_denoise_needed_for_parents, True)
            res_list.append(res)
        # クライアントは線形回帰で推定された物性値と、目標値の差を計算する
        diffs = []
        for i, el in enumerate(self.model_index):
            diff = np.array(res_list[i]) - self.goal[i]
            diffs.append(diff)
        current_diff = np.average(np.abs(diffs), axis=1)
        print(f"current_diff: {current_diff}")


        # 差をGAサービスへと送信する
        self._send_diff_to_ga_service(diff_goals=diffs)
        # 遺伝アルゴリズムの対象となる親（現在の世代）をGAサービスへと送信する
        self._send_parents_to_ga_service(xs, is_denoise_needed_for_parents)
        ## 目標値との距離をflattenし、GAサービスへ送信する。GAサービスはこの情報をもとに、暗号化された親データからエリートをピックアップする。
        next_gens = self._request_next_generation()
        res_list = np.array(res_list)

        return next_gens, res_list
    
    def _debug_remove_noise_from_generation(self, next_gens):
        ##　GAにおけるノイズを取得し、ノイズを直接除去する
        assert self.noise_stub is not None
        noise = np.array(self.noise_stub.get_noise_all(noise_model.noise_query_pb2.PB_NoiseQuery(index=0)).data)
        assert noise.shape[0] == next_gens.shape[0] * next_gens.shape[1], f"{noise.shape[0]} != {next_gens.shape[0] * next_gens.shape[1]}, shape: {noise.shape}, {next_gens.shape}"
        noise = noise.reshape(next_gens.shape[0], next_gens.shape[1])
        assert noise.shape[0] == next_gens.shape[0]
        assert noise.shape[1] == next_gens.shape[1]
        next_gens_denoised = next_gens - noise

        return next_gens_denoised


    def _remove_permutation_from_evaluation(self, xs, permutation):
        assert len(xs) == len(permutation), f"{len(xs)} != {len(permutation)}"

        res = np.zeros(len(xs))
        for i in range(len(xs)):
            res[permutation[i]] = xs[i]

        return res
    
    def _debug_remove_permutation_from_prediction(self, res):
        print(np.array(res).shape)
        assert res.shape[0] == len(self.model_index)
        res_deperm_total = np.zeros_like(res)
        for i, model_index_el in enumerate(self.model_index):
            perm = self.noise_stub.get_permutation(noise_model.noise_query_pb2.PB_NoiseQuery(index=model_index_el)).data
            res_deperm_total[i] = self._remove_permutation_from_evaluation(res[model_index_el], perm)
        return res_deperm_total
                    

    def _save_all_generations_data(self, xs):
        with open(f"{self.ga_result_folder_name}/all_generations_denoised.pkl", "wb") as f:
            pickle.dump(xs, f)

    def _save_all_generations_result(self, xs):
        with open(f"{self.ga_result_folder_name}/all_prediction_results_depermed.pkl", "wb") as f:
            pickle.dump(xs, f)
        

    def extract_data_from_all_generations(self):
        all_children_denoised, all_children_results_depermed = self._load_all_generations_data()
        if self.is_choose_from_condition:
            chosen_data, chosen_ys = self._choose_data_with_condition(all_children_denoised[:len(all_children_results_depermed)], all_children_results_depermed)
            if len(chosen_data) == 0:
                raise Exception("len(chosen_data) == 0, please check c.CHOOSE_Y1_MIN, c.CHOOSE_Y1_MAX, c.CHOOSE_Y2_MIN, c.CHOOSE_Y2_MAX")
        else:
            chosen_data, chosen_ys = self._choose_data_close_to_target(all_children_denoised[:len(all_children_results_depermed)], all_children_results_depermed, c.NUM_CHOOSE_FROM_GENERATIONS)
        #plot_utils.plot_chosen_data(chosen_ys)
        print(f"chosen_data.shape: {chosen_data.shape}")

        return chosen_data, chosen_ys

    def _is_in_condition(self, y):
        assert len(y) == len(self.conditions), f"{len(y)} != {len(self.conditions)}"

        judges = []
        for i in range(len(y)):
            judges.append(self.conditions[i][0] <= y[i] and y[i] <= self.conditions[i][1])

        return all(judges)


    def _choose_data_with_condition(self, xs, ys):
        chosen_data = []
        chosen_ys = []
        xs = xs.reshape(-1, xs.shape[2])
        ys = ys.reshape(-1, ys.shape[2])
        for i in range(len(xs)):
            if self._is_in_condition(ys[i]):
                chosen_data.append(xs[i])
                chosen_ys.append(ys[i])
        chosen_data = np.array(chosen_data)
        chosen_ys = np.array(chosen_ys)
        return chosen_data, chosen_ys


    def _choose_data_close_to_target(self, xs, ys, pick_size):
        diff = ys - self.goal
        diff = diff * diff
        diff = np.sum(diff, axis=1)
        sort_index = np.argsort(diff)

        return xs[sort_index][:pick_size], ys[sort_index][:pick_size]
        

    def _load_all_generations_data(self):
        with open(f"{self.ga_result_folder_name}/all_generations_denoised.pkl", "rb") as f:
            all_generations_denoised = pickle.load(f)
        with open(f"{self.ga_result_folder_name}/all_prediction_results_depermed.pkl", "rb") as f:
            all_prediction_results_depermed = pickle.load(f)
        
        # データ、結果共に全ての子世代を選択
        zeroth_generation_results_depermed = np.array(all_prediction_results_depermed[:1])
        all_children_results_depermed = np.array(all_prediction_results_depermed[1:])

        zeroth_generation_results_depermed = np.transpose(zeroth_generation_results_depermed, (0, 2, 1))
        all_children_results_depermed = np.transpose(all_children_results_depermed, (0, 2, 1))

        assert len(all_generations_denoised) == len(all_children_results_depermed) + 1, f"{len(all_generations_denoised)} != {len(all_children_results_depermed)}"

        return np.array(all_generations_denoised), np.array(all_children_results_depermed)
    

    def _plot_all_generations_in_y(self, xs):
        zeroth_generation_results_depermed = np.array(xs[:1])
        all_children_results_depermed = np.array(xs[1:])

        zeroth_generation_results_depermed = np.transpose(zeroth_generation_results_depermed, (0, 2, 1))
        all_children_results_depermed = np.transpose(all_children_results_depermed, (0, 2, 1))
        self._plot_all_generations_plt(zeroth_generation_results_depermed, all_children_results_depermed)
    

    def _get_colors(self):
        ## プロットに使用する色の準備
        colors = []
        for i in range(1000):
            tmp = []
            for j in range(3):
                tmp.append(random.random())
            colors.append(tmp)
        return colors

    def _plot_all_generations_plt(self, zeroth_prediction_results_depermed, all_prediction_results_depermed):
        colors = self._get_colors()
        ax = plt.subplot()
        ax.set_xlabel("PC1")
        ax.set_ylabel("PC2")
        ax.set_ylim(c.PLOT_MIN_Y, c.PLOT_MAX_Y)
        ax.set_xlim(c.PLOT_MIN_X, c.PLOT_MAX_X)
        ax.scatter(c.GOAL[0], c.GOAL[1], color=(0,0,1), s=c.SCATTER_SIZE)
        res = zeroth_prediction_results_depermed[0] 
        for j in range(len(res)):
            ax.scatter(res[j][0], res[j][1], color=colors[0], s=c.SCATTER_SIZE)
        plt.savefig(f"{self.ga_result_folder_name}/plot_all_generation_0.png")

        for i in range(len(all_prediction_results_depermed)):
            res = all_prediction_results_depermed[i]
            ax.scatter(c.GOAL[0], c.GOAL[1], color=(0,0,1), s=c.SCATTER_SIZE)
            for j in range(len(res)):
                ax.scatter(res[j][0], res[j][1], color=colors[i+1], s=c.SCATTER_SIZE)
            plt.savefig(f"{self.ga_result_folder_name}/plot_all_generation_{i+1}.png")
        plt.close()

    def apply(self):
        # 遺伝アルゴリズム最適化スタート====================================================

        # 第一世代のみlir_and_ga にはファイルからロードしたデータを使用する（もしくは自分でランダム生成する）
        # lir_and_ga の is_need_denoise はFalseに設定する（ロードしたデータにはノイズが含まれていない)
        # 最適化するデータをロード
        #xs, ys, w, b = data.load_data()
        xs = np.random.randn(self.ga_data_size, self.attrs)

        # # サービスの利用自体には必要ないが、プロットするためにPCAを行う
        # pca = PCA(n_components=2)
        # pca = pca.fit(xs)


        # 遺伝アルゴリズムのメインループ(c.ITERATION は世代の数)
        all_generations_denoised = []
        all_prediction_results_depermed = []
        res_diffs = []
        for i in range(0, self.iteration):
            t1 = time.time()
            # lir_and_ga の is_need_denoise はTrueに設定する（GAサービスから生成された次世代データにはノイズが含まれているため、LIRサービスは推論する前に暗号状態でノイズを取り除く必要がある)
            if i == 0:
                next_gens, res_total = self._predict_and_apply_ga(xs, False)
            else:
                next_gens, res_total = self._predict_and_apply_ga(next_gens, True)
            
            next_gens_denoised = self._debug_remove_noise_from_generation(next_gens)
            res_total_depermed = self._debug_remove_permutation_from_prediction(res_total)

            #res_diffs.append(current_diff)
            all_generations_denoised.append(next_gens_denoised)
            all_prediction_results_depermed.append(res_total_depermed)

            t2 = time.time()
            print(f"\n\nloop for lir_and_ga {i} / {self.iteration} done: time = {t2-t1}")
        
        # all_generations_denoised = ((全ての子世代), 各世代の数, ATTRS)
        # all_prediction_results_depermed = ((最初の世代＋全ての子世代), 2, 各世代の数)
        self._save_all_generations_data(all_generations_denoised)
        self._save_all_generations_result(all_prediction_results_depermed)
        self._plot_all_generations_in_y(all_prediction_results_depermed)