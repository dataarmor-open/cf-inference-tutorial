import pickle
import numpy as np
import pandas as pd

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

from collections import Counter
import copy
def prepare_tree(model_path):
    """prepare random forest classifier
    """
    iris = datasets.load_iris()
    X, y = iris.data, iris.target

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    clf = RandomForestClassifier(max_depth=5, random_state=0)
    clf.fit(X_train, y_train)

    pickle.dump(clf, open(model_path, 'wb'))



if __name__ == "__main__":
  prepare_tree("rf_iris.pkl")

