# Copyright (c) EAGLYS Inc. All rights reserved.

from include import *

def dispense_data():
  dataset = np.loadtxt("data_banknote_authentication.txt", delimiter=",")

  x = dataset[:, 0:4]
  y = dataset[:, 4]

  x = np.array(x)
  y = np.array(y)

  x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

  return x_train, x_test, y_train, y_test



if __name__ == "__main__":
  # set model index
  model_index = 3

  # grpc stub to input_client
  api_input_client = network_utils.get_input_client_stub()

  # grpc stub to model_client
  api_model_client = network_utils.get_model_client_stub()

  batch_size = 10


  # prepare data for inference
  x, _, y, _ = dispense_data()
  # datasize should match with model_info.batch_size
  rand = random.randint(0, len(x)-batch_size)
  test_data = x[rand:rand+batch_size]

  # call input_client.predict for cipher prediction

  t1 = time.time()
  data_for_cf = test_data.tolist()
  input_data = Tensor()
  input_data.data = pickle.dumps(data_for_cf)
  input_data.model_index = model_index

  result = api_input_client.predict(input_data)
  result = pickle.loads(result.data)
  result = np.round(result)

  t2 = time.time()


  # make comparison by predicting with sklearn raw model
  with open('lr_bank.pkl', 'rb') as f:
    raw_model = pickle.load(f)
  result_raw = raw_model.predict(test_data)

  print("\n==========")
  print("cipher result")
  print(result)
  print("\n==========")
  print("raw result")
  print(result_raw)
  print("\n==========")
  print("y_true")
  print(y[rand:rand+batch_size])

  print("\n==========")
  print(f"prediction time >>> {t2-t1}")



