# ロジスティック回帰モデルのコンパイルチュートリアル


## 概要
このチュートリアルでは、  

`Scikit Learnによって生成されたロジスティック回帰のモデル`を  
`GateAIのAPIを使って秘密計算モデルにコンパイル`します。  
コンパイルされた秘密計算モデルは`自動的にホスティング`され、  
ユーザはそのモデルに対して`暗号化した入力を送信、結果を得る`。

という流れのチュートリアルとなっています。


## チュートリアルに必要なサービス

GateAI Compile のサービス群が必要となります。

- `input_client`
- `model_client`
- `calculation`
- `calculation_with_key`


## チュートリアルの手順

### 1. GateAIサービス群を立ち上げ、テスト環境に入る

手元に`gateai_keras` とタグがついたDocker イメージを取得した状態で、

```
make folder
make start_all && make start_client && make connect_client
```

を実行します。
実行した段階で、

```
root@8dc99ca218ea:/client/cf-inference-tutorial#
```

コンソール上でテストクライアントのコンテナ環境に入ることができます。


### 2. テストコンテナ内での準備

```
 source setup.sh
 cd tutorial_compiler/tutorial_bank
```

とし、

```
python train.py
```

を実行します。  
実行すると、`lr_bank.pkl` が生成されます。  
このファイルは、Scikitlearn のLogisticRegressionモジュールを使って平文で生成されたモデルです。


### 3. 鍵生成

```
python keygen.py
```

により、`input_client`にて秘密計算に必要な鍵ペアを生成します。


### 4. モデルのアップロード

```
python upload_model.py
```

により、`model_client`に学習されたモデルをアップロードします。  
`model_client`はモデルを秘密計算可能なモデルへとコンパイルし、  
`calculation`, `calculation_with_key`へと配布します。  

モデルは自動的に入力を待つ状態でホスティングされます。



### 5. 暗号を入力とした推論の実行

```
python predict.py
```

を実行します。

まず、データはクライアントから`input_client`へと送信され、暗号化を施されます。  
暗号化されたデータは`calculation_with_key`, `calculation` のサーバでやりとりされ、　　
最終的にモデルの演算が全て完了すると、暗号文は `input_client`へと返却されます。　　

`input_client`は返却された暗号を復号し、クライアントへと戻します。  

チュートリアル実行時の標準出力には、  

```
==========
cipher result
[0. 1. 1. 0. 0. 0. 1. 1. 0. 1.]

==========
raw result
[0. 1. 1. 0. 0. 0. 1. 1. 0. 1.]

==========
y_true
[0. 1. 1. 0. 0. 0. 1. 1. 0. 1.]

==========
prediction time >>> 0.04253983497619629
```

というような出力が表示されます。

- `cipher result`はGateAIを用いたときの推論結果
- `raw result`は対照実験として、Scikitlearnを用いて平文で推論したときの結果
- `y_true`はラベルデータ
- `prediction time`はGateAIでの予測にかかった時間


を表示しています。

これでチュートリアルは終わりとなります。

お疲れ様でした。


