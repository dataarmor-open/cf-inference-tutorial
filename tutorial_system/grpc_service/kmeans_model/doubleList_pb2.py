# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: kmeans_model/doubleList.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x1dkmeans_model/doubleList.proto\"g\n\rPB_DoubleList\x12\x0c\n\x04\x64\x61ta\x18\x01 \x03(\x01\x12\x13\n\x0bmodel_index\x18\x02 \x01(\x05\x12\x0c\n\x04size\x18\x03 \x01(\x05\x12\x10\n\x08is_noise\x18\x04 \x01(\x08\x12\x13\n\x0bnoise_index\x18\x05 \x01(\x05\x62\x06proto3')



_PB_DOUBLELIST = DESCRIPTOR.message_types_by_name['PB_DoubleList']
PB_DoubleList = _reflection.GeneratedProtocolMessageType('PB_DoubleList', (_message.Message,), {
  'DESCRIPTOR' : _PB_DOUBLELIST,
  '__module__' : 'kmeans_model.doubleList_pb2'
  # @@protoc_insertion_point(class_scope:PB_DoubleList)
  })
_sym_db.RegisterMessage(PB_DoubleList)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PB_DOUBLELIST._serialized_start=33
  _PB_DOUBLELIST._serialized_end=136
# @@protoc_insertion_point(module_scope)
