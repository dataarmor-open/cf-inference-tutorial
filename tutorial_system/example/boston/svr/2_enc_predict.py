import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
import time
import datetime

sys.path.append("../../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lssvr import ENC_LSSVR

C = 10
epsilon = 0.1

C_plain = 10
epsilon_plain = 0.1
boston = load_boston()
xs = boston.data
ys = boston.target
x_train, x_test, y_train, y_test = train_test_split(xs, ys)

if __name__ == "__main__":
    start = time.time()
    preprocess = ENC_PREPROCESS()
    preprocess.reset()
    # preprocessサーバーの"tutorial_of_boston_data"という鍵を読み込む
    preprocess.load_key_on_server("tutorial_of_boston_data")
    # preprocessサーバーの"boston_standard_scaler"という平均・分散の値を読み込む
    preprocess.load_standard_scaler_on_server("boston_standard_scaler")

    lssvr = ENC_LSSVR()
    lssvr.reset()
    # SVRサーバーの"tutorial_of_boston_data"という鍵を読み込む
    lssvr.load_key_on_server("tutorial_of_boston_data")
    # SVRサーバーの"boston_lssvr_model"という学習済みモデルを読み込む
    lssvr.load_model_on_server("boston_lssvr_model")

    # 推論用のデータを送信する（今回は例のためボストンデータセットの一部）
    preprocess.transfer_x_for_predict(x_test)
    # preprocessサーバーの推論用データを標準化する
    preprocess.standard_scaler_transform_x_test()
    # preprocessサーバーの推論用データをSVRに送信する
    preprocess.send_svr_only_test()
    # 推論を行う
    y_hat = lssvr.predict_already_send()

    print(
        "ENC_SVR: MSE = ",
        mean_squared_error(y_test, y_hat),
        ", MAE = ",
        mean_absolute_error(y_test, y_hat),
        ", R2 = ",
        r2_score(y_test, y_hat),
    )
    end = time.time()
    print(f"calcuration time: {datetime.timedelta(seconds = end-start)}")