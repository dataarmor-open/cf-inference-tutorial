# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: common_model/message.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x1a\x63ommon_model/message.proto\"\xe1\x01\n\nPB_Message\x12\x0f\n\x07message\x18\x01 \x01(\t\x12\x13\n\x0bmodel_index\x18\x02 \x01(\x05\x12\x1d\n\x15is_preprocess_denoise\x18\x03 \x01(\x08\x12\x1c\n\x14is_preprocess_deperm\x18\x04 \x01(\x08\x12\x1c\n\x14is_postprocess_noise\x18\x05 \x01(\x08\x12\x1b\n\x13is_postprocess_perm\x18\x06 \x01(\x08\x12\x13\n\x0bparty_index\x18\x07 \x01(\x05\x12\x11\n\tdata_size\x18\x08 \x01(\x05\x12\r\n\x05value\x18\t \x01(\x01\x62\x06proto3')



_PB_MESSAGE = DESCRIPTOR.message_types_by_name['PB_Message']
PB_Message = _reflection.GeneratedProtocolMessageType('PB_Message', (_message.Message,), {
  'DESCRIPTOR' : _PB_MESSAGE,
  '__module__' : 'common_model.message_pb2'
  # @@protoc_insertion_point(class_scope:PB_Message)
  })
_sym_db.RegisterMessage(PB_Message)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PB_MESSAGE._serialized_start=31
  _PB_MESSAGE._serialized_end=256
# @@protoc_insertion_point(module_scope)
