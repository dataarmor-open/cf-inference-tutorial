import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston

sys.path.append("../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lssvr import ENC_LSSVR

C = 10
epsilon = 0.1

C_plain = 10
epsilon_plain = 0.1
boston = load_boston()
xs = boston.data
ys = boston.target

if __name__ == "__main__":
    preprocess = ENC_PREPROCESS()
    preprocess.load_key_on_server("tutorial_of_boston_data")
    
    lssvr = ENC_LSSVR()
    lssvr.load_key_on_server("tutorial_of_boston_data")
    
    preprocess.transfer_data(xs, ys)
    preprocess.train_test_split(test_size=0.1, random_state=1)
    preprocess.standard_scaler_fit()
    preprocess.save_standard_scaler_on_server("boston_standard_scaler")
    
    preprocess.standard_scaler_transform_x_train()
    preprocess.standard_scaler_transform_x_test()
    preprocess.send_svr()
    
    lssvr.set_config(
        C,
        epsilon
    )
    lssvr.train_with_score()
    lssvr.save_model_on_server("boston_lssvr_model")