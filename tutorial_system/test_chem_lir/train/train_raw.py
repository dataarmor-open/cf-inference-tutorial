import sys
import os
import shutil
sys.path.append("../../")
sys.path.append("../")

import numpy as np
from sklearn.linear_model import LinearRegression
import data
import pickle
import constants_client as c

def main():
    xs, ys1, ys2 = data.get_data()
    print("=============")
    print(xs.shape)
    print(ys1.shape)
    print(ys2.shape)

    reg1 = LinearRegression().fit(xs, ys1)
    print(reg1.score(xs, ys1))
    print(reg1.coef_)
    print(reg1.intercept_)

    reg2 = LinearRegression().fit(xs, ys2)
    print(reg2.score(xs, ys2))
    print(reg2.coef_)
    print(reg2.intercept_)


    with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/model0.pkl", "wb") as f:
        pickle.dump(reg1, f)
    with open(f"{c.LIR_TRAIN_RESULT_FOLDER_NAME}/model1.pkl", "wb") as f:
        pickle.dump(reg2, f)

if __name__ == "__main__":
    print("hello, world")
    main()