#!/usr/bin/python
# -*- coding: utf-8 -*-

from multiprocessing.sharedctypes import Value
from xml.dom import InvalidModificationErr
from tqdm import tqdm
from re import X
from multiprocessing.spawn import prepare
import sys
from tkinter import Y
import grpc
import numpy as np
import pytest
from math import isclose
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

sys.path.append("../")
import constants_client
from gateai_connector.plain_lssvr import LSSVR_GRAD
from grpc_service import input_client_pb2_grpc
from grpc_service import common_model
from grpc_service import noise_service_pb2_grpc
from grpc_service import svr_model


def make_pb_from_doubleList(doubleList):
    if len(doubleList.shape) == 2:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = np.ravel(doubleList)
        pb.size = doubleList.shape[1]
    else:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = doubleList
        pb.size = 0
    return pb


def make_doubleList_from_pb(pb_doubleList):
    if pb_doubleList.size is not 0:
        tmp = np.array(pb_doubleList.data)
        data = tmp.reshape(-1, pb_doubleList.size)
    else:
        data = np.array(pb_doubleList.data)
    return data


def make_intList_from_pb(self, pb_intList):
    if pb_intList.size is not 0:
        tmp = np.array(pb_intList.data)
        data = tmp.reshape(-1, pb_intList.size)
    else:
        data = np.array(pb_intList.data)
    return data


"""ENC_PREPROCESS

* PREPROCESSサービスを利用するためのinput_clientとのコネクタ

"""


class ENC_PREPROCESS:
    def __init__(self):
        channel_input_client = grpc.insecure_channel(
            f"{constants_client.INPUT_CLIENT_HOST}:{constants_client.INPUT_CLIENT_PORT}",
            options=[
                ("grpc.max_message_length", constants_client.GRPC_MAX_MESSAGE_LENGTH),
                ("grpc.max_receive_message_length", constants_client.GRPC_MAX_MESSAGE_LENGTH),
            ],
        )
        self.stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    def reset(self):
        """PREPROCESSインスタンスの初期化
        送信したパラメータ、X, yを初期化する。
        """
        self.stub.preprocess_reset_data(common_model.message_pb2.PB_Message())

    def keygen(self):
        """鍵生成
        input_clientに鍵を生成する。
        """
        self.stub.preprocess_gen_key(common_model.message_pb2.PB_Message())

    def save_key_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_save_secret_key(pb_message)

    def load_key_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_load_secret_key(pb_message)

    def transfer_data(self, x=None, y=None, axis=0):
        """データ送信
        PREPROCESSにデータを送信する。
        """
        if (x is not None) and (x.ndim is not 2):
            raise ValueError("x is need to 2 dim.")

        if (y is not None) and (y.ndim is not 1):
            raise ValueError("y is need to 1 dim.")

        if axis == 0:  # vstack
            if x is not None:
                pb_x = make_pb_from_doubleList(x)
                self.stub.preprocess_transfer_x_vstack(pb_x)
            if y is not None:
                pb_y = make_pb_from_doubleList(y)
                self.stub.preprocess_transfer_y_vstack(pb_y)
        elif axis == 1:  # hstack
            if x is not None:
                pb_x = make_pb_from_doubleList(x)
                self.stub.preprocess_transfer_x_hstack(pb_x)
            if y is not None:
                pb_y = make_pb_from_doubleList(y)
                self.stub.preprocess_transfer_y_vstack(pb_y)
        else:
            raise ValueError("axis is only support 0 or 1.")

    def save_input_x_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_save_input_x(pb_message)

    def load_input_x_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_load_input_x(pb_message)

    def save_input_y_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_save_input_y(pb_message)

    def load_input_y_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_load_input_y(pb_message)

    def transfer_x_for_predict(self, x):
        pb_x = make_pb_from_doubleList(x)
        self.stub.transfer_x_test(pb_x)

    def move_x_train_all(self):
        self.stub.move_train_all(common_model.message_pb2.PB_Message())

    def shape_x(self):
        pb = self.stub.preprocess_apply_shape_x(common_model.message_pb2.PB_Message())
        x_shape = np.array(pb.data)
        return x_shape

    def shape_y(self):
        pb = self.stub.preprocess_apply_shape_y(common_model.message_pb2.PB_Message())
        y_shape = pb.number
        return y_shape

    def train_test_split(self, test_size=None, train_size=None, shuffle=True, random_state=None):
        x_shape = self.shape_x()
        y_shape = self.shape_y()
        if x_shape[0] != y_shape:
            raise ValueError(f"x_shape[0] != y_shape {x_shape}, {y_shape}")
        indices = np.arange(y_shape)
        train_indices, test_indices = train_test_split(
            indices, test_size=test_size, train_size=train_size, shuffle=shuffle, random_state=random_state
        )
        pb = common_model.trainTestIndices_pb2.PB_TrainTestIndices()
        pb.train[:] = train_indices
        pb.test[:] = test_indices
        self.stub.preprocess_train_test_split_with_indices(pb)

    def train_test_split_with_indices(self, train_indices, test_indices):
        x_shape = self.shape_x()
        y_shape = self.shape_y()
        if x_shape[0] != y_shape:
            raise ValueError(f"x_shape[0] != y_shape {x_shape}, {y_shape}")
        pb = common_model.trainTestIndices_pb2.PB_TrainTestIndices()
        pb.train[:] = train_indices
        pb.test[:] = test_indices
        self.stub.preprocess_train_test_split_with_indices(pb)

    # TODO:remove
    def minmax_scaler(self):
        self.stub.preprocess_apply_min_of_x_train(common_model.message_pb2.PB_Message())
        self.stub.preprocess_apply_max_of_x_train(common_model.message_pb2.PB_Message())
        self.stub.preprocess_apply_fit_minmax(common_model.message_pb2.PB_Message())
        self.stub.preprocess_apply_minmax_transform_x_train(common_model.message_pb2.PB_Message())
        self.stub.preprocess_apply_minmax_transform_x_test(common_model.message_pb2.PB_Message())

    def minmax_scaler_fit(self):
        self.stub.preprocess_apply_min_of_x_train(common_model.message_pb2.PB_Message())
        self.stub.preprocess_apply_max_of_x_train(common_model.message_pb2.PB_Message())
        self.stub.preprocess_apply_fit_minmax(common_model.message_pb2.PB_Message())

    def save_minmax_scaler_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_save_minmax_scaler(pb_message)

    def load_minmax_scaler_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_load_minmax_scaler(pb_message)

    def minmax_scaler_transform_x_train(self):
        self.stub.preprocess_apply_minmax_transform_x_train(common_model.message_pb2.PB_Message())

    def minmax_scaler_transform_x_test(self):
        self.stub.preprocess_apply_minmax_transform_x_test(common_model.message_pb2.PB_Message())

    def minmax_scalar_for_predict(self):
        self.stub.preprocess_apply_minmax_transform_x_test(common_model.message_pb2.PB_Message())

    def send_svr(self):
        self.stub.transfer_data_to_svr_from_preprocess(common_model.message_pb2.PB_Message())
        self.stub.svr_convert_preprocess_data(common_model.message_pb2.PB_Message())

    def send_svr_only_test(self):
        self.stub.transfer_x_test_to_svr_from_preprocess(common_model.message_pb2.PB_Message())
        self.stub.svr_convert_preprocess_x_test(common_model.message_pb2.PB_Message())

    def send_lir(self):
        self.stub.transfer_data_to_lir_from_preprocess(common_model.message_pb2.PB_Message())
        self.stub.lir_convert_preprocess_data(common_model.message_pb2.PB_Message())

    def send_lir_only_test(self):
        self.stub.transfer_x_test_to_lir_from_preprocess(common_model.message_pb2.PB_Message())
        self.stub.lir_convert_preprocess_x_test(common_model.message_pb2.PB_Message())

    def debug_transfer_already_preprocessed_data(self, x_train, y_train, x_test, y_test):
        # transfer x_train
        pb_x = make_pb_from_doubleList(x_train)
        self.stub.transfer_x_train(pb_x)
        # transfer x_test
        pb_x = make_pb_from_doubleList(x_test)
        self.stub.transfer_x_test(pb_x)
        # transfer y_train
        pb_y = make_pb_from_doubleList(y_train)
        self.stub.transfer_y_train(pb_y)
        # transfer y_test
        pb_y = make_pb_from_doubleList(y_test)
        self.stub.transfer_y_test(pb_y)

    # def save_input_data_on_server(self, name):
    #     pb_message = common_model.message_pb2.PB_Message()
    #     pb_message.message = name
    #     self.stub.preprocess_save_input_data(pb_message)

    # def load_input_data_on_server(self, name):
    #     pb_message = common_model.message_pb2.PB_Message()
    #     pb_message.message = name
    #     self.stub.preprocess_load_input_data(pb_message)

    def standard_scaler_fit(self):
        self.stub.preprocess_calc_var(common_model.message_pb2.PB_Message())
        self.stub.preprocess_calc_standard(common_model.message_pb2.PB_Message())
        # self.stub.preprocess_apply_fit_minmax(common_model.message_pb2.PB_Message())

    def save_standard_scaler_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_save_standard_scaler(pb_message)

    def load_standard_scaler_on_server(self, name):
        pb_message = common_model.message_pb2.PB_Message()
        pb_message.message = name
        self.stub.preprocess_load_standard_scaler(pb_message)

    def standard_scaler_transform_x_train(self):
        self.stub.preprocess_apply_standard_transform_x_train(common_model.message_pb2.PB_Message())

    def standard_scaler_transform_x_test(self):
        self.stub.preprocess_apply_standard_transform_x_test(common_model.message_pb2.PB_Message())
