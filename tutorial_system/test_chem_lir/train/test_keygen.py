import sys
sys.path.append("../../")

import grpc

from grpc_service import input_client_pb2_grpc
from grpc_service import common_model

import constants_client as c

def main():
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=[])
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    for i in range(c.NUMBER_OF_GOALS):
        stub.gen_key_for_training_for_lir(common_model.message_pb2.PB_Message(model_index=i))

if __name__ == "__main__":
    print("hello, world")
    main()
