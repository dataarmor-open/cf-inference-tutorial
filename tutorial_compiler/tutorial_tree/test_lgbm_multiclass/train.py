import pickle
import numpy as np
import pandas as pd

import lightgbm as lgb

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

from collections import Counter
import copy
def prepare_tree(model_path):
    """prepare random forest classifier
    """
    iris = datasets.load_iris()
    X, y = iris.data, iris.target

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    lgb_train = lgb.Dataset(X_train, y_train)
    lgb_eval = lgb.Dataset(X_test, y_test, reference=lgb_train)

    lgbm_params = {
        'objective': 'multiclass',
        'num_class': 3,
        'n_estimators': 30
    }

    model = lgb.train(lgbm_params, lgb_train, valid_sets=lgb_eval)

    y_pred = model.predict(X_test, num_iteration=model.best_iteration)
    y_pred_max = np.argmax(y_pred, axis=1)

    accuracy = sum(y_test == y_pred_max) / len(y_test)
    print(accuracy)


    pickle.dump(model, open(model_path, 'wb'))



if __name__ == "__main__":
  prepare_tree("lgbm_multi_iris.pkl")

