import sys
import pickle
sys.path.append("../")
sys.path.append("../../")

import numpy as np
import data
from optimization import plot_utils
import constants_client as c
from grpc_service import common_model
from grpc_service import noise_model

def is_in_condition(y):
    condition1 = c.CHOOSE_Y1_MIN <= y[0] and y[0] <= c.CHOOSE_Y1_MAX
    condition2 = c.CHOOSE_Y2_MIN <= y[1] and y[1] <= c.CHOOSE_Y2_MAX

    return condition1 and condition2


def choose_data_with_condition(xs, ys):
    chosen_data = []
    chosen_ys = []
    for i in range(len(xs)):
        if is_in_condition(ys[i]):
            chosen_data.append(xs[i])
            chosen_ys.append(ys[i])
    chosen_data = np.array(chosen_data)
    return chosen_data, chosen_ys


def choose_data_close_to_target(xs, ys, pick_size):
    diff = ys - c.GOAL
    diff = diff * diff
    diff = np.sum(diff, axis=1)
    sort_index = np.argsort(diff)

    return xs[sort_index][:pick_size], ys[sort_index][:pick_size]
    


def remove_permutation_from_evaluation(xs, permutation):
    assert len(xs) == len(permutation), f"{len(xs)} != {len(permutation)}"

    res = np.zeros(len(xs))
    for i in range(len(xs)):
        res[permutation[i]] = xs[i]

    return res


def _calculate_diff_btw_predicted_result_and_target(res_total):
    ## 推論結果（並び替えられている）に対して、目標値との距離を計算する
    diff_goals = []
    for i in range(c.NUMBER_OF_GOALS):
        diff_goal = res_total[i] - c.GOAL[i]
        diff_goals.append(diff_goal)
    
    return diff_goals

def _send_diff_to_ga_service(diff_goals, stub):

    ## 目標値との距離をflattenし、GAサービスへ送信する。GAサービスはこの情報をもとに、暗号化された親データからエリートをピックアップする。
    diff_goals = np.array(diff_goals)
    diff_goals_flatten = diff_goals.reshape(-1)
    assert diff_goals_flatten.shape[0] == c.NUMBER_OF_GOALS * diff_goals.shape[1]
    pb_evaluation = common_model.doubleList_pb2.PB_DoubleList()
    pb_evaluation.data[:] = diff_goals_flatten
    pb_evaluation.size = diff_goals.shape[1]
    ## クライアントが所持するLIRサービスからの結果は並び替えが施されているので、GAサービスは逆並び替えを行う必要があることを明示する
    pb_evaluation.is_preprocess_deperm = c.PREPROCESS_DEPERM_DIFF_GA_SERVICE
    ## GAサービスへのアップロード
    stub.upload_evaluation_for_ga(pb_evaluation)


def _send_parents_to_ga_service(xs, stub, is_need_denoise):
    ## GAサービスへ、親データを送信する(input_client内で暗号化され、GAサービスへと送信される)
    ## xs.shape == (送信するデータの数, 属性数)
    assert len(xs.shape) == 2
    assert xs.shape[1] == c.ATTRS
    pb_parents = common_model.doubleList_pb2.PB_DoubleList() 
    pb_parents.data[:] = xs.reshape(-1)
    pb_parents.size = xs.shape[0]
    ## GAサービスに送信する親データは、第一世代のみノイズを含んでおらず、
    ## 第二世代から（つまりGAサービスからの結果）はノイズを含んでいるので、
    ## 次世代を作るときにノイズ除去が必要かどうか明記する
    if c.PREPROCESS_DENOISE_PARENTS_GA_SERVICE is not None:
        pb_parents.is_preprocess_denoise = c.PREPROCESS_DENOISE_PARENTS_GA_SERVICE
    else:
        pb_parents.is_preprocess_denoise = is_need_denoise
    stub.upload_encrypted_parent_for_ga_stream(pb_parents)


def _request_next_generation(stub):
    ## GAサービスに対し、エリートのピックアップ、次世代データの生成を要求する
    pb = common_model.message_pb2.PB_Message()
    ## GAサービスに対し、次世代データにノイズを加えることを明示する
    pb.is_postprocess_noise = c.POSTPROCESS_NOISE_RESULT_GA_SERVICE
    next_gens = stub.query_for_ga_stream(pb)
    next_gens_np = np.array(next_gens.data)
    assert next_gens_np.shape[0] % c.ATTRS == 0
    next_gens_np = next_gens_np.reshape(-1, c.ATTRS)

    return next_gens_np


def _request_feature_prediction_to_lir_service(next_gens_np, stub, is_need_denoise):
    next_repeat = next_gens_np.shape[0]

    # PB_DoubleList にデータを挿入し、model_indexで送信するlirサービスを指定する
    pb = common_model.doubleList_pb2.PB_DoubleList()
    xs_np = np.zeros(c.ATTRS * next_repeat)
    for i in range(next_repeat):
        xs_np[c.ATTRS*i:(i+1)*c.ATTRS] = next_gens_np[i]
    
    pb.data[:] = xs_np
    
    # 2つあるlir サービスに対してデータを送信し、推論結果を取得する
    res_total = []
    for i in range(c.NUMBER_OF_GOALS):
        pb.model_index = i
        # input_client_stub.predict_lir_one_repeast(pb)APIを使用し、推論を実行する。
        # 2世代目以降のデータにはノイズが付与されていることから、ノイズ除去（denoise）を必要とする。
        # predict_lir_one_repeat APIは、lirサービスに対してノイズ除去を行う指令が含まれている
        if c.PREPROCESS_DENOISE_DATA_LIR_SERVICE is not None:
            pb.is_preprocess_denoise = c.PREPROCESS_DENOISE_DATA_LIR_SERVICE
        else:
            pb.is_preprocess_denoise = is_need_denoise
        # LIRサービスに対して、出力結果を並び替えることを明示する
        pb.is_postprocess_perm = c.POSTPROCESS_PERM_RESULT_LIR_SERVICE
        tmp = stub.predict_lir_one_repeat_stream(pb).data

        assert len(tmp) == next_repeat
        res_total.append(np.array(tmp))

    res_total = np.array(res_total)
    assert res_total.shape[0] == c.NUMBER_OF_GOALS
    assert res_total.shape[1] == next_repeat

    return res_total


def remove_noise_from_generations(stub_noise, next_gens_np, attrs):
    noise = stub_noise.get_noise_all(noise_model.noise_query_pb2.PB_NoiseQuery())
    assert len(noise.data) == len(next_gens_np) * attrs
    noise_np = np.zeros((len(next_gens_np), attrs))
    for i in range(len(next_gens_np)):
      for j in range(attrs):
        noise_np[i][j] = noise.data[i*attrs + j]
    next_gens_np = next_gens_np.reshape(len(next_gens_np), attrs)
    next_gens_wo_noise = next_gens_np - noise_np

    return next_gens_wo_noise



def lir_and_ga(next_gens_np, loop_index, stub, is_need_denoise):
    # 最初の世代生成の際は、データは自分でロード（data.load_data)を行う。
    # また、LIRサービスによって推論を行う際、初期親データにノイズはまだ付与されていないため、ノイズ除去を要求する必要がないため、
    # predict_lir_one_repeat_wo_denoise を呼んでいる
    # GAサービスにより生成された次世代データにはノイズが付与されているため、２回目以降はpredict_lir_one_repeatを呼んでいる
    # lir サービスにより推論を行った後、GAサービスにより結果の良かったデータから次世代データを生成する。
    # 親世代となるデータは、GAサービスから出力された子データであることから、ノイズが付与されている。
    # したがって、lirサービスに対して推論前にノイズ除去を要求する必要があることから、predict_lir_one_repeatを呼んでいる

    res_total = _request_feature_prediction_to_lir_service(next_gens_np, stub, is_need_denoise) 
    
    ## このプロットは、LIR0 とLIR2の並び替えが別々に行われていることから、本当のデータのプロットではないことに注意する
    plot_utils.plot_predicted_feature_and_targets(res_total, loop_index)

    # クライアントは線形回帰で推定された物性値と、目標値の差を計算する
    diff = _calculate_diff_btw_predicted_result_and_target(res_total=res_total)
    current_diff = np.average(np.abs(np.array(diff)), axis=1)
    print(f"current_diff: {current_diff}")

    # 差をGAサービスへと送信する
    _send_diff_to_ga_service(diff_goals=diff, stub=stub)

    # 遺伝アルゴリズムの対象となる親（現在の世代）をGAサービスへと送信する
    _send_parents_to_ga_service(next_gens_np, stub, is_need_denoise)

    ## 目標値との距離をflattenし、GAサービスへ送信する。GAサービスはこの情報をもとに、暗号化された親データからエリートをピックアップする。
    next_gens_np = _request_next_generation(stub)

    return next_gens_np, current_diff


def lir_and_ga_deperm_denoise(next_gens_np, loop_index, stub, stub_noise, is_need_denoise):
    # 最初の世代生成の際は、データは自分でロード（data.load_data)を行う。
    # また、LIRサービスによって推論を行う際、初期親データにノイズはまだ付与されていないため、ノイズ除去を要求する必要がないため、
    # predict_lir_one_repeat_wo_denoise を呼んでいる
    # GAサービスにより生成された次世代データにはノイズが付与されているため、２回目以降はpredict_lir_one_repeatを呼んでいる
    # lir サービスにより推論を行った後、GAサービスにより結果の良かったデータから次世代データを生成する。
    # 親世代となるデータは、GAサービスから出力された子データであることから、ノイズが付与されている。
    # したがって、lirサービスに対して推論前にノイズ除去を要求する必要があることから、predict_lir_one_repeatを呼んでいる

    res_total = _request_feature_prediction_to_lir_service(next_gens_np, stub, is_need_denoise) 


    ############################ DEPERM ######################################
    # res_total は入力データ全てに対する推論結果だが、結果は並び替えられている
    # noise サービスにアクセスして並び替えを解除する
    assert res_total.shape[0] == c.NUMBER_OF_GOALS
    assert res_total.shape[1] == len(next_gens_np)
    res_total_deperm = np.zeros((c.NUMBER_OF_GOALS, len(next_gens_np)))

    # res_total_deperm は逆並び替えによって並び替えを明示的に除去したものである。
    # これをプロットした ga_plot/plot_all_generation_{i}.png は本来の値を持ったプロットであることに注意する。
    for i in range(c.NUMBER_OF_GOALS):
        perm = stub_noise.get_permutation(noise_model.noise_query_pb2.PB_NoiseQuery(index=i)).data
        res_total_deperm[i] = remove_permutation_from_evaluation(res_total[i], perm)

    ############################ END OF DEPERM ######################################
    
    # ga_plot/plot_{i}.png に予測された値の各世代での変遷をプロットする
    # このプロットに関して、y0, y1 はそれぞれ別々の並び替えを施されているので、本来の値とは値が異なることに注意する
    plot_utils.plot_predicted_feature_and_targets(res_total, loop_index)

    # クライアントは線形回帰で推定された物性値と、目標値の差を計算する
    diff = _calculate_diff_btw_predicted_result_and_target(res_total=res_total)
    current_diff = np.average(np.abs(np.array(diff)), axis=1)
    print(f"current_diff: {current_diff}")

    # 差をGAサービスへと送信する
    _send_diff_to_ga_service(diff_goals=diff, stub=stub)

    # 遺伝アルゴリズムの対象となる親（現在の世代）をGAサービスへと送信する
    _send_parents_to_ga_service(next_gens_np, stub, is_need_denoise)

    ## 目標値との距離をflattenし、GAサービスへ送信する。GAサービスはこの情報をもとに、暗号化された親データからエリートをピックアップする。
    next_gens_np = _request_next_generation(stub)

    ############################ DENOISE ######################################
    ##　GAにおけるノイズを取得し、ノイズを直接除去する
    noise = np.array(stub_noise.get_noise_all(noise_model.noise_query_pb2.PB_NoiseQuery(index=0)).data)
    assert noise.shape[0] == next_gens_np.shape[0] * next_gens_np.shape[1], f"{noise.shape[0]} != {next_gens_np.shape[0] * next_gens_np.shape[1]}, shape: {noise.shape}, {next_gens_np.shape}"
    noise = noise.reshape(next_gens_np.shape[0], next_gens_np.shape[1])
    assert noise.shape[0] == next_gens_np.shape[0]
    assert noise.shape[1] == next_gens_np.shape[1]
    next_gens_np_denoised = next_gens_np - noise

    ############################ END OF DENOISE ######################################

    return next_gens_np, current_diff, res_total_deperm, next_gens_np_denoised


def save_all_generations_data(xs):
    with open(f"{c.GA_RESULT_FOLDER_NAME}/all_generations_denoised.pkl", "wb") as f:
        pickle.dump(xs, f)

def save_all_generations_result(xs):
    with open(f"{c.GA_RESULT_FOLDER_NAME}/all_prediction_results_depermed.pkl", "wb") as f:
        pickle.dump(xs, f)

def plot_all_generations_in_y(xs):
    zeroth_generation_results_depermed = np.array(xs[:1])
    all_children_results_depermed = np.array(xs[1:])

    zeroth_generation_results_depermed = np.transpose(zeroth_generation_results_depermed, (0, 2, 1))
    all_children_results_depermed = np.transpose(all_children_results_depermed, (0, 2, 1))
    plot_utils.plot_all_generations_in_y(zeroth_generation_results_depermed, all_children_results_depermed)


def load_all_generations_data():
    with open(f"{c.GA_RESULT_FOLDER_NAME}/all_generations_denoised.pkl", "rb") as f:
        all_generations_denoised = pickle.load(f)
    with open(f"{c.GA_RESULT_FOLDER_NAME}/all_prediction_results_depermed.pkl", "rb") as f:
        all_prediction_results_depermed = pickle.load(f)
    
    # データ、結果共に全ての子世代を選択
    zeroth_generation_results_depermed = np.array(all_prediction_results_depermed[:1])
    all_children_results_depermed = np.array(all_prediction_results_depermed[1:])

    zeroth_generation_results_depermed = np.transpose(zeroth_generation_results_depermed, (0, 2, 1))
    all_children_results_depermed = np.transpose(all_children_results_depermed, (0, 2, 1))

    assert len(all_generations_denoised) == len(all_children_results_depermed) + 1, f"{len(all_generations_denoised)} != {len(all_children_results_depermed)}"

    return np.array(all_generations_denoised), np.array(all_children_results_depermed)


if __name__ == "__main__":
    xs = np.random.rand(10, 4)
    ys = np.random.rand(10, 2)
    print(xs.shape)
    print(ys.shape)

    xs_chosen, ys_chosen = choose_data_close_to_target(xs, ys, 5) 
    print(xs_chosen.shape)
    print(ys_chosen.shape)
    