import grpc
import pickle
import random
import sys
import time

sys.path.append("../")

from sklearn import datasets

from grpc_python import input_client_pb2_grpc
from grpc_python import model_client_pb2_grpc

from grpc_python.model import message_pb2
from grpc_python.model import randomForestModel_pb2
from grpc_python.model import treeParameter_pb2
from grpc_python.model import data_pb2

import network_utils

if __name__ == "__main__":
    print("hello, world")

    api_input_client = network_utils.get_input_client_stub()

    api_model_client = network_utils.get_model_client_stub()

    with open("rf_iris.pkl", "rb") as f:
      model = pickle.load(f)

    iris = datasets.load_iris()
    xs, ys = iris.data, iris.target
    for i in range(10):
      rand = random.randint(0, len(xs)-1)

      t1_cipher = time.time()
      pb_for_predict_random_forest = data_pb2.PB_Data()
      pb_for_predict_random_forest.data = pickle.dumps(xs[rand])
      res = api_input_client.predict_random_forest(pb_for_predict_random_forest)
      t2_cipher = time.time()

      t1_raw = time.time()
      raw_res = model.predict(xs[rand:rand+1])
      t2_raw = time.time()

      print("\n")
      print(f"cipher_prediction_time: {t2_cipher-t1_cipher}")
      print(f"raw_prediction_time   : {t2_raw-t1_raw}")
      print(f"cipher_res: {pickle.loads(res.data)}")
      print(f"raw_res   : {raw_res[0]}")
      print(f"y_res     : {ys[rand]}")

