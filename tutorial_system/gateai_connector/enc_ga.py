#!/usr/bin/python
# -*- coding: utf-8 -*-

from tqdm import tqdm
from re import L, X
from multiprocessing.spawn import prepare
import sys
from tkinter import Y


import grpc
import numpy as np

import pytest
from math import isclose
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error

from system.test_random_ga.plain_chem.test_pygad_lir_without_index import fitness_models

sys.path.append("../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lssvr import ENC_LSSVR
import constants_client
from grpc_service import input_client_pb2_grpc
from grpc_service import common_model
from grpc_service import noise_service_pb2_grpc
from grpc_service import svr_model


class enc_fitness_model:
    def __init__(self, enc_preprocess, enc_lssvr):
        self.enc_preprocess = enc_preprocess
        self.enc_lssvr = enc_lssvr

    def set(self, scaler_path, model_path, goal, weight, fit_alg):
        self.scaler_path = scaler_path
        self.model_path = model_path
        self.goal = goal
        self.weight = weight
        if self.fit_alg == "LSSVR":
            self.fit_alg = fit_alg
        elif self.fit_alg == "LIR":
            self.fit_alg = fit_alg
        else:
            return ValueError(f"Not include {fit_alg}. Current support LSSVR and LIR.")

    def predict(self):
        self.enc_preprocess.load_minmax_scaler_on_server(self.scaler_path)
        self.enc_lssvr.load_model_on_server(self.model_path)
        self.enc_preprocess.minmax_scaler_transform_x_test()
        self.enc_preprocess.send_svr_only_test()
        return self.enc_lssvr.predict_already_send()

    def score(self):
        pred = self.predict()
        return self.weight * np.sum(np.abs(self.goal - pred))


# class enc_fitness_models:
#     def __init__(self, enc_preprocess, enc_lssvr):
#         self.enc_fitness_models = None
#         self.all_data_scaler_path = None
#         self.enc_preprocess = enc_preprocess
#         self.enc_lssvr = enc_lssvr

#     def set_enc_fitness_models(model_paths, all_data_scalar_path):
#         for fm in enc_fitness_models:

#         pass

#     def gen_all_x_minmax(self):
#         self.enc_preprocess.move_x_train_aler_on_server(self.all_data_scaler_path)

#     def debug_transfer_x_for_predict(self, x):
#         self.enc_preprocess.transfer_x_for_predict(x)

#     def score(self):
#         distance = 0
#         for ef in self.enc_fitness_models:
#             distance += ef.score()
#         return 1 / distance


# class ENC_GA:
#     def __init__(self, enc_preprocess, enc_lssvr):
#         channel_input_client = grpc.insecure_channel(
#             f"{constants_client.INPUT_CLIENT_HOST}:{constants_client.INPUT_CLIENT_PORT}",
#             options=[
#                 ("grpc.max_message_length", constants_client.GRPC_MAX_MESSAGE_LENGTH),
#                 ("grpc.max_receive_message_length", constants_client.GRPC_MAX_MESSAGE_LENGTH),
#             ],
#         )
#         self.stub = input_client_pb2_grpc.InputClientStub(channel_input_client)
#         self.enc_fitness_models = None
#         self.enc_preprocess = enc_preprocess
#         self.enc_lssvr = enc_lssvr

#     def set_enc_fitness_models(self, model_paths, all_data_scaler_path):
#         self.enc_fitness_models = enc_fitness_models(model_paths, all_data_scaler_path)

#     def first_genes(self):
#         pass
