import os
import configparser
from os.path import join as osp


######################################## customizable
## BASIC SETTINGS
ROOT_FOLDER = "/cf_saas"  # change here as your root directory path for this repository
if ROOT_FOLDER is None:
    raise Exception("please set path to root directory of this repository")

IS_FIXING_RANDOM_SEED = True
RANDOM_SEED = 123


LEVEL1_CKKS_SCALE_BIT = 40
LEVEL1_CKKS_SCALE = pow(2.0, LEVEL1_CKKS_SCALE_BIT)
LEVEL1_CKKS_MULTIPLICATION_CAPABILITY = 1
LEVEL1_CKKS_MODULUS_CHAIN = [60, 40, 60]
LEVEL1_CKKS_POLY_MODULUS_DEGREE_BIT = 13
LEVEL1_CKKS_POLY_MODULUS_DEGREE = pow(2, LEVEL1_CKKS_POLY_MODULUS_DEGREE_BIT)

LEVEL2_CKKS_SCALE_BIT = 40
LEVEL2_CKKS_SCALE = pow(2.0, LEVEL2_CKKS_SCALE_BIT)
LEVEL2_CKKS_MULTIPLICATION_CAPABILITY = 2
LEVEL2_CKKS_MODULUS_CHAIN = [60, 40, 40, 60]
LEVEL2_CKKS_POLY_MODULUS_DEGREE_BIT = 13
LEVEL2_CKKS_POLY_MODULUS_DEGREE = pow(2, LEVEL2_CKKS_POLY_MODULUS_DEGREE_BIT)

LEVEL3_CKKS_SCALE_BIT = 40
LEVEL3_CKKS_SCALE = pow(2.0, LEVEL3_CKKS_SCALE_BIT)
LEVEL3_CKKS_MULTIPLICATION_CAPABILITY = 1
LEVEL3_CKKS_MODULUS_CHAIN = [60, 40, 60]
LEVEL3_CKKS_POLY_MODULUS_DEGREE_BIT = 13
LEVEL3_CKKS_POLY_MODULUS_DEGREE = pow(2, LEVEL3_CKKS_POLY_MODULUS_DEGREE_BIT)

LEVEL3_CKKS_SCALE_BIT = 40
LEVEL3_CKKS_SCALE = pow(2.0, LEVEL3_CKKS_SCALE_BIT)
LEVEL3_CKKS_MULTIPLICATION_CAPABILITY = 3
LEVEL3_CKKS_MODULUS_CHAIN = [60, 40, 40, 40, 60]
LEVEL3_CKKS_POLY_MODULUS_DEGREE_BIT = 14
LEVEL3_CKKS_POLY_MODULUS_DEGREE = pow(2, LEVEL3_CKKS_POLY_MODULUS_DEGREE_BIT)


###########################################  customizable
## SERVER SETTINGS
GRPC_MAX_MESSAGE_LENGTH = 2020000000  # max datasize that can be on grpc communication

INPUT_CLIENT_HOST = "0.0.0.0"
INPUT_CLIENT_PORT = 5101
INPUT_CLIENT_MAX_WORKER = 3

LIR_HOST = ["localhost", "localhost"]
LIR_PORT = [5110, 5111]
LIR_MAX_WORKER = 3

GA_HOST = "localhost"
GA_PORT = 5103
GA_MAX_WORKER = 3

KMEANS_HOST = "localhost"
KMEANS_PORT = 5104
KMEANS_MAX_WORKER = 3

LOR_HOST = "localhost"
LOR_PORT = 5105
LOR_MAX_WORKER = 3

SVR_HOST = "localhost"
SVR_PORT = 5106
SVR_MAX_WORKER = 3

# NOISE SERVICE CONFIG
NOISE_HOST = "localhost"
NOISE_PORT = 5107
NOISE_MAX_WORKER = 3

TREE_SERVICE_HOST = "localhost"
TREE_SERVICE_PORT = 5108
TREE_SERVICE_MAX_WORKER = 3

PREPROCESS_HOST = "localhost"
PREPROCESS_PORT = 5109
PREPROCESS_MAX_WORKER = 3


STORAGE_HOST = "localhost"
STORAGE_PORT = 5110
STORAGE_MAX_WORKER = 3

############################################ non customizable
## LICENSE
PRODUCT_ID = 2
DEVELOPMENT_VERSION = "1.3.0"


########################################### non customizable
## DEFALUT FOLDER SETTINGS
# KEY FOLDERS FOR EACH SERVER

ENC_SERVICE_FOLDER_FOR_INPUT_CLIENT = osp(ROOT_FOLDER, "datas/input_client/keyfiles/")
ENC_SERVICE_FOLDER_FOR_GA_CLIENT = osp(ROOT_FOLDER, "datas/genetic_algorithm/keyfiles/")
ENC_SERVICE_FOLDER_FOR_HC_CLIENT = osp(ROOT_FOLDER, "datas/hierarchical_clustering/keyfiles/")
INTERMEDIATE_RESULT_FOLDER_FOR_COMPARE = osp(ROOT_FOLDER, "modelfiles/intermediate_compare")


def load_config_from_env():
    ######################################## customizable
    ## BASIC SETTINGS
    global ROOT_FOLDER
    ROOT_FOLDER = os.getenv("ROOT_FOLDER", "/cf_saas")

    global USE_SSL_TO_CALCULATION
    USE_SSL_TO_CALCULATION = os.getenv("USE_SSL_TO_CALCULATION", "false") == "true"

    global USE_SSL_TO_CALCULATION_WITH_KEY
    USE_SSL_TO_CALCULATION_WITH_KEY = os.getenv("USE_SSL_TO_CALCULATION_WITH_KEY", "false") == "true"

    ## INPUT CLIENT CONFIG
    global INPUT_CLIENT_HOST
    INPUT_CLIENT_HOST = os.getenv("INPUT_CLIENT_HOST", "input_client")

    global INPUT_CLIENT_PORT
    INPUT_CLIENT_PORT = int(os.getenv("INPUT_CLIENT_PORT", "5101"))

    global INPUT_CLIENT_MAX_WORKER
    INPUT_CLIENT_MAX_WORKER = int(os.getenv("INPUT_CLIENT_MAX_WORKER", "3"))

    ## LIR SERVER CONFIG
    global LIR_HOST
    LIR_HOST = list(eval(os.getenv("LIR_HOST", '["lir1", "lir2"]')))

    global LIR_PORT
    LIR_PORT = list(eval(os.getenv("LIR_PORT", "[5110, 5111]")))

    global LIR_MAX_WORKER
    LIR_MAX_WORKER = int(os.getenv("LIR_MAX_WORKER", "3"))

    ## GA CONFIG
    global GA_HOST
    GA_HOST = os.getenv("GA_HOST", "ga")

    global GA_PORT
    GA_PORT = int(os.getenv("GA_PORT", "5103"))

    global GA_MAX_WORKER
    GA_MAX_WORKER = int(os.getenv("GA_MAX_WORKER", "3"))

    ## KMEANS CONFIG
    global KMEANS_HOST
    KMEANS_HOST = os.getenv("KMEANS_HOST", "kmeans")

    global KMEANS_PORT
    KMEANS_PORT = int(os.getenv("KMEANS_PORT", "5104"))

    global KMEANS_MAX_WORKER
    KMEANS_MAX_WORKER = int(os.getenv("KMEANS_MAX_WORKER", "3"))

    ## NOISE CONFIG
    global NOISE_HOST
    NOISE_HOST = os.getenv("NOISE_HOST", "noise")

    global NOISE_PORT
    NOISE_PORT = int(os.getenv("NOISE_PORT", "5107"))

    global NOISE_MAX_WORKER
    NOISE_MAX_WORKER = int(os.getenv("NOISE_MAX_WORKER", "3"))

    global IS_FIXING_RANDOM_SEED
    IS_FIXING_RANDOM_SEED = bool(os.getenv("IS_FIXING_RANDOM_SEED", "False"))

    global RANDOM_SEED
    RANDOM_SEED = int(os.getenv("RANDOM_SEED", "123"))

    ## SVR CONFIG
    global SVR_HOST
    SVR_HOST = os.getenv("SVR_HOST", "svr")

    global SVR_PORT
    SVR_PORT = int(os.getenv("SVR_PORT", "5103"))

    global SVR_MAX_WORKER
    SVR_MAX_WORKER = int(os.getenv("SVR_MAX_WORKER", "3"))

    ## PREPROCESS CONFIG
    global PREPROCESS_HOST
    PREPROCESS_HOST = os.getenv("PREPROCESS_HOST", "preprocess")

    global PREPROCESS_PORT
    PREPROCESS_PORT = int(os.getenv("PREPROCESS_PORT", "5109"))

    global PREPROCESS_MAX_WORKER
    PREPROCESS_MAX_WORKER = int(os.getenv("PREPROCESS_MAX_WORKER", "3"))

    ## PREPROCESS CONFIG
    global STORAGE_HOST
    STORAGE_HOST = os.getenv("STORAGE_HOST", "STORAGE")

    global STORAGE_PORT
    STORAGE_PORT = int(os.getenv("STORAGE_PORT", "5109"))

    global STORAGE_MAX_WORKER
    STORAGE_MAX_WORKER = int(os.getenv("STORAGE_MAX_WORKER", "3"))


def load_config():

    if os.getenv("USE_ENV") is not None:
        load_config_from_env()


load_config()


SSL_FOLDER_CALCULATION = osp(ROOT_FOLDER, "config/ssl/calculation")
SSL_FOLDER_CALCULATION_WITH_KEY = osp(ROOT_FOLDER, "config/ssl/calculation_with_key")

## DEFALUT FOLDER SETTINGS
# KEY FOLDERS FOR EACH SERVER
ENC_SERVICE_FOLDER_FOR_CALC_WITH_KEY = osp(ROOT_FOLDER, "datas/calculation_with_key/keyfiles/")
ENC_SERVICE_FOLDER_FOR_CALC_NO_KEY = osp(ROOT_FOLDER, "datas/calculation/keyfiles/")
ENC_SERVICE_FOLDER_FOR_MODEL_CLIENT = osp(ROOT_FOLDER, "datas/model_client/keyfiles/")
ENC_SERVICE_FOLDER_FOR_INPUT_CLIENT = osp(ROOT_FOLDER, "datas/input_client/keyfiles/")
INTERMEDIATE_RESULT_FOLDER_FOR_COMPARE = osp(ROOT_FOLDER, "modelfiles/intermediate_compare")
