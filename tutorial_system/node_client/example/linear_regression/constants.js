const attrs = 4
const bs = 5
const ds = 100
const ss = 1 << 13
const eta = 0.02
const update_option = "none"

export { attrs, bs, ds, ss, eta, update_option }