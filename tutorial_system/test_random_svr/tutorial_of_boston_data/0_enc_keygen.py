import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston

sys.path.append("../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lssvr import ENC_LSSVR

if __name__ == "__main__":
    preprocess = ENC_PREPROCESS()
    preprocess.keygen()
    preprocess.save_key_on_server("tutorial_of_boston_data")
    
    lssvr = ENC_LSSVR()
    lssvr.keygen()
    lssvr.save_key_on_server("tutorial_of_boston_data")