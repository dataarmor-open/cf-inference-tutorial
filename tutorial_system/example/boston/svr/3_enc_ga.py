import sys
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_regression


sys.path.append("../../../")
from gateai_connector.enc_lssvr import ENC_LSSVR
from gateai_connector.enc_preprocess import ENC_PREPROCESS

from gateai_connector.enc_ga import enc_fitness_model, enc_fitness_models, ENC_GA
from sklearn.datasets import load_boston

if __name__ == "__main__":
    epsilon = 0.1
    C = 10
    preprocess = ENC_PREPROCESS()
    preprocess.load_key_on_server("tutorial_of_boston_data")
    lssvr = ENC_LSSVR()
    lssvr.load_key_on_server("tutorial_of_boston_data")
    preprocess.load_input_x_on_server("tutorial_of_boston_data")
    
    ga = ENC_GA()
    efms = enc_fitness_models(preprocess, lssvr)
    ga.keygen()
    efms.set_enc_fitness_models(
        "tutorial_of_boston_data",
        ["tutorial_of_boston_data"],
        ["boston_standard_scaler"],
        ["boston_lssvr_model"],
        [30],
        [1],
        ["standard"],
        ["LSSVR"],
    )
    efms.gen_all_x_minmax()
    # efms.load_all_x_minmax()
    ga.set_enc_fitness_models(efms)
    ga.ga_send_minmax_from_preprocess()
    ga.set_config(100, 10, 70, 1, 1, 3)
    ga.run()
