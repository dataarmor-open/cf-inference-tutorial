
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



# ãã¼ã¿ãã¬ã¼ã ããè·é¢ãè¨ç®
def calc_distance(dataframe):
    distance = 0
    for col in range(len(dataframe.columns)):   # åã®æ°ã ãç¹°ãè¿ã
        distance += dataframe[col]**2
    distance = np.sqrt(distance) # ã¦ã¼ã¯ãªããè·é¢ãè¨ç®
    return distance

# ãã¼ã¿ãã¬ã¼ã ã®çµå
def merge_dataframe(origin, add):
    origin = pd.concat([origin, add], axis=1)
    return origin

# æç³»åãã¼ã¿ã¨ãã¦ã°ã©ããä½æ
def plot_log(distance, MaxGeneration):
    y_mean = np.array(distance.mean())      # ãã¼ã¿ã®å¹³åå¤ãåå¾
    y_median = np.array(distance.median())  # ä¸­å¤®å¤ãåå¾
    y_min = np.array(distance.min()) # æå°å¤ãåå¾
    y_max = np.array(distance.max()) # æå¤§å¤ãåå¾
    x = np.arange(len(y_mean))
    xicks = np.arange(0, MaxGeneration+1, MaxGeneration/10)   # MaxGenerationãã¡ã¢ãªã«è¡¨ç¤ºãããããã«+1
    plt.plot(x, y_mean, label="mean", color="green")
    plt.plot(x, y_median,label="median", color="blue")
    plt.plot(x, y_min, label="min", color="red")
    plt.plot(x, y_max, label="max", color="cyan")
    plt.xlabel("$Generation$", fontsize=15)
    plt.ylabel("$distance$", fontsize=15)
    plt.xlim(xmin=0)
    plt.ylim(ymin=0)
    plt.grid(True)
    plt.xticks(xicks)
    plt.legend()
    plt.savefig("analysis_gene.jpg")



MaxGeneration = 150

for G in range(MaxGeneration):
    if G==0:
        gene_path = "./Out/" + str(G) + ".gene"
        dataframe_0 = pd.read_csv(gene_path, header=None)  # ãã¼ã¿ã®èª­ã¿åã
        distance_0 = calc_distance(dataframe_0)            # ãã¼ã¿ãã¬ã¼ã ããè·é¢ãè¨ç®
    else:
        gene_path = "./Out/" + str(G) + ".gene"
        dataframe = pd.read_csv(gene_path, header=None)
        distance = calc_distance(dataframe)
        distance_0 = merge_dataframe(distance_0, distance)

plot_log(distance_0, MaxGeneration)


