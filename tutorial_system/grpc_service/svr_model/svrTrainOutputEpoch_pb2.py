# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: svr_model/svrTrainOutputEpoch.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n#svr_model/svrTrainOutputEpoch.proto\"&\n\x16PB_svrTrainOutputEpoch\x12\x0c\n\x04\x62\x65ta\x18\x02 \x03(\x01\x62\x06proto3')



_PB_SVRTRAINOUTPUTEPOCH = DESCRIPTOR.message_types_by_name['PB_svrTrainOutputEpoch']
PB_svrTrainOutputEpoch = _reflection.GeneratedProtocolMessageType('PB_svrTrainOutputEpoch', (_message.Message,), {
  'DESCRIPTOR' : _PB_SVRTRAINOUTPUTEPOCH,
  '__module__' : 'svr_model.svrTrainOutputEpoch_pb2'
  # @@protoc_insertion_point(class_scope:PB_svrTrainOutputEpoch)
  })
_sym_db.RegisterMessage(PB_svrTrainOutputEpoch)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PB_SVRTRAINOUTPUTEPOCH._serialized_start=39
  _PB_SVRTRAINOUTPUTEPOCH._serialized_end=77
# @@protoc_insertion_point(module_scope)
