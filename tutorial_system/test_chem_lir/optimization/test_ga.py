import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
import numpy as np
import time


from sklearn.decomposition import PCA

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc


from optimization import ga_process
from optimization import plot_utils

import constants_client as c

from optimization.test_set_config import set_config_lir, set_config_kmeans


def main():
    colors = plot_utils.get_colors()
    options = [('grpc.max_message_length', c.GRPC_MAX_MESSAGE_LENGTH),('grpc.max_receive_message_length', c.GRPC_MAX_MESSAGE_LENGTH)]

    ## input_client への接続を確立
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    ## noise サービスへの接続を確立(チュートリアルのために接続、普通は接続を制限している)
    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)


    # 遺伝アルゴリズム最適化スタート====================================================

    # 第一世代のみlir_and_ga にはファイルからロードしたデータを使用する（もしくは自分でランダム生成する）
    # lir_and_ga の is_need_denoise はFalseに設定する（ロードしたデータにはノイズが含まれていない)
    # 最適化するデータをロード
    #xs, ys, w, b = data.load_data()
    xs = np.random.randn(c.GA_DATA_SIZE, c.ATTRS)

    # サービスの利用自体には必要ないが、プロットするためにPCAを行う
    pca = PCA(n_components=2)
    pca = pca.fit(xs)


    # 遺伝アルゴリズムのメインループ(c.ITERATION は世代の数)
    all_generations_denoised = []
    all_prediction_results_depermed = []
    res_diffs = []
    for i in range(0, c.ITERATION):
        t1 = time.time()
        # 第一世代から次世代を作るときは、データを使用する
        if i == 0:
            set_config_lir(stub, len(xs), 0)
            set_config_lir(stub, len(xs), 1)
        # 第二世代以降から次世代を作るときは、前世代のデータを使用する
        else:
            set_config_lir(stub, len(next_gens_np), 0)
            set_config_lir(stub, len(next_gens_np), 1)
        # lir_and_ga の is_need_denoise はTrueに設定する（GAサービスから生成された次世代データにはノイズが含まれているため、LIRサービスは推論する前に暗号状態でノイズを取り除く必要がある)
        if i == 0:
            next_gens_np, current_diff, res_total_depermed, next_gens_np_denoised = ga_process.lir_and_ga_deperm_denoise(xs, i, stub, stub_noise, False)
        else:
            next_gens_np, current_diff, res_total_depermed, next_gens_np_denoised = ga_process.lir_and_ga_deperm_denoise(next_gens_np, i, stub, stub_noise, True)
        res_diffs.append(current_diff)
        all_generations_denoised.append(next_gens_np_denoised)
        all_prediction_results_depermed.append(res_total_depermed)


        t2 = time.time()
        print(f"\n\nloop for lir_and_ga {i} / {c.ITERATION} done: time = {t2-t1}")
    
    # all_generations_denoised = ((全ての子世代), 各世代の数, ATTRS)
    # all_prediction_results_depermed = ((最初の世代＋全ての子世代), 2, 各世代の数)
    ga_process.save_all_generations_data(all_generations_denoised)
    ga_process.save_all_generations_result(all_prediction_results_depermed)
    ga_process.plot_all_generations_in_y(all_prediction_results_depermed)

    # 遺伝アルゴリズム最適化スタート====================================================(終)



if __name__ == "__main__":
    print("hello, world")
    main()