import random
import matplotlib.pyplot as plt
import constants_client as c

def get_colors():
    ## プロットに使用する色の準備
    colors = []
    for i in range(1000):
        tmp = []
        for j in range(3):
            tmp.append(random.random())
        colors.append(tmp)
    return colors


def plot_scatter(res1, res2, name, is_color_change):
    ax = plt.subplot()
    ax.set_ylim(c.PLOT_MIN_Y, c.PLOT_MAX_Y)
    ax.set_xlim(c.PLOT_MIN_X, c.PLOT_MAX_X)
    colors = get_colors()
    for i in range(len(res1)):
        if is_color_change:
            ax.scatter(res1[i], res2[i], color=colors[i])
        else:
            ax.scatter(res1[i], res2[i], color=colors[0])
    ax.set_xlabel("PC1")
    ax.set_ylabel("PC2")
    plt.savefig(f"{c.KMEANS_RESULT_FOLDER_NAME}/{name}.png")
    plt.close()


def plot_chosen_data(ys):
    ax = plt.subplot()
    ax.set_ylim(c.PLOT_MIN_Y, c.PLOT_MAX_Y)
    ax.set_xlim(c.PLOT_MIN_X, c.PLOT_MAX_X)
    colors = get_colors()
    for i in range(len(ys)):
        ax.scatter(ys[i][0], ys[i][1], color=colors[0])
    ax.set_xlabel("PC1")
    ax.set_ylabel("PC2")
    plt.savefig(f"{c.GA_RESULT_FOLDER_NAME}/plot_chosen.png")
    plt.close()


def plot_all_generations_in_y(zeroth_prediction_results_depermed, all_prediction_results_depermed):
    colors = get_colors()
    ax = plt.subplot()
    ax.set_xlabel("PC1")
    ax.set_ylabel("PC2")
    ax.set_ylim(c.PLOT_MIN_Y, c.PLOT_MAX_Y)
    ax.set_xlim(c.PLOT_MIN_X, c.PLOT_MAX_X)
    ax.scatter(c.GOAL[0], c.GOAL[1], color=(0,0,1), s=c.SCATTER_SIZE)
    res = zeroth_prediction_results_depermed[0] 
    for j in range(len(res)):
        ax.scatter(res[j][0], res[j][1], color=colors[0], s=c.SCATTER_SIZE)
    plt.savefig(f"{c.GA_RESULT_FOLDER_NAME}/plot_all_generation_0.png")

    for i in range(len(all_prediction_results_depermed)):
        res = all_prediction_results_depermed[i]
        ax.scatter(c.GOAL[0], c.GOAL[1], color=(0,0,1), s=c.SCATTER_SIZE)
        for j in range(len(res)):
            ax.scatter(res[j][0], res[j][1], color=colors[i+1], s=c.SCATTER_SIZE)
        plt.savefig(f"{c.GA_RESULT_FOLDER_NAME}/plot_all_generation_{i+1}.png")
    plt.close()

def plot_predicted_feature_and_targets(res, i):
    ax = plt.subplot()
    ax.scatter(c.GOAL[0], c.GOAL[1], color=(0,0,1), s=c.SCATTER_SIZE)
    ax.scatter(res[0], res[1], color=(1,0,0), s=c.SCATTER_SIZE)
    ax.set_ylim(c.PLOT_MIN_Y, c.PLOT_MAX_Y)
    ax.set_xlim(c.PLOT_MIN_X, c.PLOT_MAX_X)
    ax.set_xlabel("PC1")
    ax.set_ylabel("PC2")
    plt.savefig(f"{c.GA_RESULT_FOLDER_NAME}/plot_{i}.png")
    plt.close()


def plot_data_with_cluster_with_pca(next_gens_wo_noise, ks, pca, colors):
    # クラスタリング対象となるデータ(next_gens_np)にはノイズが含まれているため、ノイズサービスにアクセスして、ノイズを除去してからプロットしている
    # あくまでチュートリアルのためにノイズサービスにアクセスしており、普通はクライアントがノイズサービスへはアクセスできない。

    next_gens_pca = pca.transform(next_gens_wo_noise)

    ax = plt.subplot()
    for i in range(len(ks)):
        #print(f"{dec_ks[i][0]}, {dec_ks[i][1]}, {colors[i]}")
        ax.scatter(ks[i][0], ks[i][1], color=colors[i], marker="x", s=100)
    
    for i in range(len(next_gens_wo_noise)):
        ax.scatter(next_gens_pca[i][0], next_gens_pca[i][1], color="b")
    ax.set_ylim(-2,2)
    ax.set_xlim(-2,2)
    plt.savefig(f"{c.KMEANS_RESULT_FOLDER_NAME}/initial.png")
    plt.close()
