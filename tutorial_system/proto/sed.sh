for filename in *.py; do
  sed -i -e "s/^from common_model/from grpc_service.common_model/" $filename
  sed -i -e "s/^from lir_model/from grpc_service.lir_model/" $filename
  sed -i -e "s/^from lor_model/from grpc_service.lor_model/" $filename
  sed -i -e "s/^from tree_model/from grpc_service.tree_model/" $filename
  sed -i -e "s/^from svr_model/from grpc_service.svr_model/" $filename
  sed -i -e "s/^from ga_model/from grpc_service.ga_model/" $filename
  sed -i -e "s/^from kmeans_model/from grpc_service.kmeans_model/" $filename
  sed -i -e "s/^from noise_model/from grpc_service.noise_model/" $filename
done

for filename in common_model/*.py; do
  sed -i -e "s/^from common_model/from grpc_service.common_model/" $filename
done

for filename in lir_model/*.py; do
  sed -i -e "s/^from lir_model/from grpc_service.lir_model/" $filename
done

for filename in lor_model/*.py; do
  sed -i -e "s/^from lor_model/from grpc_service.lor_model/" $filename
done

for filename in tree_model/*.py; do
  sed -i -e "s/^from tree_model/from grpc_service.tree_model/" $filename
done

for filename in svr_model/*.py; do
  sed -i -e "s/^from svr_model/from grpc_service.svr_model/" $filename
done

for filename in ga_model/*.py; do
  sed -i -e "s/^from ga_model/from grpc_service.ga_model/" $filename
done

for filename in kmeans_model/*.py; do
  sed -i -e "s/^from kmeans_model/from grpc_service.kmeans_model/" $filename
done

for filename in noise_model/*.py; do
  sed -i -e "s/^from noise_model/from grpc_service.noise_model/" $filename
done
