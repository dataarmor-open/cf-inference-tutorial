import sys
sys.path.append("../")
sys.path.append("../../")

import grpc
import random
import numpy as np
import time
import json
import pickle


from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

from grpc_service import input_client_pb2_grpc
from grpc_service import noise_service_pb2_grpc

from grpc_service import common_model
from grpc_service import noise_model
from grpc_service import kmeans_model

import data
from optimization import ga_process
from optimization import kmeans_process
from optimization import plot_utils

import constants_client as c

from optimization.test_set_config import set_config_lir, set_config_kmeans
from optimization.test_upload import update_lir_model


def main():
    colors = plot_utils.get_colors()
    options = [('grpc.max_message_length', c.GRPC_MAX_MESSAGE_LENGTH),('grpc.max_receive_message_length', c.GRPC_MAX_MESSAGE_LENGTH)]

    ## input_client への接続を確立
    channel_input_client = grpc.insecure_channel(f'{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}', options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)

    ## noise サービスへの接続を確立(チュートリアルのために接続、普通は接続を制限している)
    channel_noise_service = grpc.insecure_channel(f'{c.NOISE_HOST}:{c.NOISE_PORT}', options=[])
    stub_noise = noise_service_pb2_grpc.NoiseServiceStub(channel_noise_service)


    # 遺伝アルゴリズム最適化スタート====================================================
    k = c.K
    attrs = c.ATTRS

    # 第一世代のみlir_and_ga にはファイルからロードしたデータを使用する（もしくは自分でランダム生成する）
    # lir_and_ga の is_need_denoise はFalseに設定する（ロードしたデータにはノイズが含まれていない)
    # 最適化するデータをロード
    xs, ys1, ys2 = data.get_data()
    # サービスの利用自体には必要ないが、プロットするためにPCAを行う
    pca = PCA(n_components=2)
    pca = pca.fit(xs)


    # KMEANS START====================================================

    all_children_denoised, all_children_results_depermed = ga_process.load_all_generations_data()

    all_children_denoised = all_children_denoised.reshape(-1, all_children_denoised.shape[2])
    all_children_results_depermed = all_children_results_depermed.reshape(-1, all_children_results_depermed.shape[2])

    if c.IS_CHOOSE_FROM_CONDITION:
        chosen_data, chosen_ys = ga_process.choose_data_with_condition(all_children_denoised[:len(all_children_results_depermed)], all_children_results_depermed)
        if len(chosen_data) == 0:
            raise Exception("len(chosen_data) == 0, please check c.CHOOSE_Y1_MIN, c.CHOOSE_Y1_MAX, c.CHOOSE_Y2_MIN, c.CHOOSE_Y2_MAX")
    else:
        chosen_data, chosen_ys = ga_process.choose_data_close_to_target(all_children_denoised[:len(all_children_results_depermed)], all_children_results_depermed, c.NUM_CHOOSE_FROM_GENERATIONS)
    plot_utils.plot_chosen_data(chosen_ys)
    print(f"chosen_data.shape: {chosen_data.shape}")


    # KMEANS サービスへとコンフィグを送信
    set_config_kmeans(stub=stub, data_size=len(chosen_data))

    # KMEANS サービスへとクラスタリング対象データを送信
    pb_datas = common_model.doubleList_pb2.PB_DoubleList()
    pb_datas.data[:] = chosen_data.reshape(-1)
    pb_datas.size = len(chosen_data)
    pb_datas.is_preprocess_denoise = False
    stub.upload_data_for_kmeans_stream(pb_datas)

    # KMEANS サービスへk個の重心を送信する
    pb_ks = common_model.doubleList_pb2.PB_DoubleList()
    ks = np.random.rand(c.K, c.ATTRS)
    pb_ks.data[:] = ks.reshape(-1)
    stub.upload_ks_for_kmeans_stream(pb_ks)

    # チュートリアルのためにデータを２次元上にプロットする============================
    # 初めに、遺伝アルゴリズムの結果（次世代のデータ）にはノイズが加算されているため、ノイズを手動で除去する
    #next_gens_wo_noise = ga_process.remove_noise_from_generations(stub_noise, chosen_data, attrs)
    next_gens_wo_noise = chosen_data 
    # プロットする
    # plot_utils.plot_data_with_cluster_with_pca(next_gens_wo_noise, ks, pca, colors)
    # チュートリアルのためにデータを２次元上にプロットする============================(終)

    
    # Kmeans クラスタリングのメインループ
    # 重心を更新するステップをROUND と表記
    for loop in range(c.KMEANS_ROUND):
        print(f"kmeans loop: {loop}/{c.KMEANS_ROUND}============")
        t1_kmean_round = time.time()

        # KMEANSサービスに、クラスターの重心の更新をリクエスト
        kmeans_process.kmeans_main(stub)

        t2_kmean_round = time.time()
        print(f"\nkmean_round: {t2_kmean_round - t1_kmean_round}")


if __name__ == "__main__":
    print("hello, world")

    main()






