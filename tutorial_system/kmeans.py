import sys
import os
from tabnanny import check
sys.path.append("../")
import constants_client as c
import time
import itertools
import shutil
from tqdm import tqdm
import random
import numpy as np
import pickle

import optimization

from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

from grpc_service import common_model
from grpc_service import noise_model
from grpc_service import lir_model
from grpc_service import ga_model
from grpc_service import kmeans_model

from lir import LIR

if c.IS_FIXING_RANDOM_SEED:
    random.seed(c.RANDOM_SEED)
    np.random.seed(c.RANDOM_SEED)


class KMEANS(object):
    def __init__(self, stub, data_size, noise_stub=None, k=c.K, kmean_round=c.KMEANS_ROUND,
        attrs=c.ATTRS, kmeans_result_folder_name=c.KMEANS_RESULT_FOLDER_NAME):
        
        self.stub = stub
        self.noise_stub = noise_stub
        self.k = k
        self.attrs = attrs
        self.data_size = data_size
        self.round = kmean_round
        self.kmeans_result_folder_name=kmeans_result_folder_name
    
    def keygen(self):
        self.stub.gen_key_for_kmeans(common_model.message_pb2.PB_Message())
    
    def set_config(self, size=None):
        pb_config = kmeans_model.kmeansConfig_pb2.PB_KMeansConfig()
        pb_config.attrs = self.attrs
        pb_config.k = self.k
        pb_config.data_size = self.data_size
        self.stub.upload_config_for_kmeans(pb_config)
    
    
    def upload_datas(self, chosen_data):
        # KMEANS サービスへとクラスタリング対象データを送信
        pb_datas = common_model.doubleList_pb2.PB_DoubleList()
        pb_datas.data[:] = chosen_data.reshape(-1)
        pb_datas.size = len(chosen_data)
        pb_datas.is_preprocess_denoise = False
        self.stub.upload_data_for_kmeans_stream(pb_datas)
    
    def upload_initial_centroids(self):
        # KMEANS サービスへk個の重心を送信する
        pb_ks = common_model.doubleList_pb2.PB_DoubleList()
        ks = np.random.rand(self.k, self.attrs)
        pb_ks.data[:] = ks.reshape(-1)
        self.stub.upload_ks_for_kmeans_stream(pb_ks)

    
    def apply(self):
        # Kmeans クラスタリングのメインループ
        # 重心を更新するステップをROUND と表記
        for loop in range(self.round):
            print(f"kmeans loop: {loop}/{self.round}============")
            t1_kmean_round = time.time()

            # KMEANSサービスに、クラスターの重心の更新をリクエスト
            self.kmeans_main()

            t2_kmean_round = time.time()
            print(f"\nkmean_round: {t2_kmean_round - t1_kmean_round}")


    def kmeans_main(self):
        # 各データとクラスター重心の距離を計算（結果が並び替えられたものがresとして返却、input_clientで復号済み）
        pb = common_model.message_pb2.PB_Message()
        # KMEANSサービスに対して、各データと各クラスター重心との距離を並び替えて返却するか明記する
        pb.is_postprocess_perm = True
        res = self.stub.update_distance_for_kmeans_stream(pb)

        # 復号された距離情報を再暗号化して送信
        self.stub.upload_distance_for_kmeans(res)

        # 送信された距離データに基づきクラスター重心を更新
        pb = common_model.message_pb2.PB_Message()
        # KMEANSサービスに対して、各データと各クラスター重心との距離を逆並び替えしてからクラスター重心を更新するように明記
        pb.is_preprocess_deperm = True
        self.stub.update_ks_for_kmeans(pb)

    def get_cluster_centroids(self):
        # input_client から秘密鍵を取得
        res = stub.get_service_for_kmeans(common_model.message_pb2.PB_Message())
        service = CKKSEncryptionServiceWrapper.deserialize_encryption_service(serialized_obj=json.loads(res.ckks_enc_service_encoded), poly_modulus_degree=2**13, scale=2**40, include_galois_key=False)

        # input_client を経由し、kmeans サービスから暗号化されたクラスター重心データを取得
        res = self.stub.get_ks_for_kmeans(common_model.message_pb2.PB_Message())
        assert len(res.data) == self.k
        tmp = CKKSSerializeService.deserialize_ctxt_1d(res.data, service, True)

        # 秘密鍵を使ってクラスタリング重心データを復号
        dec_ks_tmp = service.decrypt_2d_as_coeff(tmp)
        dec_ks = []
        for i in range(len(dec_ks_tmp)):
            dec_ks.append(dec_ks_tmp[i][:self.attrs])

        
        # input_client を経由し、kmeans サービスからクラスター情報（どのデータがどのクラスターに属するか）を取得
        res = self.stub.get_cluster_for_kmeans(common_model.message_pb2.PB_Message())
        cluster = json.loads(res.message)

        return dec_ks, cluster

        