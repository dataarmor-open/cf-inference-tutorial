# LIR サービスの使用方法

このドキュメントでは、GateAIサービス群の内の一つ、

LIR サービスの使用方法を解説します。

用意された`tmp.csv`
もしくはご自身でカスタマイズされたデータセットに対して、
GateAIのLIRサービスを使って線形回帰モデルを学習します。


本チュートリアルは、

- LIRサービスを用いて暗号状態で学習したモデルの精度
- 学習速度

について検証できるチュートリアルとなっています。
 

## チュートリアルコードの実行

`constants_client.py`の設定を行った後、実際にチュートリアルコードの実行を行います。
また、このチュートリアルを実行するために必要なGateAIサービス群のうち、

- input_client
- lir

が立ち上がっていることをご確認ください。

確認は `docker ps`
にて確認可能です。


### 実行されるプログラムの解説

チュートリアルプログラムは、
`main.py`
を使用することで実行可能です。

```
python main.py
```

を行うことで、
- 線形回帰モデルの学習
- 遺伝アルゴリズムによる入力ベクトルの最適化
- 最適化されたデータに対するkmeans クラスタリング

が実行されます。

プログラムの詳細については、`main.py`を実際にご覧ください。


### GateAI を用いた学習の実行

main.py をご覧ください。

```
from lir import LIR
```
にて線形回帰モデルの学習に必要なLIRクラスをインポートしています。

LIRクラスの使い方を確認するために、LIRクラスについて記載します。

```
class LIR(object):
    def __init__(self, stub, model_index,
        attrs=c.ATTRS, bs=c.BS, ss=c.SS, epoch=c.LEARNING_EPOCH, eta=c.ETA, lmb=c.LMB, update_option=c.UPDATE_OPTION, is_use_warm_up=c.IS_USE_WARM_UP, initial_eta=c.INITIAL_ETA,
        max_eta=c.MAX_ETA, min_eta=c.MIN_ETA, rise_num=c.RISE_NUM, down_num=c.DOWN_NUM, is_overflow_detection=c.IS_OVERFLOW_DETECTION, overflow_threshold=c.
        early_stop_threshold=c.EARLY_STOP_THRESHOLD

    ):
```

LIRクラスのコンストラクタには、`constants_client.py` で設定されたパラメータがデフォルトで入ります。
コンストラクトするときに渡さないといけない必須のプロパティは、

- stub (GateAIのinput_client のgrpc スタブ)
- model_index (GateAI lir サービスを複数立ち上げている際、どのlir サービスに対して学習を実行するか、のインデックス)

です。
他の指定できるパラメータの詳細は、下にまとめていますのでご覧ください。

main.py  の　test_lir 関数をご覧いただくと、

```
lir0 = LIR(stub=stub, model_index=0)
```

となっています。


```
lir0.reset_training()
```
を学習を行う前に実行し、GateAIサービスが保持する学習履歴などの情報を空にできます。

次に、

```
lir0.keygen_level1()
```

を用い、学習用、推論用の鍵ペアを生成します。
input_clientで生成された鍵は、lirサービスに自動的に送付されます。

鍵の生成が実行された後は、学習に必要なデータを準備します。

```
    xs, ys1, ys2 = data.get_data()
    ys = ys1
    xs_train, xs_test, ys_train, ys_test = train_test_split(xs, ys, test_size=0.2, random_state=10)
```

にて、学習用のデータ、およびテストデータを準備しています。

次に、

```
        lir0.set_config()
        lir0.reset_training_data()

        lir0.upload_train_data(xs_train, ys_train)
        lir0.upload_test_data(xs_test, ys_test)
```

の部分についてです。

```
lir0.set_config()
lir0.reset_training_data()
```

を実行し、lirサービスに対して、学習に必要なハイパーパラメータの値を設定します。
また、reset_training_data() を呼ぶことで、lirサービスに存在する暗号化された学習データを消去してリセットしています。

以上が終わると、

`upload_train_data(X, Y)`
を実行し、学習データをinput_client　に送信します。
これらの学習データは、後述の fit()関数を実行する際、
暗号化されてlirサービスへと送信され、学習されます。

また、`lir.upload_test_data(X, Y)`の実行により、
テストデータもinput_clientに送信されます。
テストデータは、学習中にロスを計算する用途で使われます。

```
        model_name = f"test_model_lmb_{lmb}"
        lir0.fit_level1(model_name)
```

学習用、テスト用のデータが送信されたら、
`lir.fit_level1(model_name)`
を実行し、学習をスタートします。

このとき、暗号化されたテストデータに対して計算されたロスをトラッキングし、
ロスが下がったときはlirサービスに保存されている暗号化モデルが更新されます。
また、アーリーストップにより、設定されたエポック数の間ロスが改善しなければ、学習がストップします。

ここで、`model_name`　を指定することで、lirサービスに保存されているモデルの名前を指定できます。
グリッドサーチを行うときなど、名前を同じものにするとモデルが上書きされますのでご注意ください。

また、

```
    lir0.get_models_info_all()
    print("\n\n========================")
    print(f"mae_info: {lir0.mae_info}")
    print(f"mse_info: {lir0.mse_info}")
    print(f"r2_info: {lir0.r2_info}")
```

の記載がありますが、
`lir.get_models_info_all()`　関数を実行すると、
lirサービスから学習したモデルの

- mae (mean absolute error)
- mse (mean square error)
- r2 (r2 value)

が返却されます。これらのメトリクスは、暗号化されたテストデータに対して計算されています。


これらの実行でモデルの性能を確認できます。
学習したモデルのうち、実際に推論に使いたいモデルを選出する際は、

```
    model_name = "test_model_lmb_0.01"
    lir1.transfer_model_to_inference_by_name(model_name)
```

というように、model_nameを指定し、

`lir.transfer_model_to_inference_by_name(model_name)`  
を実行します。

今回の学習でもでは、test_lirにもあるように、


`lmb_list = [0.01, 0.02, 0.03]`

と、リッジ回帰のパラメータに対して学習のループを回し、
その中で良かったものを確認し、推論に使えるように
選出しているシナリオを記載しています。






GateAI を用いた学習の実行時には、

```
  0%|                                                                                         | 0/100 [00:00<?, ?it/s]loss: 0.3031907647427941
mae: data: 0.01898363683789503

r2: data: 0.7155297396610949

  1%|8                                                                                | 1/100 [00:02<04:07,  2.50s/it]
best model will be saved in server... loss: 0.25401829641143836
```

のようなログが学習時のエポック毎に出力されます。  

- mae
- r2
- loss
が各エポックごとに計算され、ロスの減少が確認されると、

```
best model will be saved in server... loss: 0.254018296
```

と表示され、lirサービスにモデルが保存されていきます。

アーリーストップの条件に入ると、

```
early stop called, done========================
mae: data: 0.018987315684829625

r2: data: 0.715546816914878

```

となり、学習がストップします。


以上で線形回帰の学習に関するチュートリアルは終了です。
お疲れ様でした。



## LIRクラスの指定可能パラメータ

```
- stub: GateAI input_client へのスタブ
- model_index: GateAI lir サービスが複数デプロイされているときに、どのサービスを使用して学習を実行するか指定するためのインデックス
- attrs: 学習データの次元数
- bs: 学習時のバッチサイズ
- ss: 暗号のスロットサイズと呼ばれるパラメータ
- epoch: 学習する時のエポック数
- eta: 学習率
- lmb: リッジ回帰の際のハイパーパラメータ
- udpate_option: ["none", "l2"]の中から選択。"none"であれば単なる線形回帰、"l2"はリッジ回帰を指定
- is_use_warm_up: 学習率をウォームアップ(WU)で変化させるかのboolean
- initial_eta: WUする際の初期学習率
- max_eta: WUする際の最大学習率
- min_eta: WU後に小さくする学習率の終着値
- rise_num: WUする際の、initial_eta からmax_eta までに要するエポック数
- down_num: WUする際の、max_eta からmin_etaに減少するのに要するエポック数
- is_overflow_detection: 暗号特有のオーバーフローを検知するか指定するboolean
- overflow_threshold: オーバーフローを検知する際、lossが異常に大きな絶対値を持っていたときはオーバーフローが発生したとみなすが、その際の絶対値の閾値
- early_stop_threshold: アーリーストップする際の、ロスが減少しなくても今日するエポックのイテレーション回数
```


