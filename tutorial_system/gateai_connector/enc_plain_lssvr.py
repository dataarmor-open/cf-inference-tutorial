#!/usr/bin/python
# -*- coding: utf-8 -*-

from tqdm import tqdm
from re import X
from multiprocessing.spawn import prepare
import sys
from tkinter import Y
import grpc
import numpy as np
from sklearn.utils import check_X_y, check_array
import pytest
from math import isclose
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error

sys.path.append("../")
import constants_client
from gateai_connector.plain_lssvr import LSSVR_GRAD
from gateai_connector.enc_lssvr import ENC_LSSVR
from grpc_service import input_client_pb2_grpc
from grpc_service import common_model
from grpc_service import noise_service_pb2_grpc
from grpc_service import svr_model


def isclose(a, theory, rtol, option_print=0):
    if option_print == 1:
        print("rtol = ", np.max(np.abs(a - theory) / abs(theory)))
    return np.isclose(a, theory, rtol=rtol, atol=0).all()


"""ENC_LSSVR

* SVRサービスを利用するためのinput_clientとのコネクタ

"""


class ENC_PLAIN_LSSVR:
    def __init__(self):
        self.enc_lssvr = ENC_LSSVR()
        self.plain_lssvr = None
        self.X_train_for_plain = None
        self.y_train_for_plain = None

    def reset(self):
        """SVRインスタンスの初期化
        送信したパラメータ、X, y, サポートベクターのフラグを初期化する。
        """
        self.enc_lssvr.reset()
        self.plain_lssvr = None

    def keygen(self):
        """鍵生成
        input_clientに鍵を生成する。
        """
        self.enc_lssvr.keygen()

    def set_config(self, X_attrs, X_ds, C, epsilon):
        """パラメータの送信
        Args:
           X_attrs: 学習データの重み
           X_ds: 学習データのデータ数
           C: 正則化パラメータ
           epsilon: イプシロンチューブの長さ
           learning_rate: 最適化問題の学習率
           learning_epoch: 最適化問題の回数
        """
        self.enc_lssvr.set_config(
            X_attrs,
            X_ds,
            C,
            epsilon,
        )
        self.plain_lssvr = LSSVR_GRAD(learning_rate=learning_rate, learning_epoch=learning_epoch, C=C, epsilon=epsilon)

    def transfer_train_data(self, X_train, y_train):
        """学習データの送信
        Args:
           X_train: 学習データ
           y_train: 学習ラベル
        """
        # データを送信
        self.enc_lssvr.transfer_train_data(X_train, y_train)

        self.X_train_for_plain = X_train
        self.y_train_for_plain = y_train

    def train(self):
        """学習の開始（事前にデータの送信が必要）"""
        print("start train")
        if self.plain_lssvr.gamma == "auto":
            self.plain_lssvr.gamma = 1 / self.X_train_for_plain.shape[1]
        support = []
        if len(support) == 0:
            self.plain_lssvr.support_ = np.ones(self.X_train_for_plain.shape[0], dtype=bool)
        else:
            self.plain_lssvr.support_ = check_array(support, ensure_2d=False, dtype="bool")
        self.plain_lssvr.support_vectors_ = self.X_train_for_plain[self.plain_lssvr.support_, :]
        # print("support_vector = ", self.plain_lssvr.support_vectors_.shape)
        K = self.plain_lssvr.kernel_func(self.X_train_for_plain, self.plain_lssvr.support_vectors_)
        ata, beta, atone = self.plain_lssvr.make_problem(K, self.y_train_for_plain)
        self.plain_lssvr.solve_problem(ata, beta, atone)

        # 学習開始
        pb_message = common_model.message_pb2.PB_Message()
        self.enc_lssvr.stub.apply_train_for_training_without_solve(pb_message)
        pb_ata = self.enc_lssvr.stub.debug_insecure_get_ata_for_training(common_model.message_pb2.PB_Message())
        ata_np = np.array(pb_ata.data)
        ata_enc = ata_np.reshape(int(pb_ata.size), -1)
        pb_atone = self.enc_lssvr.stub.debug_insecure_get_atone_for_training(common_model.message_pb2.PB_Message())
        atone_enc = np.array(pb_atone.data)
        assert isclose(ata, ata_enc, rtol=1e-02, option_print=1)
        assert isclose(atone, atone_enc, rtol=1e-02, option_print=1)

        print("start solve")
        # debug
        for i in tqdm(range(self.plain_lssvr.learning_epoch)):
            # debug
            p = np.dot(ata, beta)
            p = p - atone
            beta = beta - self.plain_lssvr.learning_rate * p
            self.plain_lssvr.alpha_ = beta[1:]
            self.plain_lssvr.bias_ = beta[0]
            self.enc_lssvr.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
            pb_beta = self.enc_lssvr.stub.debug_insecure_get_beta_for_training(common_model.message_pb2.PB_Message())
            beta_enc = np.array(pb_beta.data)

            if i % 10:
                print(
                    "mse: = ",
                    self.enc_lssvr.score_of_self(),
                    self.plain_lssvr.score(self.X_train_for_plain, self.y_train_for_plain),
                )
            print(i, ": rtol = ", np.max(np.abs(beta_enc - beta) / abs(beta)))

        print(
            "mse: = ",
            self.enc_lssvr.score_of_self(),
            self.plain_lssvr.score(self.X_train_for_plain, self.y_train_for_plain),
        )

        if self.plain_lssvr.epsilon is not 0:
            # サポートベクターを更新
            self.enc_lssvr.stub.update_support_for_training(pb_message)
            support = self.plain_lssvr.update_support_vector(self.X_train_for_plain, self.y_train_for_plain)
            self.plain_lssvr.support_vectors_ = self.X_train_for_plain[self.plain_lssvr.support_, :]

            # 再度学習開始
            pb_message = common_model.message_pb2.PB_Message()
            K = self.plain_lssvr.kernel_func(self.X_train_for_plain, self.plain_lssvr.support_vectors_)
            ata, beta, atone = self.plain_lssvr.make_problem(K, self.y_train_for_plain)

            pb_message = common_model.message_pb2.PB_Message()
            self.enc_lssvr.stub.apply_train_for_training_without_solve(pb_message)
            pb_ata = self.enc_lssvr.stub.debug_insecure_get_ata_for_training(common_model.message_pb2.PB_Message())
            ata_np = np.array(pb_ata.data)
            ata_enc = ata_np.reshape(int(pb_ata.size), -1)
            pb_atone = self.enc_lssvr.stub.debug_insecure_get_atone_for_training(common_model.message_pb2.PB_Message())
            atone_enc = np.array(pb_atone.data)
            assert isclose(ata, ata_enc, rtol=1e-02, option_print=1)
            assert isclose(atone, atone_enc, rtol=1e-02, option_print=1)

            # self.stub.apply_train_for_training(pb_message)
            for i in tqdm(range(self.plain_lssvr.learning_epoch)):
                # debug
                p = np.dot(ata, beta)
                p = p - atone
                beta = beta - self.plain_lssvr.learning_rate * p
                self.plain_lssvr.alpha_ = beta[1:]
                self.plain_lssvr.bias_ = beta[0]

                self.enc_lssvr.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
                pb_beta = self.enc_lssvr.stub.debug_insecure_get_beta_for_training(
                    common_model.message_pb2.PB_Message()
                )
                beta_enc = np.array(pb_beta.data)

                if i % 10:
                    print(
                        "mse: = ",
                        self.enc_lssvr.score_of_self(),
                        self.plain_lssvr.score(self.X_train_for_plain, self.y_train_for_plain),
                    )

                print(i, ": rtol = ", np.max(np.abs(beta_enc - beta) / abs(beta)))
            # self.plain_lssvr.fit(self.X_train_for_plain, self.y_train_for_plain)

    def predict(self, X):
        """予測の開始（事前に学習を完了させることが必要）"""
        y_hat_enc = self.enc_lssvr.predict(X)
        y_hat_plain = self.plain_lssvr.predict(X)
        assert isclose(y_hat_plain, y_hat_enc, rtol=1e-01, option_print=1)
        return y_hat_enc


#     def score(self, X, y):
#         """作成したモデルと、引数の検証データでのMSEの計算
#         Args:
#             X: 検証データ
#             y: 検証ラベル
#         """
#         pb_input = svr_model.svrTrainInput_pb2.PB_SvrTrainInput()
#         X_test_ravel = np.ravel(X)
#         pb_input.X[:] = X_test_ravel
#         pb_input.index = X.shape[1]
#         pb_input.y[:] = y
#         self.stub.score_initialize_for_svr(pb_input)
#         mse = self.stub.score_for_svr(common_model.message_pb2.PB_Message()).data
#         return mse

#     def train_with_score(self, X, y):
#         """作成したモデルと、学習データでのMSEの計算、scoreよりも高速
#         """
#         # 学習開始
#         # debug
#         lssvr_grad = LSSVR_GRAD()


#         pb_message = common_model.message_pb2.PB_Message()
#         self.stub.apply_train_for_training_without_solve(pb_message)
#         pb_ata = self.stub.debug_insecure_get_ata_for_training(common_model.message_pb2.PB_Message())
#         ata_np = pb_ata.data
#         ata_reshape = ata_np.reshape(int(pb_ata.size), -1)

#         pb_atone = self.stub.debug_insecure_get_atone_for_training(common_model.message_pb2.PB_Message())
#         atone = np.array(pb_atone.data)

#         print("first train start")
#         for i in tqdm(range(self.learning_epoch-1)):
#             self.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())

#             if i%100 == 0:
#                 mse = self.stub.score_self_for_svr(common_model.message_pb2.PB_Message()).data
#                 print(i, ":","MSE = ",  mse)
#                 if mse > 1000000:
#                     return print("Overflow! ,Please consider to reduce learning_rate.")

#         if self.epsilon is not 0:
#             print("update support")
#             # サポートベクターを更新
#             self.stub.update_support_for_training(pb_message)

#             print("second train start")
#             # 再度学習開始
#             pb_message = common_model.message_pb2.PB_Message()
#             self.stub.apply_train_for_training_without_solve(pb_message)
#             for i in tqdm(range(self.learning_epoch-1)):
#                 self.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
#                 if i%100 == 0:
#                     mse = self.stub.score_self_for_svr(common_model.message_pb2.PB_Message()).data
#                     print(i, ":","MSE = ",  mse)
#                     if mse > 1000000:
#                         return print("Overflow! ,Please consider to reduce learning_rate.")

#     def train_with_debug(self):
#         """作成したモデルと、学習データでのMSEの計算、scoreよりも高速
#         """
#         # 学習開始
#         pb_message = common_model.message_pb2.PB_Message()
#         self.stub.apply_train_for_training_without_solve(pb_message)


#         print("first train start")
#         for i in tqdm(range(self.learning_epoch-1)):
#             self.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
#             self.stub.debug_insecure_get_ata_for_training(common_model.message_pb2.PB_Message())

#             if i%100 == 0:
#                 mse = self.stub.score_self_for_svr(common_model.message_pb2.PB_Message()).data
#                 print(i, ":","MSE = ",  mse)
#                 if mse > 1000000:
#                     return print("Overflow! ,Please consider to reduce learning_rate.")

#         if self.epsilon is not 0:
#             print("update support")
#             # サポートベクターを更新
#             self.stub.update_support_for_training(pb_message)

#             print("second train start")
#             # 再度学習開始
#             pb_message = common_model.message_pb2.PB_Message()
#             self.stub.apply_train_for_training_without_solve(pb_message)
#             for i in tqdm(range(self.learning_epoch-1)):
#                 self.stub.svr_solve_problem_one_batch(common_model.message_pb2.PB_Message())
#                 if i%100 == 0:
#                     mse = self.stub.score_self_for_svr(common_model.message_pb2.PB_Message()).data
#                     print(i, ":","MSE = ",  mse)
#                     if mse > 1000000:
#                         return print("Overflow! ,Please consider to reduce learning_rate.")


#     def get_model(self):
#         """サーバー上にあるモデルを取得する。
#         """
#         plain_model = LSSVR_GRAD()
#         plain_model.C = self.C
#         plain_model.epsilon = self.epsilon
#         plain_model.gamma = self.gamma
#         plain_model.learning_rate = self.learning_rate
#         plain_model.learning_epoch = self.learning_epoch
#         pb_message = common_model.message_pb2.PB_Message()
#         enc_model = self.stub.debug_insecure_get_model_for_training(pb_message)
#         bias = np.array(enc_model.bias)
#         alpha = np.array(enc_model.alpha)

#         plain_model.bias_ = bias
#         plain_model.alpha_ = alpha
#         sv_np = np.array(enc_model.sv)
#         sv_reshape = sv_np.reshape(int(enc_model.n_sv), -1)
#         plain_model.support_vectors_ = sv_reshape
#         return plain_model

# if __name__ == "__main__":
#     pass
