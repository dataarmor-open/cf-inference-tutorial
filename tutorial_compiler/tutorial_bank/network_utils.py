import grpc

from grpc_python.input_client_pb2_grpc import InputClientStub
from grpc_python.model_client_pb2_grpc import ModelClientStub

import network_constants

def get_input_client_stub():
  channel_input_client = grpc.insecure_channel(f'{network_constants.INPUT_CLIENT_HOST}:{network_constants.INPUT_CLIENT_PORT}', options=[])
  api = InputClientStub(channel_input_client)
  return api

def get_model_client_stub():
  channel_model_client = grpc.insecure_channel(f'{network_constants.MODEL_CLIENT_HOST}:{network_constants.MODEL_CLIENT_PORT}', options=[])
  api = ModelClientStub(channel_model_client)
  return api
