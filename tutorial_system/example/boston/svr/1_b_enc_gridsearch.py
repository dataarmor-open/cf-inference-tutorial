import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.model_selection import KFold
import time
import datetime

sys.path.append("../../../")
from gateai_connector.enc_preprocess import ENC_PREPROCESS
from gateai_connector.enc_lssvr import ENC_LSSVR
from gateai_connector.enc_model_selection import gridsearchCV

boston = load_boston()
xs = boston.data
ys = boston.target

if __name__ == "__main__":
    start = time.time()
    # preprocessサーバーの"tutorial_of_boston_data"という鍵を読み込む
    preprocess = ENC_PREPROCESS()
    preprocess.reset()
    preprocess.load_key_on_server("tutorial_of_boston_data")

    # SVRサーバーの"tutorial_of_boston_data"という鍵を読み込む
    lssvr = ENC_LSSVR()
    lssvr.reset()
    lssvr.load_key_on_server("tutorial_of_boston_data")
    
    # preprocessサーバーにデータを送信する
    preprocess.transfer_data(xs, ys)

    # KFoldの設定
    kf = KFold(n_splits=3, shuffle=True)

    # SVRで、パラメータC, epsilonに対して、グリッドサーチを行う。
    params_cnt = 5
    params = {"regressor__C": np.logspace(-2, 2, params_cnt), "regressor__epsilon": np.logspace(-2, 2, params_cnt)}
    print(params)
    gridsearch = gridsearchCV(preprocess, "standard", params, cv=kf)
    print(gridsearch["best_score_"])
    print(gridsearch["best_C"])
    print(gridsearch["best_epsilon"])

    # ベストパラメータで学習
    C = gridsearch["best_C"]
    epsilon = gridsearch["best_epsilon"]
    preprocess.train_test_split(test_size=0.1, random_state=1)
    preprocess.standard_scaler_fit()
    preprocess.save_standard_scaler_on_server("boston_standard_scaler")

    preprocess.standard_scaler_transform_x_train()
    preprocess.standard_scaler_transform_x_test()
    preprocess.send_svr()

    lssvr.set_config(C, epsilon)
    lssvr.train_with_score()
    lssvr.save_model_on_server("boston_lssvr_model")
    end = time.time()
    print(f"calcuration time: {datetime.timedelta(seconds = end-start)}")