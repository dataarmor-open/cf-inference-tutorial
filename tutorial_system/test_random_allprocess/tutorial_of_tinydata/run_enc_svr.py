import sys

import grpc
import numpy as np
import pickle
import time

from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.utils import check_X_y, check_array
from sklearn.exceptions import NotFittedError
from scipy.sparse.linalg import lsmr
from sklearn.preprocessing import normalize
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
import os
import shutil
import time
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.datasets import make_regression

sys.path.append("../../")
from gateai_connector.enc_lssvr import ENC_LSSVR
from grpc_service import input_client_pb2_grpc
from grpc_service import common_model
import constants_client as c

# search_parameter_for_enc_svrでのchosen C, chosen epsilonをセット
C = 100
epsilon = 1

# search_parameter_for_sklearn_svr C, chosen epsilonをセット
C_plain = 100
epsilon_plain = 0.01


def _make_pb_from_doubleList(doubleList):
    if len(doubleList.shape) == 2:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = np.ravel(doubleList)
        pb.size = doubleList.shape[1]
    else:
        pb = common_model.doubleList_pb2.PB_DoubleList()
        pb.data[:] = doubleList
        pb.size = 0
    return pb


def transfer_train_test_data(X_train, X_test, y_train, y_test):
    # make connection
    options = [
        ("grpc.max_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
        ("grpc.max_receive_message_length", c.GRPC_MAX_MESSAGE_LENGTH),
    ]
    channel_input_client = grpc.insecure_channel(f"{c.INPUT_CLIENT_HOST}:{c.INPUT_CLIENT_PORT}", options=options)
    stub = input_client_pb2_grpc.InputClientStub(channel_input_client)
    # make preprocess key
    stub.preprocess_gen_key(common_model.message_pb2.PB_Message())
    stub.preprocess_reset_data(common_model.message_pb2.PB_Message())
    # transfer x_train
    pb_x = _make_pb_from_doubleList(X_train)
    stub.transfer_x_train(pb_x)
    # transfer x_test
    pb_x = _make_pb_from_doubleList(X_test)
    stub.transfer_x_test(pb_x)
    # transfer y_train
    pb_y = _make_pb_from_doubleList(y_train)
    stub.transfer_y_train(pb_y)
    # transfer y_test
    pb_y = _make_pb_from_doubleList(y_test)
    stub.transfer_y_test(pb_y)
    # transfer data to svr
    stub.transfer_data_to_svr_from_preprocess(common_model.message_pb2.PB_Message())
    # convert svr shape
    stub.svr_convert_preprocess_data(common_model.message_pb2.PB_Message())


if __name__ == "__main__":
    xs, ys = make_regression(n_samples=100, n_features=10, random_state=1)
    # xs, ys = make_regression(n_samples=10,n_features=10,random_state=1)
    reg = ENC_LSSVR()
    reg.reset()
    reg.keygen()
    print(xs.shape, ys.shape)

    # 標準化を行う
    X_train, X_test, y_train, y_test = train_test_split(xs, ys, test_size=0.1, random_state=1)
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    # 　暗号文での学習を開始

    print("start transfer_train_data")
    transfer_train_test_data(X_train, X_test, y_train, y_test)
    reg.set_config(
        C,
        epsilon,
    )
    print("start train_with_score")
    reg.train_with_score()
    print("start predict")
    y_hat = reg.predict(X_test)

    # SVRサービスの学習の性能評価のため、Scikitlearnで学習された線形回帰モデルを作成する
    reg_plain_lir = LinearRegression().fit(X_train, y_train)
    y_hat_lir = reg_plain_lir.predict(X_test)

    # SVRサービスの学習の性能評価のため、Scikitlearnで学習されたSVR回帰モデルを作成する
    reg_plain_svr = SVR(C=C_plain, epsilon=epsilon_plain)
    reg_plain_svr.fit(X_train, y_train)
    y_hat_skl_svr = reg_plain_svr.predict(X_test)

    # それぞれの予測結果の性能を評価
    res_folder = "./res"
    if os.path.exists(res_folder):
        shutil.rmtree(res_folder)
        os.mkdir(res_folder)
    else:
        os.mkdir(res_folder)
    l = np.arange(y_test.min(), y_test.max())
    plt.scatter(y_test, y_hat_lir, c="green", label="Sklearn LIR")
    plt.scatter(y_test, y_hat_skl_svr, c="blue", label="Sklearn SVR")
    plt.scatter(y_test, y_hat, c="red", label="Encrypted SVR")
    plt.plot(l, l, c="k")
    plt.legend()
    plt.savefig(f"{res_folder}/y_predict_y_test.png")
    plt.clf()
    plt.close()

    print(
        "LIR: MSE = ",
        mean_squared_error(y_test, y_hat_lir),
        ", MAE = ",
        mean_absolute_error(y_test, y_hat_lir),
        ", R2 = ",
        r2_score(y_test, y_hat_lir),
    )
    coef = np.polyfit(y_test, y_hat_lir, 1)
    print("LIR:y_true = " + str(coef[0]) + " + " + str(coef[1]) + " * y_predict")

    print(
        "SVR: MSE = ",
        mean_squared_error(y_test, y_hat_skl_svr),
        ", MAE = ",
        mean_absolute_error(y_test, y_hat_skl_svr),
        ", R2 = ",
        r2_score(y_test, y_hat_skl_svr),
    )
    coef = np.polyfit(y_test, y_hat_skl_svr, 1)
    print("SVR:y_true = " + str(coef[0]) + " + " + str(coef[1]) + " * y_predict")

    print(
        "ENC_SVR: MSE = ",
        mean_squared_error(y_test, y_hat),
        ", MAE = ",
        mean_absolute_error(y_test, y_hat),
        ", R2 = ",
        r2_score(y_test, y_hat),
    )
    coef = np.polyfit(y_test, y_hat, 1)
    print("ENC_SVR:y_true = " + str(coef[0]) + " + " + str(coef[1]) + " * y_predict")

    # LSSVR_GRADクラスでモデルを取得
    model_from_enc = reg.get_model()

    # pickleでファイルに保存可能
    with open("model.pickle", mode="wb") as fp:
        pickle.dump(model_from_enc, fp)

    with open("model.pickle", mode="rb") as fp:
        reg_load = pickle.load(fp)

    # LSSVR_GRADクラスのためこのようにpredictできる
    y_hat_model_from_enc = reg_plain_svr.predict(X_test)
